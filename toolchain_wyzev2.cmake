set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_C_COMPILER "/openmiko/build/buildroot-2016.02/output/host/usr/bin/mipsel-ingenic-linux-uclibc-gcc")
set(CMAKE_CXX_COMPILER "/openmiko/build/buildroot-2016.02/output/host/usr/bin/mipsel-ingenic-linux-uclibc-g++")


set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)