/**
******************************************************************************
* @file           : image_cap.h
* @brief          : Header file of capture image
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef IMAGE_CAP_H
#define IMAGE_CAP_H

#include <string>
#include <unistd.h>



namespace BeexCamApp{
    class ImageCapture{
    private:
        std::string v4l2Dev;
        int fd;

        // method
        bool initDev(void);
        int xioctl(int fd, int request, void *arg);
        
    public:
        ImageCapture(){
            this->v4l2Dev = "/dev/video4";
        }
        ~ImageCapture(){
            close(this->fd);
        }
        uint8_t *buffer;

        bool capImageAsJPG(std::string &path);
        bool capImageAsBuffer(uint8_t *buffer);
    };
    
} // namespace BeexCamApp


#endif
