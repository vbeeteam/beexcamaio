/**
******************************************************************************
* @file           : upload_app.h
* @brief          : Header file of camera upload application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef UPLOAD_APP
#define UPLOAD_APP

/* Include ------------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"


class CamUploadApp{
private:
    
public:
    CamUploadApp();
    ~CamUploadApp();

    // Attributes
    bool is_running;
    bool is_stop;

    // Method
    int run(CamDevInfor *cam_info);
};



#endif
