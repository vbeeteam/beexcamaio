/**
******************************************************************************
* @file           : storage_app.h
* @brief          : Header file of camera Storage application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef STORAGE_APP_H
#define STORAGE_APP_H

/* Include ------------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"
#include "data_struct/avpacket_queue.h"
#include <string>
#include <queue>
#include <mutex>


class CamStorageApp{
private:
    
public:
    CamStorageApp();
    ~CamStorageApp();

    // attribute
    bool is_running;
    bool is_stop;
    std::string storage_path;
    uint32_t time_check_ffmpeg;

    WebeCamAVPacket::AVPacketQueue avPacketQueue;
#ifdef WYZE_CAM_V2
    uint32_t timeout_storage;
#endif

    int runFfmpeg(CamDevInfor *cam_info);
    int runFfmpegStorage(CamDevInfor *cam_info, boardCastDataProvider::ffmpeg_cap *producer, bool *cap_state);

    bool saveThumbnail(CamDevInfor *cam_info, std::string path);

    bool initDir(CamDevInfor *cam_info);  
    
};



#endif
