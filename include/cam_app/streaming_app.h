/**
******************************************************************************
* @file           : streaming_app.h
* @brief          : Header file of camera streaming application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef STREAMING_APP
#define STREAMING_APP

/* Include ------------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"
#include "data_struct/avpacket_queue.h"



class CamStreamingApp{
private:
    // Attribute
    std::mutex fameQueuLocker;
    static const uint16_t frame_queue_max = 30;
    
public:
    CamStreamingApp();
    ~CamStreamingApp();

    // Attributes
    bool is_stop;
    bool is_running;
    uint32_t time_refresh;
    WebeCamAVPacket::AVPacketQueue avPacketQueue;
    WebeCamAVPacket::AVPacketQueue avPacketQueueRelay;
    

    // Methods
    void run(CamDevInfor *cam_info);
    void run(CamDevInfor *cam_info, boardCastDataProvider::ffmpeg_cap *producer, boardCastDataProvider::ffmpeg_transcode *trans_code_eng, bool *trans_code_state);
    int MultiChanelFfmpegStreaming(CamDevInfor *camInfo, boardCastDataProvider::ffmpeg_cap *producer, bool *cap_state);
    int RtspRelayStreaming(CamDevInfor *camInfo, boardCastDataProvider::ffmpeg_cap *producer, bool *cap_state);
};



#endif
