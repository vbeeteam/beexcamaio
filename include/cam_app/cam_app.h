/**
******************************************************************************
* @file           : cam_app.h
* @brief          : Header file of camera application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CAM_APP_H
#define CAM_APP_H

/* Include ------------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"
#include "logger/webe_logger.h"
#include <queue>
#include <mutex>
#include <thread>

#include "cam_app/streaming_app.h"
#include "cam_app/storage_app.h"
#include "cam_app/upload_app.h"

#include "ffmpeg_engine/boardCastImplement.hpp"
#include "data_struct/avpacket_queue.h"





class WebeCamApp{
private:
    // Attributes
    CamDevInfor cam_infor;

    WebeCamAVPacket::AVPacketQueue decodeQueue;
    WebeCamAVPacket::AVPacketQueue transcodeQueue;
    uint16_t transcode_size;
    
public:
    WebeCamApp();
    ~WebeCamApp();

    // Attributes
    CamStreamingApp streamingApp;
    CamStorageApp storageApp;
    CamUploadApp uploadApp;
    bool is_running;
    bool is_stop;
    bool cap_state;
    bool transcode_state;

    uint32_t timeout_capture_thread;
    pthread_t capture_thread_handler;
    uint32_t timeout_capture;

    boardCastDataProvider::ffmpeg_cap *producer;
#ifdef WYZE_CAM_V2
    bool restart_flag;

    void handleRestart(void);
#endif
    

    // Methods
    int run(CamDevInfor *p_cam);
    int handleFfmpegAVPacketCapture(void);
    uint32_t getTimestamp(void);
};




#endif
