/**
******************************************************************************
* @file           : cam_onvif_control.h
* @brief          : Header file of worker for Camera Onvif Control
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1
#ifndef CAM_ONVIF_CONTROL_H
#define CAM_ONVIF_CONTROL_H

#include <string>>

namespace BeexCameraCenter{
    class CameraOnvifControl{
    private:
        // Attribute
        const std::string PORT_HTTP_SERVER_ONVIF_MODULE = "8080";
        const std::string HOST_HTTP_SERVER_ONVIF_MODULE = "http://127.0.0.1";
        
        bool getListOnvifCamera(std::string *msgResp);
        bool updateAuthenOnvifCamera(std::string &ipOnvif, std::string &username, std::string &pass);
    public:
        CameraOnvifControl(){

        }
        ~CameraOnvifControl(){

        }

        void run(void);
    };
    
} // namespace BeexCameraCenter


extern BeexCameraCenter::CameraOnvifControl cameraOnvifWorker;


#endif
#endif
