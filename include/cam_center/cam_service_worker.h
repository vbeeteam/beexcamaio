/**
******************************************************************************
* @file           : cam_service_worker.h
* @brief          : Header file of worker for camera service
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CAM_SERVICE_WORKER_H
#define CAM_SERVICE_WORKER_H

/* Include ------------------------------------------------------------------*/
#include "socket/tcp_socket.h"
#include "data_struct/cam_data_struct.h"
#include <string>
#include <mutex>

class IpcWBCorePacket{
private:
    
public:
    IpcWBCorePacket();
    ~IpcWBCorePacket();

    static const uint16_t body_max = 5000;
    uint16_t magic;
    std::string serial;
    uint16_t serv_type;
    uint16_t dev_type;
    uint16_t body_len;
    uint8_t body[IpcWBCorePacket::body_max];    
};


class IpcWBCore{
private:
    Socket ipc_socket;
    TcpConnection ipc_client;
    uint16_t ipc_port;
    std::string ipc_host;
    std::mutex tcpLocker;
    
public:
    IpcWBCore();
    ~IpcWBCore();

    // Attributes

    bool conn_state;

    //Methods
    void run(void);
    void init(void);
    bool sendPacket(IpcWBCorePacket *packet);
    bool recvPacket(IpcWBCorePacket *packet);
};



class CamServiceWorker{
private:
    
public:
    CamServiceWorker();
    ~CamServiceWorker();

    // Attributes
    static const uint16_t request_serial = 23;
    static const uint16_t authen_service = 10;
    static const uint16_t ping_service = 8;
    static const uint16_t start_view_service = 21;
    static const uint16_t stop_view_service = 22;
    static const uint16_t zoom_state_view_service = 29;

    static const uint16_t scanCamera = 32;
    static const uint16_t genCamSerial = 35;
    static const uint16_t check_stream = 34;
    static const uint16_t remove_camera = 11;

    static const uint16_t req_cam_serv_info = 103;
    static const uint16_t sync_cam  = 12;

    static const uint16_t reload_service = 104;
    static const uint16_t restart_janus = 105;

    static const uint16_t report_info_camera = 36;

    static const uint16_t pan_til_control = 24;

    IpcWBCore ipcWBCore;
    WebeCamDevState::CamDevState sync_cam_status;

    // Methods
    void run(void);
    void handleCamStatus(void);
    void handleRecvServ(void);

    // service
    void requestSerial(std::string mac_addr);
    void requestAuthen(std::string dev_serial);
    void ping(CamDevInfor *cam_info);
    void respScanCam(IpcWBCorePacket *packet);
    void parseAddCam(IpcWBCorePacket *packet);
    bool getThumbnailUrl(CamDevInfor *cam_info, std::string *thumb_url);
    void respCheckStream(IpcWBCorePacket *packet);
    void removeCamera(IpcWBCorePacket *packet);
    void reqCamServInfo(std::string dev_serial);
    void parseCamServInfo(IpcWBCorePacket *packet);
    void reqSyncCam(void);
    void parseSyncCam(IpcWBCorePacket *packet);
    void parseReloadServ(std::string dev_serial);
    void reqRestartJanusEngine(void);
    void reportInfoCam(CamDevInfor *cam_info);
};



/* Public Object -----------------------------------------------------------------------*/
extern CamServiceWorker camServiceWorker;



#endif
