/**
******************************************************************************
* @file           : cam_app_worker.h
* @brief          : Header file of worker for Camera Application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CAM_APP_WORKER_H
#define CAM_APP_WORKER_H

/* Include ------------------------------------------------------------------*/


class CamAppWorker
{
private:
    
public:
    CamAppWorker();
    ~CamAppWorker();

    void run(void);
};


/* Public Object --------------------------------------------------------------*/
extern CamAppWorker camAppWorker;

#endif
