/**
******************************************************************************
* @file           : resource_manage_worker.h
* @brief          : Header file of worker for manage resource
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef RESOURCE_MANAGE_WORKER_H
#define RESOURCE_MANAGE_WORKER_H

class ResourceManageWorker{
private:
    
public:
    ResourceManageWorker(){

    }
    ~ResourceManageWorker(){

    }

    void run(void);
    void manageStorage(void);
};

extern ResourceManageWorker resourceManageWorker;

#endif
