/**
******************************************************************************
* @file           : main_cam_center.h
* @brief          : Header file of main of camera center
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef MAIN_CAM_CENTER_H
#define MAIN_CAM_CENTER_H

/* Include ------------------------------------------------------------------*/


class MainCamCenter
{
private:
    
public:
    MainCamCenter();
    ~MainCamCenter();

    void run(void);
};

/* Public Object ---------------------------------------------------------------*/
extern MainCamCenter camCenter;

#endif
