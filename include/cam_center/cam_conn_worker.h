/**
******************************************************************************
* @file           : cam_conn_worker.h
* @brief          : Header file of worker for camera connection
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CAM_CONN_WORKER_H
#define CAM_CONN_WORKER_H

/* Include ------------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"


class CamConnWorker{
private:
    
public:
    CamConnWorker();
    ~CamConnWorker();

    void run(void);

    bool hanldeCamConn(void);
    void checkCapRtsp(CamDevInfor *camInfo, uint32_t *timeout, int *result);
    bool getOnvifInfor(CamDevInfor *cam_infor);
};


/* Public Object --------------------------------------------------------------*/
extern CamConnWorker camConnWorker;

#endif
