/**
******************************************************************************
* @file           : cam_upload_worker.h
* @brief          : Header file of worker for camera upload file
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CAM_UPLOAD_WORKER_H
#define CAM_UPLOAD_WORKER_H

/* Include -----------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"
#include <queue>
#include <mutex>
#include <stdint.h>
#include <stdbool.h>

#ifdef INTEL_NUC_BOX
#include "Poco/Net/SSLManager.h"
#include "Poco/Net/HTTPSClientSession.h"



class SSLInitializer {
public:
    SSLInitializer() { Poco::Net::initializeSSL(); }

    ~SSLInitializer() { Poco::Net::uninitializeSSL(); }
};

#endif


struct UploadMemoryStruct {
    char *memory;
    size_t size;
};

class CamUploadWorker{
private:
    static const int16_t queue_size = 2;
    std::queue<CamUploadPacket> pktQueue;
    std::mutex myLocker;

    // Method

    void updateUrl(void);

    static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);    

    bool uploadPacketByHttps(std::string url, CamUploadPacket *pkt, std::string *resp);
    bool uploadPacketByHttp(std::string url, CamUploadPacket *pkt, std::string *resp);
    #ifdef INTEL_NUC_BOX
    bool recvResponseByHttps(Poco::Net::HTTPSClientSession *httpSession, bool *result);
    #endif
    
public:
    CamUploadWorker();
    ~CamUploadWorker();

    // Attribute
    std::string record_video;
    std::string record_video_thumb; 

    bool is_stop;

    std::string url_upload_default;

    void run(void);
    bool uploadFile(CamUploadPacket *pkt, std::string *resp);

    bool pushPacket(CamUploadPacket *pkt);
    bool getPacket(CamUploadPacket *pkt);
};


//extern CamUploadWorker camUploadWorker;

#endif
