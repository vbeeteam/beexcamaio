/**
******************************************************************************
* @file           : os_func.h
* @brief          : Header file of OS Function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef OS_FUNC_H
#define OS_FUNC_H

/* Include ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <string>

class OSFunc{
private:
    
public:
    static uint32_t getTimestamp(void);
    static std::string getDate(void);
    static std::string getTime(void);
    static uint8_t splitString(std::string data, std::string key_split, std::string *data_split);
    static bool checkFile(std::string path);

    static size_t getSizeFile(std::string path);
    static std::string exeCommand(const char* cmd);
};


#endif
