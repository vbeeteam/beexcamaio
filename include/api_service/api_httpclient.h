/**
******************************************************************************
* @file           : api_httpclient.h
* @brief          : Header file of api httpclient
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef API_HTTPCLIENT_H
#define API_HTTPCLIENT_H

#include <unistd.h>
#include <string>

namespace BeexApiService{

    struct HttpClientMemoryStruct {
        char *memory;
        size_t size;
    };

    class ApiHttpClient{
    public:
        // sttribute
        static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);

        // method
        // http GET method
        bool requestGetByHttp(std::string &api, std::string &authorHeader, std::string *msgResp);

        // http POST method
        bool requestPostJsonByHttp(std::string &api, std::string &authorHeader, std::string &jsonData, std::string *msgResp);

        // http DELETE method
        bool requestDeleteJsonByHttp(std::string &api, std::string &authorHeader, std::string &jsonData, std::string *msgResp);
        
    
        ApiHttpClient(){

        }
        ~ApiHttpClient(){

        }

        // method
        
    };
    
} // namespace BeexApiService


#endif
