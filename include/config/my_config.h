/**
******************************************************************************
* @file           : my_config.h
* @brief          : header file of my configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef MY_CONFIG_H
#define MY_CONFIG_H

#include <string>
#include <mutex>
#include "data_struct/cam_data_struct.h"

namespace BeexConfig{

    class MyConfigInfo{
    private:
        
    public:
        MyConfigInfo(){

        }
        ~MyConfigInfo(){

        }
        DevInforMap listCamera;
    };
    

    class MyConfig{
    private:
        std::string filePath;
        std::string fileName;
        MyConfigInfo configInfo;
        std::mutex updateLocker;

        bool loadConfig(void);
        bool saveConfig(void);
    public:
        MyConfig(){
            this->fileName = "config.txt";
        }
        ~MyConfig(){

        }
        bool init(void);
        void getConfigInfo(MyConfigInfo &info);
        void updateListCam(DevInforMap &listCam);
    };
    
}

extern BeexConfig::MyConfig myConfig;

#endif
