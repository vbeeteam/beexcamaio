/**
******************************************************************************
* @file           : box_config.h
* @brief          : header file of box configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef BOX_CONFIG_H
#define BOX_CONFIG_H

#include <string>

namespace BeexConfig{

    // #define BEEX_BOX_CONFIG_FILE                    "/opt/boxconfig/boxconfig.txt"
#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1
    #define BEEX_BOX_CONFIG_FILE                    "/opt/boxconfig/boxconfig.txt"
#endif
#ifdef WYZE_CAM_V2
    #define BEEX_BOX_CONFIG_FILE                    "/sdcard/boxconfig/boxconfig.txt"
#endif
    class BoxInfo{
    private:
        
    public:
        BoxInfo(){

        }
        ~BoxInfo(){

        }
        //Attribute
        std::string serial;
        std::string appPath;
        std::string firmwareOs;
    };
    

    class BoxConfig{
    private:
        // Attribute
        BoxInfo boxInfo;

        bool loadConfig(void);
    public:
        BoxConfig(){
            this->boxInfo.serial = "";
            this->boxInfo.appPath = "/opt/myApp/";
            this->boxInfo.firmwareOs = "Unknown";
        }
        ~BoxConfig(){

        }

        void init(void);
        void getBoxInfo(BoxInfo *info);
    };
    
}

extern BeexConfig::BoxConfig boxConfig;

#endif
