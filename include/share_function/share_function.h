/**
******************************************************************************
* @file           : share_function.h
* @brief          : header file of share function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef SHARE_FUNCTION_H
#define SHARE_FUNCTION_H

#include <stdint.h>
#include <stdbool.h>
#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <iomanip>

namespace BeexShareFunction
{
	uint32_t getTimestamp(void);
	uint32_t getTimeMs(void);
	uint8_t splitString(std::string data, std::string key_split, std::string *data_split);
	bool checkFile(std::string path);
	std::string fileName(std::string path);
	bool creatFile(std::string path);
}


#endif // !SHARE_FUNCTION_H
