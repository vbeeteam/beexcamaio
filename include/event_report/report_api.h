/**
******************************************************************************
* @file           : report_api.h
* @brief          : Header file of report API
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef REPORT_API_H
#define REPORT_API_H

#include <string>
#include <mutex>
namespace BeexEventReport{
#ifdef INTEL_NUC_BOX
    class SSLInitializer {
    public:
        SSLInitializer() { Poco::Net::initializeSSL(); }

        ~SSLInitializer() { Poco::Net::uninitializeSSL(); }
    };
#endif
    class ReportApi{
    private:
        std::string url;
        std::mutex locker;

        std::string cacert_path;
        std::string dev_serial;
        std::string gw_serial;

        bool requestByHttps(std::string &api, std::string &data);
        bool requestByHttp(std::string &api, std::string &data);
        
    public:
        ReportApi(std::string devSerial, std::string gwSerial){
            this->cacert_path = "/opt/cacert/";
            this->dev_serial = devSerial;
            this->gw_serial = gwSerial;

            this->url = "https://api.beex.vn:8089/device-reports";
        }
        ~ReportApi(){

        }

        bool reportEvent(std::string &type, std::string &content);
    };
}


#endif
