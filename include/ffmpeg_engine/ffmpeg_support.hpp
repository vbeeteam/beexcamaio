#ifndef FFMPEG_SUPPORT_HPP
#define FFMPEG_SUPPORT_HPP
#ifdef _dbg_using_spdlog_

#include <stdio.h>

extern "C"{
#include <libavformat/avformat.h>
#include <libavutil/timestamp.h>
}
#include <iostream>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>

namespace boardCastDataProvider{

const double dbg_spdlog_size = 1048576 * 5;
const int dbg_spdlog_files = 3;

class Logger{
//private:
//    // methods
//    void init(void);
    char err_str[AV_ERROR_MAX_STRING_SIZE];
public:
    Logger(std::string nameLogger,std::string fileLogPath){
        sysLogger = spdlog::rotating_logger_mt(nameLogger, fileLogPath, dbg_spdlog_size, dbg_spdlog_files);
        spdlog::set_default_logger(sysLogger);
        spdlog::set_pattern("[%Y-%m-%dT%T%z] [%n] [%^---%L---%$] %v");
        spdlog::flush_every(std::chrono::seconds(3));
    }
    ~Logger(){}

    // logger
    std::shared_ptr<spdlog::logger>  sysLogger;
};

void log_packet(AVRational *time_base, const AVPacket *pkt, const char *tag = NULL);
void showFError(int ret);
bool getFError(int ret,std::string &outErrStr);
}

extern boardCastDataProvider::Logger dbg_spdlogger;

#endif
#endif // FFMPEG_SUPPORT_HPP
