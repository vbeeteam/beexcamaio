/*
 * boardCastReader_share.hpp
 *
 *  Created on: Mar 12, 2020
 *      Author: kien
 */

#ifndef S_COMPRESSEDDATAWRAPPER_BOARDCASTREADER_SHARE_HPP_
#define S_COMPRESSEDDATAWRAPPER_BOARDCASTREADER_SHARE_HPP_


extern "C"{
#include <libavformat/avformat.h>
}

#include <vector>
#include <unistd.h>


namespace boardCastDataProvider{
#define CV_WARN(message) fprintf(stderr, "warning: %s (%s:%d)\n", message, __FILE__, __LINE__)
#define CALC_FFMPEG_VERSION(a,b,c) ( a<<16 | b<<8 | c )
#ifndef USE_AV_INTERRUPT_CALLBACK
#if LIBAVFORMAT_BUILD >= CALC_FFMPEG_VERSION(53, 21, 0)
#define USE_AV_INTERRUPT_CALLBACK 1
#else
#define USE_AV_INTERRUPT_CALLBACK 0
#endif
#endif

struct Image_FFMPEG;
struct AVInterruptCallbackMetadata;

class wrap_OpencvDataReader{
protected:
	AVFormatContext * ic;
	AVCodec         * avcodec;
	int               video_stream;
	AVStream        * video_st;
	AVFrame         * picture;
	AVFrame           rgb_picture;
	int64_t           picture_pts;

	AVPacket          packet;
	//FIXME: Not handle alloc and free this instance yet
	Image_FFMPEG      *frame;

	int64_t frame_number, first_frame_number;

	double eps_zero;

	//FIXME: Remove after test
	int64_t sumPacket;
	char              * filename;
	AVCodecContext *decoder_ctx;

	bool f_grab_state;

#if LIBAVFORMAT_BUILD >= CALC_FFMPEG_VERSION(52, 111, 0)
	AVDictionary *dict;
#endif
#if USE_AV_INTERRUPT_CALLBACK
	//FIXME: Not handle alloc and free this instance yet
	AVInterruptCallbackMetadata *interrupt_metadata;
#endif
	bool postProcessFrame();
	double  r2d(AVRational r) const;
	int64_t dts_to_frame_number(int64_t dts);
	double  dts_to_sec(int64_t dts);
	int retrieveFrame();
	bool grabFrame(int &error_g);

	AVCodecParserContext *parser_direct;
public:
	wrap_OpencvDataReader(){
		frame = NULL;
#if USE_AV_INTERRUPT_CALLBACK
		interrupt_metadata = NULL;
#endif
		init();
	};
	virtual ~wrap_OpencvDataReader(){close();};

	bool open( const char* filename, const char* extra_infor = NULL);
	bool open(int av_codec_id = AV_CODEC_ID_H264,const char* extra_infor = NULL);
	virtual void close();
	bool demuxToFile(FILE* f_out);
	//FIXME: Remove retrieveFrame function after test
	virtual void init();
	double  get_fps() const;

};

#if USE_AV_INTERRUPT_CALLBACK
#define LIBAVFORMAT_INTERRUPT_OPEN_TIMEOUT_MS 30000
#define LIBAVFORMAT_INTERRUPT_READ_TIMEOUT_MS 30000

static
inline void get_monotonic_time(timespec *time)
{
    clock_gettime(CLOCK_MONOTONIC, time);
}

static
inline timespec get_monotonic_time_diff(timespec start, timespec end)
{
    timespec temp;
    if (end.tv_nsec - start.tv_nsec < 0)
    {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    }
    else
    {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

static
inline double get_monotonic_time_diff_ms(timespec time1, timespec time2)
{
    timespec delta = get_monotonic_time_diff(time1, time2);
    double milliseconds = delta.tv_sec * 1000 + (double)delta.tv_nsec / 1000000.0;

    return milliseconds;
}

struct AVInterruptCallbackMetadata
{
    timespec value;
    unsigned int timeout_after_ms;
    int timeout;
};


static
inline int _opencv_ffmpeg_interrupt_callback(void *ptr)
{
    AVInterruptCallbackMetadata* metadata = (AVInterruptCallbackMetadata*)ptr;
    //assert(metadata);

    if (metadata->timeout_after_ms == 0)
    {
//        printf("Interrupt disable!\n");
        return 0; // timeout is disabled
    }

    timespec now;
    get_monotonic_time(&now);
    double v_diffT = get_monotonic_time_diff_ms(metadata->value, now);
    metadata->timeout = get_monotonic_time_diff_ms(metadata->value, now) > metadata->timeout_after_ms;
//    metadata->timeout = v_diffT > metadata->timeout_after_ms;
//    printf("Test interrupt callback test %g --- threshold %d!\n",v_diffT,metadata->timeout_after_ms);
    return metadata->timeout ? AVERROR_EXIT : 0;
}
#endif

struct Image_FFMPEG
{
    unsigned char* data;
    int step;
    int width;
    int height;
    int cn;
};

class ImplMutex
{
public:
    ImplMutex() { }
    ~ImplMutex() { }

    void init();
    void destroy();

    void lock();
    bool trylock();
    void unlock();

    struct Impl;
protected:
    Impl* impl;

private:
    ImplMutex(const ImplMutex&);
    ImplMutex& operator = (const ImplMutex& m);
};

//struct ImplMutex::Impl
//{
//    void init() { pthread_spin_init(&sl, 0); refcount = 1; }
//    void destroy() { pthread_spin_destroy(&sl); }
//
//    void lock() { pthread_spin_lock(&sl); }
//    bool trylock() { return pthread_spin_trylock(&sl) == 0; }
//    void unlock() { pthread_spin_unlock(&sl); }
//
//    pthread_spinlock_t sl;
//    int refcount;
//};
//
//void ImplMutex::init()
//{
//    impl = new Impl();
//    impl->init();
//}
//
//void ImplMutex::destroy()
//{
//    impl->destroy();
//    delete(impl);
//    impl = NULL;
//}
//
//void ImplMutex::lock() { impl->lock(); }
//void ImplMutex::unlock() { impl->unlock(); }
//bool ImplMutex::trylock() { return impl->trylock(); }

class AutoLock
{
public:
    AutoLock(ImplMutex& m) : mutex(&m) { mutex->lock(); }
    ~AutoLock() { mutex->unlock(); }
protected:
    ImplMutex* mutex;
private:
    AutoLock(const AutoLock&); // disabled
    AutoLock& operator = (const AutoLock&); // disabled
};

static ImplMutex _mutex;


static int get_number_of_cpus(void)
{
#if LIBAVFORMAT_BUILD < CALC_FFMPEG_VERSION(52, 111, 0)
    return 1;
#elif defined __linux__ || defined __HAIKU__
    return (int)sysconf( _SC_NPROCESSORS_ONLN );
#else
    return 1;
#endif
}

static bool mvFile(const char* nameInputF,const char* nameOutF)
{
    FILE *fInput = fopen(nameInputF,"rb");
    FILE *fOuput = fopen(nameOutF,"rb");
    bool resultF = false;
    if(fInput == NULL)
        printf("Path input wrong!\n");
    else if(fOuput != NULL)
        printf("Output file path exist!\n");
    else{
        fOuput = fopen(nameOutF,"wb");
        if(fOuput == NULL)
            printf("Create file output failed!\n");
        else{
            char tempD[1025];
            int length = fread(tempD,1,1024,fInput);
            int length1;
            resultF = true;
            while(length > 0)
            {
                length1 = fwrite(tempD,1,length,fOuput);
                if(length1 != length)
                {
                    resultF = false;
                    break;
                }
                length = fread(tempD,1,1024,fInput);
            }
        }
    }
    if(fInput != NULL)
        fclose(fInput);
    if(fOuput != NULL)
        fclose(fOuput);
    return resultF;
}


}


#endif /* S_COMPRESSEDDATAWRAPPER_BOARDCASTREADER_SHARE_HPP_ */
