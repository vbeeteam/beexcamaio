/*
 * Ffmpeg_support.h
 *
 *  Created on: Mar 13, 2020
 *      Author: kien
 */


#ifdef _dbg_using_spdlog_

#include <stdio.h>

extern "C"{
#include <libavformat/avformat.h>
}

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/rotating_file_sink.h>

static std::shared_ptr<spdlog::logger> dbg_spdlogger;

namespace boardCastDataProvider{

const double dbg_spdlog_size = 1048576 * 5;
const int dbg_spdlog_files = 3;

void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt, const char *tag);
void log_packetX(const AVFormatContext *fmt_ctx, const AVPacket *pkt, const char *tag,int64_t old_dts);
void showFError(int ret);

#define LOG_MSG( ... )      printf( __VA_ARGS__ )
#define myErrorPrintfD( ... ) printf( __VA_ARGS__ " line %d of file %s\n",__LINE__,__FILE__)

static void init_logger(std::string nameLogger,std::string fileLogPath){
    dbg_spdlogger = spdlog::rotating_logger_mt(nameLogger, fileLogPath, dbg_spdlog_size, dbg_spdlog_files);
    spdlog::set_default_logger(dbg_spdlogger);
    spdlog::set_pattern("[%H:%M:%S %z] [%n] [%^---%L---%$] %v");
    spdlog::flush_every(std::chrono::seconds(3));
}

//Remove after test
void test_logger_bc_error();
void test_logger_bc_infor();
void test_logger_bc_warn();
void test_logger_bc_critical();
void test_logger_bc_debug();

}
#endif /* S_COMPRESSEDDATAWRAPPER_FFMPEG_SUPPORT_H_ */
