#ifndef BOARDCAST_FFMPEG_IO_H
#define BOARDCAST_FFMPEG_IO_H
#include "boardCastImplement.hpp"
#include "boardCastReader_share.hpp"
//#define _testAu_Est_dts_

namespace boardCastDataProvider{

class ffmpeg_cap_rtsp: public ffmpeg_cap{
protected:
#if USE_AV_INTERRUPT_CALLBACK
    AVInterruptCallbackMetadata interrupt_metadata;
#endif
public:
    ffmpeg_cap_rtsp(){}
    virtual ~ffmpeg_cap_rtsp(){}
    bool open(const char *input);
#if USE_AV_INTERRUPT_CALLBACK
    int get_new_packet(AVPacket &pkt){
        interrupt_metadata.timeout_after_ms = LIBAVFORMAT_INTERRUPT_READ_TIMEOUT_MS;
        get_monotonic_time(&interrupt_metadata.value);
        return ffmpeg_cap::get_new_packet(pkt);
    }
#endif
};

class ffmpeg_cap_v1: public ffmpeg_cap_rtsp{
protected:
    int vid_s;
#ifdef _dbg_cap_au_pf_
    int64_t diff_pts, prev_pts;
    int flag_t;
#endif
    int64_t diff_dts, prev_dts;
    //For control when we could confidence in diff_pts
    int64_t frame_c;

    void refresh(){
        frame_c = 0;
        vid_s = -1;
#ifdef _dbg_cap_au_pf_
        flag_t = 0;
        diff_pts = AV_NOPTS_VALUE;
        prev_pts = AV_NOPTS_VALUE;
#endif
        diff_dts = AV_NOPTS_VALUE;
        prev_dts = AV_NOPTS_VALUE;
        ffmpeg_cap::refresh();
    }
public:
    ffmpeg_cap_v1(){
        vid_s = -1;
#ifdef _dbg_cap_au_pf_
        diff_pts = AV_NOPTS_VALUE;
        prev_pts = AV_NOPTS_VALUE;
#endif
        diff_dts = AV_NOPTS_VALUE;
        prev_dts = AV_NOPTS_VALUE;

    }
    virtual ~ffmpeg_cap_v1(){}
    bool open(const char* input);
    int get_new_packet(AVPacket &pkt);
#ifdef _dbg_cap_au_pf_
    int get_new_packet(AVPacket &pkt,int &nPadFrame);
#endif
};

class ffmpeg_cap_v2: public ffmpeg_cap{
public:
    bool open(const char* input);
    ffmpeg_cap_v2(){}
};

class ffmpeg_out_ipl: public ffmpeg_out{
protected:
    AVOutputFormat *ofmt;
    AVFormatContext *ofmt_ctx;
    int stream_mapping_size;
    int *stream_mapping;
    std::vector<AVRational> in_tBase;
    AVPacket pPkt;
    //double frame_counter;
    void refresh();
public:
    bool open(const char* output,ffmpeg_cap &cap,int flag);
    bool open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag);
    bool write_data_old(int ret,AVPacket &in_pkt,bool f_continue = true);
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
    ffmpeg_out_ipl(){
        ofmt = NULL;
        ofmt_ctx = NULL;
        stream_mapping_size = 0;
        stream_mapping = NULL;
        //frame_counter = 0;
    }
    virtual ~ffmpeg_out_ipl(){refresh();}

};

class ffmpeg_out_ipl1: public ffmpeg_out{
protected:
    AVOutputFormat *ofmt;
    AVFormatContext *ofmt_ctx;
    AVPacket pPkt;
    double diff_ts;
    int vid;
    std::vector<AVRational> in_tBase;
    void refresh();
    my_output_signal internal_signal;
public:
    bool open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag);
    bool open(const char* output,ffmpeg_cap &cap,int correct_fps = -1);
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
    bool write_data_good(int ret,AVPacket &pkt,my_output_signal out_signal = MOUTFILE_NORMAL);
    ffmpeg_out_ipl1(){
        ofmt = NULL;
        ofmt_ctx = NULL;
        vid = 0;
        diff_ts = 0;
    }
    virtual ~ffmpeg_out_ipl1(){refresh();}
};

/**
 * @brief The ffmpeg_out_rtpT class: for write output stream to rtp protocol, create output
 * stream from transcoder
 */
class ffmpeg_out_rtpT: public ffmpeg_out_ipl1{
protected:
    AVBSFContext *bsfc;
    //bool remuxNonStop();
    bool initBSFC(AVCodecContext *in_ctx,const char* bsf_name = "dump_extra");
    void refresh(){
        if(bsfc != NULL){
            av_bsf_free(&bsfc);
        }
        ffmpeg_out_ipl1::refresh();
    }
public:
    ffmpeg_out_rtpT():bsfc(NULL){}
    virtual ~ffmpeg_out_rtpT(){
        if(bsfc != NULL){
            av_bsf_free(&bsfc);
        }
    }
    bool open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag);
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
    bool write_data_old(int ret,AVPacket &in_pkt,bool f_continue = true);
};

/**
 * @brief The ffmpeg_out_rtpM class: create output stream from encoder
 */
class ffmpeg_out_rtpM: public ffmpeg_out_rtpT{
protected:
    int pkt_counter;
    void refresh(){
        pkt_counter = 0;
        ffmpeg_out_rtpT::refresh();
    }
public:
    bool open(const char* output,ffmpeg_M2out *encode_out,int flag);
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
};

class ffmpeg_out_rtpS: public ffmpeg_out_ipl1{
protected:
    int pkt_counter;
    void refresh(){
        pkt_counter = 0;
        ffmpeg_out_ipl1::refresh();
    }
public:
    ffmpeg_out_rtpS(){}
    bool open(const char* output,ffmpeg_cap &cap,int flag);
    bool open(const char* output,ffmpeg_M2out *encode_out,int flag);
};

class ffmpeg_out_ipl3: public ffmpeg_out_ipl1{
protected:
    AVPacket dPkt;
    int time_to_end;//in minute
    uint8_t *data_last_end;
    int frame_counter;
    void refresh(){
        frame_counter = 0;
        if(data_last_end != NULL){
            av_freep(&data_last_end);
        }
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        dPkt.pts = AV_NOPTS_VALUE;
        dPkt.dts = AV_NOPTS_VALUE;
        dPkt.size = 0;
        ffmpeg_out_ipl1::refresh();
    }
    void handleLastWrite();
public:
    ffmpeg_out_ipl3(){
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        time_to_end = 0;
        data_last_end = NULL;
    }
    ffmpeg_out_ipl3(uint timeStop){
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        time_to_end = timeStop;
        data_last_end = NULL;
    }
    ~ffmpeg_out_ipl3(){
        if(data_last_end != NULL){
            av_freep(&data_last_end);
        }
    }
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
};

class ffmpeg_out_ipl4: public ffmpeg_out_ipl1{
protected:
    int time_to_end;
    std::vector<AVPacket*> buffAV;
    int frame_counter;
    double r_ts;
    void refresh(){
        r_ts = 0;
        frame_counter = 0;
        for(auto it : buffAV){
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
        ffmpeg_out_ipl1::refresh();
    }
    virtual void handleLastWrite();
public:
    ffmpeg_out_ipl4(uint timeStop){
        time_to_end = timeStop;
    }
    ~ffmpeg_out_ipl4(){
        for(auto it : buffAV){
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
    }
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
};

class ffmpeg_out_ipl6: public ffmpeg_out{
protected:
    int vid;
    int time_to_end;
    int frame_counter;
    std::string fileWorking;
    std::string fileTempName;
    my_output_signal internal_signal;
    AVOutputFormat *ofmt;
    AVFormatContext *ofmt_ctx;
    AVRational timeBase;
    void refresh();
    void close();
    bool writeToRealFile();
public:
    ffmpeg_out_ipl6(){
        frame_counter = 0;
        time_to_end = 10;
        vid = 0;
        internal_signal = MOUTFILE_WAIT_KEY_FRAME;
        ofmt = NULL;
        ofmt_ctx = NULL;
    }
    ffmpeg_out_ipl6(int _time2End_){
        frame_counter = 0;
        time_to_end = _time2End_;
        vid = 0;
        internal_signal = MOUTFILE_WAIT_KEY_FRAME;
        ofmt = NULL;
        ofmt_ctx = NULL;
    }
    ~ffmpeg_out_ipl6(){
        if(ofmt_ctx != NULL){
            if(ofmt != NULL){
                if(!(ofmt->flags & AVFMT_NOFILE)){
                    avio_closep(&ofmt_ctx->pb);
                }
            }
            avformat_free_context(ofmt_ctx);
        }
        ofmt = NULL;
        ofmt_ctx = NULL;
    }
    void setDuration(uint duration){
       time_to_end = duration;
    }
    bool write_data_good(int ret,AVPacket &in_pkt,my_output_signal out_signal = MOUTFILE_NORMAL);
    bool open(const char* output,const char* realFinalFile,ffmpeg_cap &cap);
    bool open(const char* output,ffmpeg_cap &cap,int flag = 0);
    bool open(const char* output,ffmpeg_M2out *encode_out,int flag = 0);
    bool open(const char* output,const char* realFinalFile,ffmpeg_M2out *encode_out);
};

class ffmpeg_out_ipl5: public ffmpeg_out_ipl6
{
protected:
    std::vector<AVPacket*> buffAV;
    void handleLastWrite();
    void refresh(){
        for(auto it : buffAV){
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
        ffmpeg_out_ipl6::refresh();
    }
public:
    ffmpeg_out_ipl5(uint timeStop):ffmpeg_out_ipl6(timeStop){}
    ~ffmpeg_out_ipl5(){
        for(auto it : buffAV){
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
    }
    bool write_data_good(int ret,AVPacket &in_pkt,my_output_signal out_signal = MOUTFILE_NORMAL);
    bool open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag = 0);
    bool open(const char* output,const char* realFinalFile,ffmpeg_cap &cap,ffmpeg_transcode *tsc);
};

class ffmpeg_out_debug: public ffmpeg_out_ipl1{
public:
    AVPacket dPkt;
    int64_t diff_ts;
    int dbg_c;
    int vid;
    int time_to_end;//in minute
    uint8_t *data_last_end;
    std::vector<AVPacket*> buffAV;
    void refresh(){
        for(auto it : buffAV){
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();

        if(data_last_end != NULL){
            av_freep(&data_last_end);
        }
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        dPkt.pts = AV_NOPTS_VALUE;
        dPkt.dts = AV_NOPTS_VALUE;
        dPkt.size = 0;
        ffmpeg_out_ipl1::refresh();
    }
    ffmpeg_out_debug(){
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        diff_ts = 0;
        time_to_end = 0;
        data_last_end = NULL;
    }
    ffmpeg_out_debug(uint timeStop){
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        diff_ts = 0;
        time_to_end = timeStop;
        data_last_end = NULL;
    }
    ~ffmpeg_out_debug(){
        if(data_last_end != NULL){
            av_freep(&data_last_end);
        }
    }
    void handleLastWrite_New();
    void handleLastWrite();
    bool open(const char* output,ffmpeg_cap &cap,int flag);
    bool write_data_new(int ret,AVPacket &in_pkt,bool f_continue = true);
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
    bool write_data_old(int ret,AVPacket &in_pkt,bool f_continue = true);
};

class ffmpeg_out_debug1: public ffmpeg_out_ipl1{
public:
    AVPacket dPkt;
    int64_t diff_ts;
    int dbg_c;
    int vid;
    int time_to_end;//in minute
    float init_fps;
    float final_fps;
    std::string fileWorking;
    std::string fileTempName;
    ffmpeg_out_debug1(){
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        diff_ts = 0;
        time_to_end = 0;
        fileTempName = std::string("temp.mp4");
    }
    ffmpeg_out_debug1(uint timeStop){
        memset(&dPkt,0,sizeof(dPkt));
        av_init_packet(&dPkt);
        diff_ts = 0;
        time_to_end = timeStop;
        fileTempName = std::string("temp.mp4");
    }
    bool setFileTempName(const char* uniqueObjectTempPath){
        if(uniqueObjectTempPath != NULL){
            fileTempName = std::string(uniqueObjectTempPath);
            return true;
        }
        return false;
    }
    bool openRefine(ffmpeg_cap &cap);
    void handleLastWrite();
    bool open(const char* output,ffmpeg_cap &cap,int flag);
    bool write_data(int ret,AVPacket &in_pkt,bool f_continue = true);
};

class ffmpeg_out_ipl2: public ffmpeg_out_ipl1{
public:
    bool open(const char* output,ffmpeg_M2out *encode_out,int flag);
};

class ffmpeg_out_srtpT: public ffmpeg_out_rtpT{
protected:
    std::string key_base64;
public:
    ffmpeg_out_srtpT()=delete;
    ffmpeg_out_srtpT(const char* key_s){key_base64 = std::string(key_s);}
    virtual ~ffmpeg_out_srtpT(){}
    bool open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag);
};
}
#endif // BOARDCAST_FFMPEG_IO_H
