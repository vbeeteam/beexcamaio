/*
 * boardCastImplement.hpp
 *
 *  Created on: Dec 5, 2019
 *      Author: Le Trung Kien
 *       Email: le.trung.kien@mqsolutions.vn
 * 		 Email: kien210593@gmail.com
 */

#ifndef S_COMPRESSEDDATAWRAPPER_BOARDCASTIMPLEMENT_HPP_
#define S_COMPRESSEDDATAWRAPPER_BOARDCASTIMPLEMENT_HPP_

extern "C"{
#include <libavformat/avformat.h>
}

#include <string>
#include <vector>

//#define _dbg_cap_au_pf_ //Check output auto pad frame
#ifdef _terga_codec_
#include <ip_lib.h>
#endif

#ifdef _dbg_using_spdlog_
//#include "Ffmpeg_support.h"
#include "ffmpeg_support.hpp"
#endif

#define _BC_MAX_FPS_ 30

namespace boardCastDataProvider{

class ffmpeg_remux{
protected:
	AVOutputFormat *ofmt;
	AVFormatContext *ifmt_ctx, *ofmt_ctx;
	int stream_mapping_size;
	int *stream_mapping;
	int64_t old_dts;
	virtual void refresh();
	virtual bool remuxNonStop();
public:
	virtual bool remuxUntil(int maxFrame = -1);
	virtual bool remux(bool f_continue = true);
	ffmpeg_remux();
	virtual ~ffmpeg_remux(){refresh();};
	virtual bool open(const char* input,const char* output,const char* extra_infor = NULL);
};

class ffmpeg_remux_v1: public ffmpeg_remux{
protected:
	AVBSFContext *bsfc = NULL;
	//bool remuxNonStop();
	bool initBSFC(const char* bsf_name,int iVideoStream);
public:
	//bool remuxUntil(int maxFrame = -1);
	bool remux(bool f_continue = true);
	ffmpeg_remux_v1();
	virtual ~ffmpeg_remux_v1();
	bool open(const char* input,const char* output,const char* extra_infor = NULL);
};

class imageFFmpegCvt_G{
protected:
public:
    int open(uint width, uint height, enum AVPixelFormat i_pix_fmt,enum AVPixelFormat o_pix_fmt);
    void close();
    int convert(AVFrame *i_pic,AVFrame *o_pic);
    virtual ~imageFFmpegCvt_G(){close();}
};

class imageFFmpegCvt{
protected:
	unsigned char* data;
	int step;
	int width;
	int height;
	int cn;
	int bWidth;
	int bHeight;
	AVFrame rgb_picture;
public:
	void init();
    int open(AVCodecContext *decoder_ctx);
    virtual int open(AVCodecContext *decoder_ctx,enum AVPixelFormat in_pix_fmt);
    virtual int open(uint img_height,uint img_width,enum AVPixelFormat in_pix_fmt);
	void close();
    imageFFmpegCvt(){init();}
    virtual ~imageFFmpegCvt(){close();}
};

class imageFFmpegCvt_1: public imageFFmpegCvt{
public:
    imageFFmpegCvt_1(){}
    int open(AVCodecContext *decoder_ctx,enum AVPixelFormat out_pix_fmt);
    int open(uint img_height,uint img_width,enum AVPixelFormat out_pix_fmt);
};


//#define _test_GTIMEOUT_


enum my_stream_type{
    MSTREAM_NODEFINE = 0,
    MSTREAM_FILE = 1,
    MSTREAM_RTSP = 2,
    MSTREAM_WCAM = 3
};

class ffmpeg_cap{
protected:
    virtual void refresh(){
        mSt = MSTREAM_NODEFINE;
#ifdef _test_GTIMEOUT_
		d_t = NULL;
#endif
		if(ifmt_ctx != NULL){
			avformat_close_input(&ifmt_ctx);
		}
    }
    my_stream_type mSt;
    int max_fps;
public:
    // cv::Size getVideoSizeInfor(){
    //     cv::Size vSizeInfor(0,0);
    //     if(ifmt_ctx != NULL){
    //         int ret = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    //         if (ret < 0) {
    //             fprintf(stderr, "Cannot find a video stream in the input file\n");
    //             return vSizeInfor;
    //         }
    //         AVStream *video = ifmt_ctx->streams[ret];
    //         vSizeInfor.width = video->codecpar->width;
    //         vSizeInfor.height = video->codecpar->height;
    //     }
    //     return vSizeInfor;
    // }
    AVRational getVideoTimeBase(){
        AVRational timebase = {0,0};
        if(ifmt_ctx != NULL){
            int ret = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
            if (ret < 0) {
                fprintf(stderr, "Cannot find a video stream in the input file\n");
                return timebase;
            }
            AVStream *video = ifmt_ctx->streams[ret];
            timebase = video->time_base;
        }
        return timebase;
    }
    int getVideoStreamIndex(){
        if(ifmt_ctx != NULL){
            return av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
        }else{
            return -1;
        }
    }
    AVCodecID getInputCodecFormat(){
        AVCodecID rs_codecId = AV_CODEC_ID_NONE;
        if(ifmt_ctx != NULL){
            int ret = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
            if (ret < 0) {
                fprintf(stderr, "Cannot find a video stream in the input file\n");
                return rs_codecId;
            }
            AVStream *video = ifmt_ctx->streams[ret];
            rs_codecId = video->codecpar->codec_id;
        }
        return rs_codecId;
    }
#ifdef _test_GTIMEOUT_
	AVDictionary *d_t;
#endif
	AVFormatContext *ifmt_ctx;
#ifdef _test_GTIMEOUT_
	ffmpeg_cap():d_t(NULL),ifmt_ctx(NULL){};
#else
    ffmpeg_cap():ifmt_ctx(NULL),max_fps(_BC_MAX_FPS_){

    }
#endif

    my_stream_type getMStreamInputType(){return mSt;}
	//Return -1 if fail
    float get_fps();
    AVRational getAVFps();
    virtual ~ffmpeg_cap(){refresh();}
    virtual bool open(const char* input);
    virtual void showInfor(){
        av_dump_format(ifmt_ctx, 0, NULL, 0);
    }
    //Setting after the open
    virtual void setMaxFps(int in_max_Fps){
        max_fps = in_max_Fps;
    }
    //Note use it only after open function
    virtual std::string getInputInfor(){
        std::string infor;
        if(ifmt_ctx->iformat->name != NULL){
            infor = std::string("Name: ") + std::string(ifmt_ctx->iformat->name);
        }
        if(ifmt_ctx->iformat->long_name != NULL){
            infor += std::string(" | Long Name: ") + std::string(ifmt_ctx->iformat->long_name);
        }
        if(ifmt_ctx->iformat->mime_type != NULL){
            infor += std::string(" | Mime Type: ") + std::string(ifmt_ctx->iformat->mime_type);
        }
        return infor;
    }
    virtual int get_new_packet(AVPacket &pkt);
};

class ffmpeg_transcode{
protected:
	AVCodecContext *decoder_ctx;
	AVFrame *frame;
public:
	//We could use this fields to initial the output container
	//Note: We could wait to first decode success! Member the rtsp initial way
	AVCodecContext *encoder_ctx;
	int video_stream;
	AVCodec *enc_codec;
//    bool f_init;
    int n_init;
	int fps_out;
    uint64_t _frame_count_;
	virtual void refresh();
protected:
	virtual int encode_pkt(std::vector<AVPacket*> &outSet);
	virtual int dec_enc(AVPacket *packet,std::vector<AVPacket*> &outSet);
public:
	ffmpeg_transcode();
	virtual ~ffmpeg_transcode(){
		enc_codec = NULL;
        av_frame_unref(frame);
		av_frame_free(&frame);
		avcodec_free_context(&decoder_ctx);
		avcodec_free_context(&encoder_ctx);
    }
    virtual bool changeBitRate(int targetBitrate){return false;}
    virtual int get_gop_size(){return -1;}
    virtual bool open(ffmpeg_cap &cap,int new_fps = 0);
	virtual bool transcode(int ret,AVPacket &pkt,std::vector<AVPacket*> &outSet,bool f_continue = true);
};

class ffmpeg_M2out;

enum my_output_signal{
    //Default value when everything is okie
    MOUTFILE_NORMAL = 0,
    //Value to signal end of file
    MOUTFILE_END = 1,
    //Value to signal cam was interrupted
    MOUTFILE_INTERUPT = 2,
    //Not use yet: Value to signal end of file early than expected
    MOUTFILE_END_EARLY = 3,
    //Value use internal of write engine
    MOUTFILE_WAIT_KEY_FRAME = 4
};

class ffmpeg_out{
protected:
    virtual void refresh() = 0;
public:
    //Set time record video in minute
    virtual void setDuration(uint duration){}
    ffmpeg_out(){}
    virtual ~ffmpeg_out(){}
    virtual bool open(const char* output,const char* realFinalFile,ffmpeg_cap &cap){return false;}
    virtual bool open(const char* output,const char* realFinalFile,ffmpeg_cap &cap,ffmpeg_transcode *tsc){return false;}
    virtual bool open(const char* output,const char* realFinalFile,ffmpeg_M2out *encode_out){return false;}
    virtual bool write_data_good(int ret,AVPacket &pkt,my_output_signal out_signal = MOUTFILE_NORMAL){return false;}

    virtual bool open(const char* output,ffmpeg_cap &cap,int flag = 0){return false;}
    virtual bool open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag = 0){return false;}
    virtual bool open(const char* output,ffmpeg_M2out *encode_out,int flag = 0){return false;}
    virtual bool write_data(int ret,AVPacket &pkt,bool f_continue = true){return false;}
};

/*
 * FIXME: Remove after implement:
 * - This class will first return the opencv Mat (could be rbg or more better format for reduce convert
 * - Second It will keep a origin frame for encode result or using after processing submit frame for result.
 */

class ffmpeg_2Mat: public ffmpeg_out{
public:
    ffmpeg_2Mat(){}
    bool write_data(int ret,AVPacket &pkt,bool f_continue = true){return false;}
    virtual bool write_data(int ret,AVPacket &pkt,std::vector<AVFrame*> &avFset,bool f_continue = true){return false;}
    /* ret will show if need handle input frame */
    virtual ~ffmpeg_2Mat(){}
};

#ifdef _terga_codec_
class ffmpeg2Img{
public:
    ffmpeg2Img(){}
    virtual bool write_data(AVPacket &pkt,nvxcu_pitch_linear_image_t &nvx_target,bool f_continue = true){return false;}
    virtual bool open(ffmpeg_cap &cap){return false;}
    virtual ~ffmpeg2Img(){}
};

#endif

/*
 * General interface for handle result frame after processed
 */
class ffmpeg_M2out{
public:
    virtual ~ffmpeg_M2out(){}
    virtual bool open(ffmpeg_2Mat *tsc){return false;}
    virtual bool open(AVCodecID eCodecId,int fps,int width,int height){return false;}
    virtual void* getEncodec(){return nullptr;}
    virtual void* getCodecContext(){return nullptr;}
    virtual void setSizeTarget(int width,int height){}
    virtual void setBitRateTarget(int bit_rate){}
    virtual void setUsingH265(bool f_using){}
//    virtual bool open(ffmpeg_2Mat *tsc, cv::Size img_size_encoder, int bit_rate){return false;}
    virtual bool encode_out(int ret,AVFrame *decoded_frame,std::vector<AVPacket*> &outSet,bool f_continue = true){return false;}
#ifdef _terga_codec_
    virtual bool open(uint width,uint height,uint fps,uint32_t encoder_pixfmt){return false;}
    virtual bool encode_out(nvxcu_pitch_linear_image_t &nvx_i420,std::vector<AVPacket*> &outSet,bool f_continue = true){return false;}
#endif
};

#ifdef using_ffmpeg_3_4_1
static bool initComponents(){
    av_register_all();
    avcodec_register_all();
    avformat_network_init();
    return false;
}
static bool deInitComponents(){
    avformat_network_deinit();
}
#endif

ffmpeg_cap* createFF_cap_rtsp();
ffmpeg_cap* createFF_cap_v1();
ffmpeg_cap* createFF_cap_v2();
ffmpeg_out* createFF_out();
/*
 * Create write to video file object with target time record
 * @Input: timeToSave in minute
 */
ffmpeg_out* createFF_padFrame(uint timeToSave);
/*
 * For internal buffer avpacket
 */
ffmpeg_out* createFF_out_auFps(uint timeToSave);
/*
 * For auto correct duration transcode
 * */
ffmpeg_out* createFF_out_auFps_tc(uint timeToSave);
/*
 * For auto correct duration direct
 * */
ffmpeg_out* createFF_out_auFps_dr(uint timeToSave);

ffmpeg_2Mat* createFF_2Mat();
ffmpeg_out* createFF_out_ts();
/**
 * @brief createFF_out_rtpT: create rtp for transcoder
 * @return
 */
ffmpeg_out* createFF_out_rtpT();
/**
 * @brief createFF_out_rtpM: create rtp for encoder
 * @return
 */
ffmpeg_out* createFF_out_rtpM();
/**
 * @brief createFF_out_rtpTCP: create rtp for encoder over TCP
 * @return
 */
ffmpeg_out* createFF_out_rtpTCP();

ffmpeg_out* createFF_out_pp();
ffmpeg_out* createFF_out_srtpT(const char* key_s);
ffmpeg_transcode* createFF_transcode_pi();
ffmpeg_transcode* createFF_transcode_v2();
ffmpeg_transcode* createFF_transcode_vaapi();
/**
 * @brief createFF_2Mat_v4l2: create pull frame from v4l2 source
 * @return
 */
ffmpeg_2Mat* createFF_2Mat_v4l2();
#ifdef using_hardware_qsv
ffmpeg_2Mat* createFF_2Mat_qsv();
ffmpeg_transcode* createFF_transcode_qsv(int gop_size = 0);
//For handle lost frame
ffmpeg_transcode* createFF_transcode_qsv1(int gop_size = 0);
ffmpeg_transcode* createFF_transcode_qsv_hevc(int gop_size = 0);
ffmpeg_M2out* createFF_postProcess(int gop_size = 0);
//Expect handle both size image, bitrate output hevc encode data
ffmpeg_M2out* createFF_postProcess_v1(int gop_size = 0);
//Using this for multi-configure encoder support
ffmpeg_M2out* createFF_ppStable(int gop_size = 0);
#endif


}
#endif /* S_COMPRESSEDDATAWRAPPER_BOARDCASTIMPLEMENT_HPP_ */
