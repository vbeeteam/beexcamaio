/**
******************************************************************************
* @file           : cam_data_struct.h
* @brief          : Header file of camera data struct
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef CAM_DATA_STRUCT_H
#define CAM_DATA_STRUCT_H

/* Include -------------------------------------------------------------------*/
#include <string>
#include <stdint.h>
#include <stdbool.h>
#include <map>
#include <mutex>
#include "ffmpeg_engine/boardCastImplement.hpp"

#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1
#define BEEX_PATH                               "/home/beex/Templates/app/"
#endif
#ifdef WYZE_CAM_V2
#define BEEX_PATH                               "/sdcard/home/beex/Templates/app/"
#endif

#define BEEX_APP_DIR                                    "camera/cam-app/"
#define BEEX_LOG_DIR                                    "log/"
#define BEEX_CONFIG_DIR                                 "config/"

namespace WebeCamDevState{
    enum CamDevState{
        NOT_SERIAL,
        SERIAL,
        NOT_AUTHEN,
        AUTHEN,
        CONNECTED,
        DISCONNECTED,
        NOT_OWNER,
        OWNER,
        NOT_SYNC,
        SYNC
    };


    enum CamAppState{
        INACTIVE,
        ACTIVE
    };
}

class CamApp{
private:

public:
    CamApp();
    ~CamApp();
    WebeCamDevState::CamAppState active_state;
    std::string host_media;
    std::string port_media;
    

};


class CamServiceId
{
private:
    
public:

    static const uint16_t storage_serv = 10;
    static const uint16_t security_serv = 7;
    static const uint16_t smart_streaming = 12;
    static const uint16_t normal_streaming = 4;
};


class CamStreamZoomState
{
private:
    
public:
    CamStreamZoomState(){

    }
    ~CamStreamZoomState(){

    }
    static const uint8_t ZOOM_STATE_FULL     = 1;
    static const uint8_t ZOOM_STATE_30       = 3;
    static const uint8_t ZOOM_STATE_60       = 6;
};



class CamStreamingInfo
{
private:
    
public:
    CamStreamingInfo(){
        this->is_ffmpeg_state = WebeCamDevState::INACTIVE;
        this->is_qsv_state = WebeCamDevState::INACTIVE;
        this->is_gst_out_state = WebeCamDevState::INACTIVE;
        this->is_relay_state = WebeCamDevState::INACTIVE;
        this->reset_state = WebeCamDevState::INACTIVE;
        //this->consumer1 = boardCastDataProvider::createFF_out_rtpT();
        // this->consumer2 = boardCastDataProvider::createFF_out();

        this->is_consumer1_opened = false;
        this->is_consumer2_opened = false;
        this->is_gst_out_opend = false;
        this->zoom_state = CamStreamZoomState::ZOOM_STATE_30;
        this->zoom_state_pre = CamStreamZoomState::ZOOM_STATE_30;
        this->is_zoom_switching = false;
        this->start_time = 0;
        this->serviceId = 0;
        
    }
    ~CamStreamingInfo(){

    }
    boardCastDataProvider::ffmpeg_out *consumer1;
    boardCastDataProvider::ffmpeg_out *consumer2;

    bool is_consumer1_opened;
    bool is_consumer2_opened;
    bool is_gst_out_opend;
    WebeCamDevState::CamAppState is_qsv_state;
    WebeCamDevState::CamAppState is_ffmpeg_state;
    WebeCamDevState::CamAppState is_gst_out_state;
    WebeCamDevState::CamAppState is_relay_state;
    WebeCamDevState::CamAppState reset_state;
    std::string host_media;
    std::string port_media;
    uint8_t zoom_state;
    uint8_t zoom_state_pre;
    bool is_zoom_switching;
    unsigned long start_time;
    uint16_t serviceId;
};



class CamStorageInfo{
private:
    
public:
    CamStorageInfo();
    ~CamStorageInfo();
    WebeCamDevState::CamAppState active_state;
    uint16_t fps;
    uint32_t time_split;
    std::string resolution;
    std::string origin_path;
    std::string backup_path;
    std::string thumb_path;
};

class CamUploadServ{
private:
    
public:
    CamUploadServ();
    ~CamUploadServ();

    WebeCamDevState::CamAppState active_status;
    std::string baseStorageUrl;
    std::string baseStorageUrlBackup;
    std::string baseStoragePrivateBackupUrl;
    std::string auto_backup;
    std::string type_upload;
    bool is_update;
};

class CamUploadPacket{
private:
    
public:
    CamUploadPacket();
    ~CamUploadPacket();

    std::string action;

    std::string dev_serial;

    std::string name_file;
    std::string path_file_origin;
    std::string path_file_backup;

    std::string path_sub_file;

    std::string time_start;
    uint32_t time_duration;

    std::string extra_info;

    CamUploadServ upload_serv;

    uint16_t fps;
    uint16_t resol_width;
    uint16_t resol_height;
    size_t size;


};


class FrameInfor{

private:


public:
    uint16_t width;
    uint16_t height;
};

class OnvifCamInfor{
private:
    
public:
    OnvifCamInfor();
    ~OnvifCamInfor();

    std::string local_addr;
    std::string name;
    std::string mac_Addr;
    std::string authen_state;
    std::string userName;
    std::string password;
    uint32_t lastTimeUpdate;
};


class CamDevInfor{
private:
    
public:
    CamDevInfor();
    ~CamDevInfor();

    static const uint16_t dev_type = 0x0003;
    static const uint8_t streaming_num_max = 3;

    std::string camera_type;
    std::string mac_addr;
    std::string dev_serial;
    std::string rtsp_url;
    std::string source;
    uint8_t strength;
    uint16_t fps;
    uint16_t resol_width;
    uint16_t resol_height;
    uint32_t bitrate_max;

    WebeCamDevState::CamDevState conn_state;
    WebeCamDevState::CamDevState serial_state;
    WebeCamDevState::CamDevState authen_state;
    WebeCamDevState::CamDevState owner_state;
    uint32_t last_time;
    
    // Service
    bool capture_enable;
    bool is_running_app;
    bool capture_state;
    bool is_running_videosample;

    uint32_t last_time_connected;

    bool is_update_serv;
    uint32_t last_time_update_serv;

    WebeCamDevState::CamAppState is_smart_streaming;

    FrameInfor frameInfor;

    std::map<std::string, CamStreamingInfo> listStreaming;
    std::map<std::string, boardCastDataProvider::ffmpeg_out *> listStreamOut;
    CamStorageInfo storageApp;
    CamUploadServ uploadApp;

    OnvifCamInfor onvifInfor;
    CamStreamingInfo relayStreaming;

};


class DevInforMap{
public:
    DevInforMap();
    ~DevInforMap();
    
    // Attribute
    static const uint8_t cam_num_max = 1;
    std::map<std::string, CamDevInfor> devMap;
    bool change_flag;

    // map API
    bool checkDevMap(std::string dev_serial);
    CamDevInfor getDevMap(std::string dev_serial);
    bool insertDevMap(CamDevInfor dev_infor);
    bool eraseDevMap(std::string dev_serial);
    uint16_t getAllDevMap(CamDevInfor *dev_infor);
private:
    
    std::mutex devMapLocker;
};



/* Public Object and Varial ---------------------------------------------------------*/
extern DevInforMap camInforMap;

#endif
