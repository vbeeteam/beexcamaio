/**
******************************************************************************
* @file           : data_config.h
* @brief          : Header file of camera data config
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef DATA_CONFIG_H
#define DATA_CONFIG_H

#include <string>
#include <mutex>
#include "data_struct/cam_data_struct.h"
#include "share_function/share_function.h"

class CamConfigInfo{
private:
    
public:
    CamConfigInfo(){

    }
    ~CamConfigInfo(){

    }
    //Attribute
    DevInforMap listCamera;

};

class CamConfig{
private:
    std::mutex configLocker;

    std::string path_file;
    std::string config_file;
public:
    CamConfig(){
        this->config_file = "cameraconfig.txt";
        this->path_file = BEEX_PATH +  std::string("camera/configuration/");
        std::string cmd = "mkdir -p " + this->path_file;
        system(cmd.c_str());
        this->path_file += this->config_file;
        if(!BeexShareFunction::checkFile(this->path_file)){
            BeexShareFunction::creatFile(this->path_file);
        }
    }
    ~CamConfig(){

    }
    bool writeConfig(DevInforMap *info);
    bool readConfig(DevInforMap *info);
};

extern CamConfig camConfig;
#endif
