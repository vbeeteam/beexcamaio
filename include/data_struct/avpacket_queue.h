/**
******************************************************************************
* @file           : avpacket_queue.h
* @brief          : Header file of AVPacket Queue
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef AVPACKET_QUEUE_H
#define AVPACKET_QUEUE_H
/* Include ------------------------------------------------------------------*/
#include "ffmpeg_engine/boardCastImplement.hpp"
#include <queue>
#include <mutex>

namespace WebeCamAVPacket{
    class AVPacketQueue{
    private:
        std::queue<AVPacket> avpacket_queue;
        std::queue<int> ret_queue;
        std::mutex locker;
        
    public:
        AVPacketQueue();
        ~AVPacketQueue();
        
        uint16_t queue_size;

        bool pushPacket(AVPacket *pkt, int ret);
        bool getPacket(AVPacket *pkt, int *ret);
        bool clearQueue(void);
        uint16_t getQueueSizeAvailable(void);

    };
    
    
}


#endif
