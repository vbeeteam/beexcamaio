/**
******************************************************************************
* @file           : http_client.h
* @brief          : header file of http client
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifndef HTTP_CLIENT_H
#define HTTP_CLIENT_H

#include "Poco/Net/SSLManager.h"

namespace BeexHttp{
#ifdef INTEL_NUC_BOX    
    class SSLInitializer {
    public:
        SSLInitializer() { Poco::Net::initializeSSL(); }

        ~SSLInitializer() { Poco::Net::uninitializeSSL(); }
    };
#endif
    class HttpClient{
    private:
        
    public:
        HttpClient(){

        }
        ~HttpClient(){

        }

        bool requestHttp(std::string api, std::string *msg);
        bool requestHttps(std::string api, std::string *msg);
    };
}



#endif

