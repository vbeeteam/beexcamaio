/**
******************************************************************************
* @file           : webe_logger.h
* @brief          : header file of Webe logger
******************************************************************************
******************************************************************************
*/
#ifndef WEBE_LOGGER_H
#define WEBE_LOGGER_H

/* Include ------------------------------------------------------------------*/
#include <string>

namespace WebeLogger{
    class Logger{
    private:
        std::string log_path;
        std::string sys_log_file;
        std::string gateway_log_file;

        // methods
        void init(void);
        
    public:
        Logger();
        ~Logger();
        
        // logger
#ifdef INTEL_NUC_BOX        
        std::shared_ptr<spdlog::logger>  sysLogger;
        std::shared_ptr<spdlog::logger> gatewayLogger;
#endif
    };
}


/* object and variable Public ---------------------------------------------------------*/
extern WebeLogger::Logger myLogger;

#endif
