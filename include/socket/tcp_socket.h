/**
******************************************************************************
* @file           : tcp_socket.h
* @brief          : header file of tcp socket
******************************************************************************
******************************************************************************
*/

#ifndef SOCKET_H
#define SOCKET_H

/* Includes ------------------------------------------------------------------*/
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include "mutex"


class TcpConnection
{
public:
	TcpConnection();
	~TcpConnection();
	static const uint16_t packet_len_max = 5000;
	int new_socket;
	int stt_conn;

	int connect(std::string host, uint16_t port);
	int connect(std::string host_port);
	void close(void);
	int init(uint16_t port);
	int init(uint16_t port, uint16_t conn_num);
	TcpConnection accept(void);
	int send(std::string data);
	int send(uint8_t *buff, int len);
	int receive(uint8_t *buff, int len);
	int keepalive(void);
	
private:
	
};

class Socket
{
public:
	Socket();
	~Socket();

	TcpConnection init(void);
	int close(void);

protected:
	int stt_conn;


};


#endif // !SOCKET_H
