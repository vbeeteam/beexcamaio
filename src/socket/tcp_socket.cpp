/**
******************************************************************************
* @file           : socket.cpp
* @brief          : Source file of socket
******************************************************************************
******************************************************************************
*/

#include "socket/tcp_socket.h"
#include <fcntl.h>

TcpConnection Socket::init(void)
{
	this->stt_conn = 0;
	int a = 1;
	TcpConnection obj;
	//unsigned long nonblocking = 1;
	obj.new_socket = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(obj.new_socket, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(int));
	if (obj.new_socket < 0)
	{
		std::cout << "+TCP - Creat new socket: Error" << std::endl;
	}
	else
	{
		std::cout << "+TCP - Creat new socket: OK" << std::endl;
	}
	return obj;
}


int Socket::close()
{
	//close(new_socket);
	return 1;
}

TcpConnection::TcpConnection()
{
	this->stt_conn = 0;
}

TcpConnection::~TcpConnection()
{
	//::close(this->new_socket);
}

int TcpConnection::connect(std::string host, uint16_t port)
{
	struct sockaddr_in serveraddr;
	int conn_result;

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(host.c_str());
	serveraddr.sin_port = htons(port);

	for (uint8_t i = 0; i < 3; i++)
	{
		conn_result =::connect(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
		if (conn_result < 0)
		{
			std::cout << "+TCP - Connect to server: Error" << std::endl;
			std::cout << "+TCP - Try to connect..." << i << std::endl;
		}
		else
		{
			
			std::cout << "+TCP - Connect to server: OK" << std::endl;
			this->stt_conn = 1;
			return 1;
		}
	}
	std::cout << "+TCP - Cannot connect to server...Break!" << std::endl;
	this->stt_conn = 0;
	return 0;
}

int TcpConnection::connect(std::string host_port)
{
	struct sockaddr_in serveraddr;
	int conn_result;
	char host[30] = { 0 };
	std::string port = "";
	int port_convert;
	uint8_t index = 0;
	for (uint8_t i = 0; i < 30; i++)
	{
		if (host_port[i] == ':')
		{
			index = i;
			break;
		}
		host[i] = host_port[i];
	}
	for (uint8_t i = 0; i < 10; i++)
	{
		if (host_port[i + index + 1] == 0)
		{
			break;
		}
		port += host_port[i + index + 1];
	}	
	sscanf(port.c_str(), "%d", &port_convert);
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(host);
	serveraddr.sin_port = htons(port_convert);

	for (uint8_t i = 0; i < 1; i++)
	{
		conn_result =:: connect(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
		if (conn_result < 0)
		{
			std::cout << "+TCP - Connect to server: Error" << std::endl;
			std::cout << "+TCP - Try to connect..." << i << std::endl;
		}
		else
		{
			std::cout << "+TCP - Connect to server: OK" << std::endl;
			stt_conn = 1;
			return 1;
		}
	}
	std::cout << "+TCP - Cannot connect to server...Break!" << std::endl;
	stt_conn = 0;
	return 0;
}

void TcpConnection::close(void)
{
	::close(new_socket);
}

int TcpConnection::init(uint16_t port)
{
	struct sockaddr_in serveraddr;

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(port);
	if (bind(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
	{
		std::cout << "error on Binding" << port << std::endl;
		return 0;
	}
	else
	{
		listen(new_socket, 5);
		std::cout << "waiting to client connect.." << std::endl;
		return 1;
	}
}

int TcpConnection::init(uint16_t port, uint16_t conn_num)
{
	struct sockaddr_in serveraddr;

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(port);
	if (bind(new_socket, (struct sockaddr *)&serveraddr, sizeof(serveraddr)) < 0)
	{
		std::cout << "error on Binding" << port << std::endl;
		return 0;
	}
	else
	{
		listen(new_socket, conn_num);
		std::cout << "waiting to client connect.." << std::endl;
		return 1;
	}
}

TcpConnection TcpConnection::accept(void)
{
	TcpConnection new_conn;
	socklen_t clientlen;
	struct sockaddr_in clientaddr;

	clientlen = sizeof(clientaddr);
	new_conn.new_socket =::accept(new_socket, (struct sockaddr *)&clientaddr, &clientlen);
	if (new_conn.new_socket < 0)
	{
		std::cout << "error on accept" << std::endl;
	}
	else
	{
		new_conn.stt_conn = 1;
		std::cout << "new client connected" << std::endl;
	}
	return new_conn;
}

int TcpConnection::send(std::string data)
{
	if (!stt_conn)
	{
		std::cout << "+TCP - Cannot send data" << std::endl;
		return 0;
	}
	else
	{
		if (::send(new_socket, data.c_str(), data.length(), 0) < 0)
		{
			std::cout << "+TCP - Send data: Error" << std::endl;
			stt_conn = 0;
			return 0;
		}
		else
		{
			return 1;
		}
	}
}

int TcpConnection::send(uint8_t *buff, int len)
{
	if (!stt_conn)
	{
		std::cout << "+TCP - Cannot send data" << std::endl;
		return 0;
	}
	else
	{
		if (::send(new_socket, buff, len, 0) < 0)
		{
			std::cout << "+TCP - Send data: Error" << std::endl;
			return 0;
		}
		else
		{
			//std::cout << "Send data: OK" << std::endl;
			return 1;
		}
	}
}

int TcpConnection::receive(uint8_t *buff, int len)
{
	int result;
	while (stt_conn)
	{
		result = read(new_socket, buff, len);
		if (result < 0)
		{
			std::cout << "+TCP - error on receive data" << std::endl;
			stt_conn = 0;
			return 0;
		}
		else if (result == 0)
		{
			std::cout << "+TCP - connecttion closed." << std::endl;
			stt_conn = 0;
			return 0;
		}
		//std::cout << "Received data: " << buff << "len" << result << std::endl;
		return result;
	}
	return -1;
}


int TcpConnection::keepalive(void)
{
	int get_value;
	socklen_t get_value_len;

	get_value = 1;
	get_value_len = sizeof(get_value);
	if (setsockopt(new_socket, SOL_SOCKET, SO_KEEPALIVE, &get_value, get_value_len) < 0)
	{
		std::cout << "error on setsockopt" << std::endl;
	}
	else
	{
		std::cout << "SO_KEEPALIVE set on socket" << std::endl;
		get_value = 10;
		get_value_len = sizeof(get_value);
		if (setsockopt(new_socket, IPPROTO_TCP, TCP_KEEPIDLE, &get_value, get_value_len) < 0)
		{
			std::cout << "error on set keepalive time" << std::endl;
		}
		else
		{
			std::cout << "set keepalive time : ok" << std::endl;
		}

	}
	return 0;
}


Socket::Socket()
{
}

Socket::~Socket()
{
}

