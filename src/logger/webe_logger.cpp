/**
******************************************************************************
* @file           : webe_logger.cpp
* @brief          : source file of Webe logger
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -----------------------------------------------------------------*/
#include "logger/webe_logger.h"
#include "data_struct/cam_data_struct.h"

WebeLogger::Logger myLogger;
using namespace WebeLogger;

Logger::Logger(){
    this->log_path = BEEX_PATH +  std::string("log/");
    std::string cmd = "mkdir -p " + this->log_path;
    system(cmd.c_str());    
    this->sys_log_file = "cam_logger";
    this->init();
}

Logger::~Logger(){
}

void Logger::init(void){
    //this->sysLogger = spdlog::rotating_logger_mt(this->sys_log_file, this->log_path + this->sys_log_file + ".txt", 1048576 * 50, 20);
    //spdlog::set_default_logger(this->sysLogger);
    //spdlog::flush_on(spdlog::level::info);
}