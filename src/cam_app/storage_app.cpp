/**
******************************************************************************
* @file           : storage_app.cpp
* @brief          : Source file of camera Storage application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_app/storage_app.h"
#include "logger/webe_logger.h"
#include "ffmpeg_engine/boardCastImplement.hpp"
#include "my_func/os_func.h"
#include <iostream>
#include <unistd.h>

CamStorageApp::CamStorageApp(){
    this->is_running = false;
    this->is_stop = false;
    this->storage_path = BEEX_PATH + std::string("storage/");
    this->time_check_ffmpeg = 0;
#ifdef WYZE_CAM_V2
    this->timeout_storage = OSFunc::getTimestamp();
#endif
}

CamStorageApp::~CamStorageApp(){
}


// int CamStorageApp::runFfmpeg(CamDevInfor *cam_info){
//     //myLogger.sysLogger->info("Starting Camera Storage App use FFmpeg .... OK!");

//     boardCastDataProvider::ffmpeg_remux_v1 test_rm;

//     while (1){
//         usleep(10000);
//         this->is_stop = false;
//         while (1){
//             if(this->is_stop == true){
//                 this->is_running = false;
//                 //myLogger.sysLogger->info("Stop Camera Storage App use FFmpeg .... OK!");
//                 return -1;
//             }            
//             if(cam_info->storageApp.active_state == WebeCamDevState::ACTIVE){
//                 //myLogger.sysLogger->info("Start Storage App for camera {0} - {1}", cam_info->mac_addr, cam_info->dev_serial);
//                 this->is_running = true;
//                 break;
//             }
//             usleep(300000);
//         }      


//         this->initDir(cam_info);
    
//         uint32_t start_time = OSFunc::getTimestamp();
//         uint32_t dur_time = 0;

//         uint32_t time_stamp_debug = OSFunc::getTimestamp();
//         uint8_t fps = 0;
        
//         std::string input_link = cam_info->rtsp_url;
//         std::string output_link = cam_info->storageApp.origin_path + cam_info->dev_serial + "_" + std::to_string(start_time) + "_0" + ".mp4";
//         std::string thumb_file = cam_info->storageApp.thumb_path + cam_info->dev_serial + "_" + std::to_string(start_time) + "_0" + ".jpg";
        
//         //myLogger.sysLogger->info("creat path: {0}", output_link);        
//         try{
//             if(test_rm.open(input_link.c_str(),output_link.c_str())){
//                 int count_t = 0;
//                 //myLogger.sysLogger->info("Open Storage App for camera {0} - {1}, storage path = {2}", cam_info->mac_addr, cam_info->dev_serial, output_link);
//                 this->saveThumbnail(cam_info, thumb_file);
//                 while(test_rm.remux() == true){
//                     count_t += 1;
//                     fps++;
//                     if(cam_info->storageApp.active_state == WebeCamDevState::INACTIVE){
//                         this->is_stop = true;
//                         //myLogger.sysLogger->info("stop storage by inactive");
//                         break;
//                     }
//                     if(OSFunc::getTimestamp() - start_time >= 30){
//                         //myLogger.sysLogger->info("stop storage by expire");
//                         break;
//                     }
//                     if(this->is_stop == true){
//                         //myLogger.sysLogger->info("stop storage by is_stop");
//                         break;
//                     }

//                     uint32_t get_time_current = OSFunc::getTimestamp();
//                     if(get_time_current - time_stamp_debug >= 2){
//                         time_stamp_debug = get_time_current;
//                         //myLogger.sysLogger->info("debug in storage camera {0} : fps = {1}", cam_info->dev_serial, std::to_string(fps));
//                         fps = 0;
//                     }                    
//                 }
//                 test_rm.remux(false);

//                 uint32_t dur_time = OSFunc::getTimestamp() - start_time;
//                 std::string file_save = cam_info->storageApp.origin_path + cam_info->dev_serial + "_" + std::to_string(start_time) + "_" + std::to_string(dur_time) + ".mp4";
//                 //myLogger.sysLogger->info("storage summarize for camera = {0}: file save = {1}", cam_info->dev_serial, file_save);
//                 rename(output_link.c_str() , file_save.c_str() );           
//             }
//             else{
//                 //myLogger.sysLogger->error("Open Storage App for camera {0} - {1} : error", cam_info->mac_addr, cam_info->dev_serial);
//                 usleep(500000);
//             }
//         }
//         catch(const std::exception& e){
//             std::string get_except(e.what());
//             //myLogger.sysLogger->error("exception in storage: {0}", get_except);
//         }
//     }
// }



int CamStorageApp::runFfmpegStorage(CamDevInfor *cam_info, boardCastDataProvider::ffmpeg_cap *producer, bool *cap_state){
    this->time_check_ffmpeg = OSFunc::getTimestamp();
    std::cout << "runFfmpegStorage - Starting Storage App" << std::endl;
    //myLogger.sysLogger->info("runFfmpegStorage - Starting Storage App for camera = {0}, pid = {1}", cam_info->dev_serial, std::to_string(getpid()));
    this->is_stop = false;
    this->is_running = true;
    try{
        boardCastDataProvider::ffmpeg_out *consumer1 = boardCastDataProvider::createFF_out();
        //assert(consumer1 != NULL);
        AVPacket packet;
        memset(&packet, 0, sizeof(packet));
        av_init_packet(&packet);           
        while (1){
            usleep(10000);           
            while (1){
                this->time_check_ffmpeg = OSFunc::getTimestamp();
                if(this->is_stop == true){
                    this->is_running = false;
                    std::cout << "runFfmpegStorage - Stop Storage App by is_stop" << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Stop Storage App for camera = {0}", cam_info->dev_serial);
                    if(consumer1 != NULL){
                        delete consumer1;
                        consumer1 = NULL;
                    }
                    return -1;
                }            
                if(cam_info->storageApp.active_state == WebeCamDevState::ACTIVE){
                    //myLogger.sysLogger->info("runFfmpegStorage - Active Storage App for camera = {0} ", cam_info->dev_serial);
                    std::cout << "runFfmpegStorage - Active Storage App" << std::endl;
                    break;
                }
                usleep(300000);
            }      


            this->initDir(cam_info);
        
            uint32_t start_time = OSFunc::getTimestamp();

            // uint32_t time_stamp_debug = OSFunc::getTimestamp();
            uint16_t fps = 0;
            std::string output_link = cam_info->storageApp.origin_path + cam_info->dev_serial + "_" + std::to_string(start_time) + "_0" + ".mp4";
            std::string thumb_file = cam_info->storageApp.thumb_path + cam_info->dev_serial + "_" + std::to_string(start_time) + "_0" + ".jpg";

            while (*cap_state == false){
                this->time_check_ffmpeg = OSFunc::getTimestamp();
                if(this->is_stop == true){
                    this->is_running = false;
                    std::cout << "runFfmpegStorage - AcStoptive Storage App by is stop" << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Stop Storage App for camera = {0}", cam_info->dev_serial);
                    if(consumer1 != NULL){
                        delete consumer1;
                        consumer1 = NULL;
                    }                    
                    return -1;
                }
                usleep(1000);
            }
            // save thumbnail
            this->saveThumbnail(cam_info, thumb_file);            
            if(consumer1->open(output_link.c_str(), *producer) == false){
                this->time_check_ffmpeg = OSFunc::getTimestamp();
                std::cout << "runFfmpegStorage - Error open file = " << output_link << std::endl;
                //myLogger.sysLogger->error("runFfmpegStorage - Error Open file Storage App for camera = {0}, storage path = {1}", cam_info->dev_serial, output_link);
                return -1;
            }
            //myLogger.sysLogger->info("runFfmpegStorage - Open file Storage App for camera = {0}, storage path = {1}", cam_info->dev_serial, output_link);            
            uint32_t time_stamp = OSFunc::getTimestamp();
            this->time_check_ffmpeg = time_stamp;

            int ret;
            uint32_t check_size_storage = 0;
            while (1){
                this->time_check_ffmpeg = OSFunc::getTimestamp();
#ifdef WYZE_CAM_V2
                this->timeout_storage = OSFunc::getTimestamp();
#endif
                if(this->avPacketQueue.getPacket(&packet, &ret) == true){
                    bool flag_continue = true;

                    flag_continue = consumer1->write_data(ret, packet, flag_continue);
                    if(flag_continue == false){
                        std::cout << "runFfmpegStorage - Stop Storage App by write file error" << std::endl;
                        //myLogger.sysLogger->error("runFfmpegStorage - Stop Storage App by write file error for camera = {0}, file path = {1}", cam_info->dev_serial, output_link);
                        break;
                    }
                    else{
                        fps++;
                        check_size_storage += packet.size;
                    }
                    av_packet_unref(&packet);                                 
                }
                // check capture statue for sync
                if(*cap_state == false){
                    std::cout << "runFfmpegStorage - Stop Storage App by cature error" << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Stop Storage App by capture thread for camera = {0}", cam_info->dev_serial);
                    break;                    
                }
                if(cam_info->storageApp.active_state == WebeCamDevState::INACTIVE){
                    this->is_stop = true;
                    std::cout << "runFfmpegStorage - stop Storage App by inactive" << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Stop Storage App by inactive for camera = {0}", cam_info->dev_serial);
                    break;
                }
                if(OSFunc::getTimestamp() - start_time >= cam_info->storageApp.time_split){
                    std::cout << "runFfmpegStorage - stop Storage App by split" << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Stop Storage App by split time for camera = {0}", cam_info->dev_serial);
                    break;
                }
                if(this->is_stop == true){
                    std::cout << "runFfmpegStorage - stop Storage App by is stop" << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Stop Storage App by stop app time for camera = {0}", cam_info->dev_serial);
                    break;
                }
                if(OSFunc::getTimestamp() - time_stamp >= 10){
                    time_stamp = OSFunc::getTimestamp();
                    std::cout << "runFfmpegStorage - check Storage App, size = " << std::to_string(check_size_storage/10000) << std::endl;
                    //myLogger.sysLogger->info("runFfmpegStorage - Check Storage for camera {0} : fps = {1} : size = {2}", cam_info->dev_serial, std::to_string(fps), std::to_string(check_size_storage/10000));
                    fps = 0; 
                    check_size_storage = 0;                       
                }
                usleep(1000);
            }
            uint32_t dur_time = OSFunc::getTimestamp() - start_time;
            consumer1->write_data(ret, packet,false);
            std::string file_save = cam_info->storageApp.origin_path + cam_info->dev_serial + "_" + std::to_string(start_time) + "_" + std::to_string(dur_time) + ".mp4";
            //myLogger.sysLogger->info("runFfmpegStorage - storage App summarize for camera = {0}: file save = {1}", cam_info->dev_serial, file_save);
            std::cout << "runFfmpegStorage - storage App summarize = " << file_save << std::endl;
            rename(output_link.c_str() , file_save.c_str() );
            av_packet_unref(&packet); 
            
        }
    }
    catch(const std::exception& e){
        std::string get_except(e.what());
        //myLogger.sysLogger->error("runFfmpegStorage - exception in storage: {0}", get_except);
    }
}


bool CamStorageApp::initDir(CamDevInfor *cam_info){
    // creat direction for origin
    std::string path_storage_origin = this->storage_path + cam_info->dev_serial + "/" + "origin/";
    std::string cmd = "mkdir -p " + path_storage_origin;
    system(cmd.c_str());
    // creat direction for backup
    std::string path_storage_backup = this->storage_path + cam_info->dev_serial + "/" + "backup/";
    cmd = "mkdir -p " + path_storage_backup;
    system(cmd.c_str());  

    // creat direction for thumb

    std::string path_storage_thumb = this->storage_path + cam_info->dev_serial + "/" + "thumb/";
    cmd = "mkdir -p " + path_storage_thumb;
    system(cmd.c_str());

    if(camInforMap.checkDevMap(cam_info->dev_serial) == true){
        *cam_info = camInforMap.getDevMap(cam_info->dev_serial);
        cam_info->storageApp.thumb_path = path_storage_thumb;       
        cam_info->storageApp.backup_path = path_storage_backup;
        cam_info->storageApp.origin_path = path_storage_origin;
        camInforMap.insertDevMap(*cam_info); 
    }
}



bool CamStorageApp::saveThumbnail(CamDevInfor *cam_info, std::string path){

    // cv::VideoCapture cap(cam_info->rtsp_url);
    // if(!cap.isOpened()){
    //     //myLogger.sysLogger->error("open Capture thumb of video for camera = {0} for save thumbnail: error", cam_info->dev_serial);
    //     cap.release();
    //     return false;
    // }  

    // uint8_t time_out = 0;
    // while (1){
    //     try{
    //         cv::Mat frame;
    //         cap.read(frame);
    //         if(!frame.empty()){
    //             if(camInforMap.checkDevMap(cam_info->dev_serial) == true){
    //                 *cam_info = camInforMap.getDevMap(cam_info->dev_serial);
    //                 cam_info->resol_width = frame.size().width;
    //                 cam_info->resol_height = frame.size().height;      
    //                 camInforMap.insertDevMap(*cam_info);
    //             }
    //             cv::imwrite(path, frame);
    //             cap.release();
    //             frame.release();
    //             //myLogger.sysLogger->info("Capture thumb of video for camera = {0} for save thumbnail: success", cam_info->dev_serial);
    //             return true;
    //         }
    //         time_out++;
    //         if(time_out > 100){
    //             cap.release();
    //             frame.release();
    //             return false;
    //         }
    //         usleep(10000);        
    //     }
    //     catch(const std::exception& e){
    //         std::cerr << e.what() << '\n';
    //     }        
    // }
    return true;
}