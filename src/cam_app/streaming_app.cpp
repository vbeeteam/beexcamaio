/**
******************************************************************************
* @file           : streaming_app.cpp
* @brief          : Source file of camera streaming application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_app/streaming_app.h"
#include <unistd.h>
#include "my_func/os_func.h"
#include "logger/webe_logger.h"
#include "ffmpeg_engine/boardCastImplement.hpp"
#include "my_func/os_func.h"
#include "data_struct/data_config.h"


CamStreamingApp::CamStreamingApp(){
    this->is_stop = false;
    this->is_running = false;
    this->avPacketQueue.queue_size = 80;
}

CamStreamingApp::~CamStreamingApp(){
}


int CamStreamingApp::MultiChanelFfmpegStreaming(CamDevInfor *camInfo, boardCastDataProvider::ffmpeg_cap *producer, bool *cap_state){
    //myLogger.sysLogger->info("MultiChanelFfmpegStreaming...");
    this->is_stop = false;
    AVPacket packet;
    AVPacket key_frame;
    int key_frame_ret = 0;
    bool flag_continue = true;
    memset(&packet, 0, sizeof(packet));
    memset(&packet, 0, sizeof(key_frame));
    av_init_packet(&packet);
    av_init_packet(&key_frame);
    CamDevInfor cam_info;
    cam_info.dev_serial = camInfo->dev_serial;

    uint32_t time_debug = 0;
    CamStreamingInfo streamingInfo[CamDevInfor::streaming_num_max];
    bool is_fist_start = true;
 
    while (1){
        // get list streaming

        
        uint8_t streaming_num = 0;
        std::map<std::string, CamStreamingInfo>::iterator map;   
        cam_info = camInforMap.getDevMap(cam_info.dev_serial);

	    for (map = cam_info.listStreaming.begin(); map != cam_info.listStreaming.end(); map++) {
            streamingInfo[streaming_num] = map->second;
            if(is_fist_start){
                //streamingInfo[streaming_num].is_consumer2_opened = false;
                //streamingInfo[streaming_num].consumer2 = boardCastDataProvider::createFF_out();
            }
            streaming_num++;
            if(streaming_num > CamDevInfor::streaming_num_max){
                break;
            }
        }
        is_fist_start = false;

        time_debug++;

        // check open streaming
        bool check_ffmpeg = false;
        for (uint8_t i = 0; i < streaming_num; i++){
            if(time_debug >= 1000){
                time_debug = 0;
                //myLogger.sysLogger->info("MultiChanelFfmpegStreaming - streaming number = {0} - state = {1} - is_opened = {2} - camera = {3}", std::to_string(streaming_num), std::to_string(streamingInfo[i].is_ffmpeg_state), std::to_string(streamingInfo[i].is_consumer2_opened), cam_info.dev_serial);

            }
            if(streamingInfo[i].is_ffmpeg_state == WebeCamDevState::ACTIVE){
                check_ffmpeg = true;
                if((streamingInfo[i].is_consumer2_opened == false) && (*cap_state == true)){
                    std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                    if(cam_info.listStreamOut.find(key) == cam_info.listStreamOut.end()){
                        cam_info.listStreamOut[key] = boardCastDataProvider::createFF_out();
                    }
                    std::string output_link = "rtp://" + streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                    if(cam_info.listStreamOut[key]->open(output_link.c_str(), *producer, 1) == true){
                        //myLogger.sysLogger->info("MultiChanelFfmpegStreaming - open streaming for camera = {0}, outputLink = {1} : success", cam_info.dev_serial, output_link);
                        streamingInfo[i].is_consumer2_opened = true;
                        cam_info.listStreamOut[key]->write_data(key_frame_ret, key_frame);
                        streamingInfo[i].start_time = OSFunc::getTimestamp();
                        
                        // upddate to list
                        
                        if(camInforMap.checkDevMap(cam_info.dev_serial)){
                            CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                            camera.listStreaming[key] = streamingInfo[i];
                            camera.listStreamOut = cam_info.listStreamOut;
                            camInforMap.insertDevMap(camera);
                        }
                    }
                    else{
                        //myLogger.sysLogger->error("MultiChanelFfmpegStreaming - open streaming for camera = {0}, outputLink = {1} : error", cam_info.dev_serial, output_link);
                    }
                }
            }
            else if((streamingInfo[i].is_qsv_state == WebeCamDevState::INACTIVE) && (streamingInfo[i].is_ffmpeg_state == WebeCamDevState::INACTIVE)){
                //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - stop streaming by inactive = {0}:{1}", streamingInfo[i].host_media, streamingInfo[i].port_media);
                // upddate to list
                std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                if(camInforMap.checkDevMap(cam_info.dev_serial)){
                    CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                    camera.listStreaming.erase(key);
                    camInforMap.insertDevMap(camera);
                }
            }
            else if(streamingInfo[i].is_ffmpeg_state == WebeCamDevState::INACTIVE){
                if(streamingInfo[i].is_consumer2_opened == true){
                    streamingInfo[i].is_consumer2_opened = false;
                    // upddate to list
                    std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                    if(camInforMap.checkDevMap(cam_info.dev_serial)){
                        CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                        camera.listStreaming[key] = streamingInfo[i];
                        camInforMap.insertDevMap(camera);
                    }
                }
            }             
        }


        // write streaming
        int ret;
        if(check_ffmpeg){
            if(this->avPacketQueue.getPacket(&packet, &ret)){
                // get key frame
                // if(packet.flags&AV_PKT_FLAG_KEY == 1){
                //     av_packet_unref(&key_frame);
                //     av_copy_packet(&key_frame, &packet);
                // }

                for (uint8_t i = 0; i < streaming_num; i++){
                    if((streamingInfo[i].is_ffmpeg_state == WebeCamDevState::ACTIVE) && (streamingInfo[i].is_consumer2_opened == true)){
                        std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                        if(cam_info.listStreamOut[key]->write_data(ret, packet, flag_continue) == false){
                            //myLogger.sysLogger->error("MultiChanelFfmpegStreaming - write packet to streaming = {0}:{1} : false", streamingInfo[i].host_media, streamingInfo[i].port_media);
                            // upddate to list
                            std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                            if(camInforMap.checkDevMap(cam_info.dev_serial)){
                                CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                                CamStreamingInfo streaming = camera.listStreaming[key];
                                streaming.is_consumer2_opened = false;
                                camera.listStreaming[key] = streaming;
                                camInforMap.insertDevMap(camera);
                            }
                        }
                    }
                    if(*cap_state == false){
                        //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - reset streaming by captureState = {0}:{1} : false", streamingInfo[i].host_media, streamingInfo[i].port_media);
                        // upddate to list
                        std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                        if(camInforMap.checkDevMap(cam_info.dev_serial)){
                            CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                            CamStreamingInfo streaming = camera.listStreaming[key];
                            streaming.is_consumer2_opened = false;
                            camera.listStreaming[key] = streaming;
                            camInforMap.insertDevMap(camera);
                        }
                    }

                    if(OSFunc::getTimestamp() - streamingInfo[i].start_time > 1800){
                        //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - reset streaming by period = {0}:{1} : false", streamingInfo[i].host_media, streamingInfo[i].port_media);
                        // upddate to list
                        std::string key = streamingInfo[i].host_media + ":" + streamingInfo[i].port_media;
                        if(camInforMap.checkDevMap(cam_info.dev_serial)){
                            CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                            CamStreamingInfo streaming = camera.listStreaming[key];
                            streaming.is_consumer2_opened = false;
                            streaming.start_time = OSFunc::getTimestamp();
                            camera.listStreaming[key] = streaming;
                            camInforMap.insertDevMap(camera);
                        }
                    }
                }
                av_packet_unref(&packet);
            }
        }

        if(this->is_stop == true){
            this->is_running = false;
            //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - for camera = {0}: stop", cam_info.dev_serial);
            return -1;
        }           
        
        usleep(5000);


    } 
}



int CamStreamingApp::RtspRelayStreaming(CamDevInfor *camInfo, boardCastDataProvider::ffmpeg_cap *producer, bool *cap_state){
    //myLogger.sysLogger->info("MultiChanelFfmpegStreaming...");
    this->is_stop = false;
    AVPacket packet;
    AVPacket key_frame;
    int key_frame_ret = 0;
    bool flag_continue = true;
    memset(&packet, 0, sizeof(packet));
    memset(&packet, 0, sizeof(key_frame));
    av_init_packet(&packet);
    av_init_packet(&key_frame);
    CamDevInfor cam_info;
    cam_info.dev_serial = camInfo->dev_serial;

    uint32_t time_debug = 0;
    bool is_fist_start = true;
 
    while (1){
        cam_info = camInforMap.getDevMap(camInfo->dev_serial);

	    if(is_fist_start){
            if(camInforMap.checkDevMap(cam_info.dev_serial)){
                CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                camera.relayStreaming.is_consumer2_opened = false;
                camera.relayStreaming.consumer2 = boardCastDataProvider::createFF_out_rtpTCP();
                camInforMap.insertDevMap(camera);
                is_fist_start = false;
            }
        }
        time_debug++;

        // check open streaming
        bool check_ffmpeg = false;
            if(time_debug >= 1000){
                time_debug = 0;
                //myLogger.sysLogger->info("MultiChanelFfmpegStreaming - streaming number = {0} - state = {1} - is_opened = {2} - camera = {3}", std::to_string(streaming_num), std::to_string(streamingInfo[i].is_ffmpeg_state), std::to_string(streamingInfo[i].is_consumer2_opened), cam_info.dev_serial);

            }
            if(cam_info.relayStreaming.is_relay_state == WebeCamDevState::ACTIVE){
                check_ffmpeg = true;
                if((cam_info.relayStreaming.is_consumer2_opened == false) && (*cap_state == true)){
                    std::string output_link = "rtsp://" + cam_info.relayStreaming.host_media + ":" + cam_info.relayStreaming.port_media + "/mystream";
                    if(cam_info.relayStreaming.consumer2->open(output_link.c_str(), *producer, 1) == true){
                        //myLogger.sysLogger->info("MultiChanelFfmpegStreaming - open streaming for camera = {0}, outputLink = {1} : success", cam_info.dev_serial, output_link);
                        cam_info.relayStreaming.is_consumer2_opened = true;
                        cam_info.relayStreaming.consumer2->write_data(key_frame_ret, key_frame);
                        cam_info.relayStreaming.start_time = OSFunc::getTimestamp();
                        
                        // upddate to list
                        if(camInforMap.checkDevMap(cam_info.dev_serial)){
                            CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                            camera.relayStreaming = cam_info.relayStreaming;
                            camInforMap.insertDevMap(camera);
                        }
                    }
                    else{
                        //myLogger.sysLogger->error("MultiChanelFfmpegStreaming - open streaming for camera = {0}, outputLink = {1} : error", cam_info.dev_serial, output_link);
                        sleep(5);
                    }
                }
            }
            else if(cam_info.relayStreaming.is_relay_state == WebeCamDevState::INACTIVE){
                if(cam_info.relayStreaming.is_consumer2_opened == true){
                    cam_info.relayStreaming.is_consumer2_opened = false;
                    // upddate to list
                    if(camInforMap.checkDevMap(cam_info.dev_serial)){
                        CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                        camera.relayStreaming = cam_info.relayStreaming;
                        camInforMap.insertDevMap(camera);
                    }
                }
            }             


        // write streaming
        int ret;
        if(check_ffmpeg){
            if(this->avPacketQueue.getPacket(&packet, &ret)){
                    if((cam_info.relayStreaming.is_relay_state == WebeCamDevState::ACTIVE) && (cam_info.relayStreaming.is_consumer2_opened == true)){
                        if(cam_info.relayStreaming.consumer2->write_data(ret, packet, flag_continue) == false){
                            //myLogger.sysLogger->error("MultiChanelFfmpegStreaming - write packet to streaming = {0}:{1} : false", streamingInfo[i].host_media, streamingInfo[i].port_media);
                            // upddate to list
                            if(camInforMap.checkDevMap(cam_info.dev_serial)){
                                CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                                camera.relayStreaming.is_consumer2_opened = false;
                                camInforMap.insertDevMap(camera);
                            }
                        }
                    }
                    if(*cap_state == false){
                        //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - reset streaming by captureState = {0}:{1} : false", streamingInfo[i].host_media, streamingInfo[i].port_media);
                        // upddate to list
                        if(camInforMap.checkDevMap(cam_info.dev_serial)){
                            CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                            camera.relayStreaming.is_consumer2_opened = false;
                            camInforMap.insertDevMap(camera);
                        }
                    }

                    if(OSFunc::getTimestamp() - cam_info.relayStreaming.start_time > 1800){
                        //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - reset streaming by period = {0}:{1} : false", streamingInfo[i].host_media, streamingInfo[i].port_media);
                        // upddate to list
                        if(camInforMap.checkDevMap(cam_info.dev_serial)){
                            CamDevInfor camera = camInforMap.getDevMap(cam_info.dev_serial);
                            camera.relayStreaming.is_consumer2_opened = false;
                            camera.relayStreaming.start_time = OSFunc::getTimestamp();
                            camInforMap.insertDevMap(camera);
                        }
                    }
                av_packet_unref(&packet);
            }
        }

        if(this->is_stop == true){
            this->is_running = false;
            //myLogger.sysLogger->warn("MultiChanelFfmpegStreaming - for camera = {0}: stop", cam_info.dev_serial);
            return -1;
        }           
        
        usleep(5000);


    } 
}