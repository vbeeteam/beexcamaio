/**
******************************************************************************
* @file           : upload_app.cpp
* @brief          : Source file of camera upload application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/

/* Include ------------------------------------------------------------------*/
#include "cam_app/upload_app.h"
#include "my_func/os_func.h"
#include "logger/webe_logger.h"
#ifdef INTEL_NUC_BOX
#include <Poco/StreamCopier.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>
#include "Poco/JSON/Array.h"
#include "Poco/JSON/Parser.h"
#include "Poco/Dynamic/Var.h"
using namespace Poco::Net;
using namespace Poco;
#endif
#include <thread>
#include <iostream>
#include <dirent.h>
#include <unistd.h>

#include "cam_center/cam_upload_worker.h"

#ifdef WYZE_CAM_V2
#include "rapidjson/document.h"
#include "rapidjson/writer.h"

using namespace rapidjson;
#endif

CamUploadApp::CamUploadApp(){
    this->is_running = false;
    this->is_stop = false;
}

CamUploadApp::~CamUploadApp(){
}


int CamUploadApp::run(CamDevInfor *cam_info){
    // myLogger.sysLogger->info("Start Upload App for camera: serial = {0}", cam_info->dev_serial);
    std::cout << "CamUploadApp::run - start upload app" << std::endl;
    this->is_stop = false;
    this->is_running = true;
    CamUploadWorker camUploadWorker;
    std::thread CamUploadWorkerThread(&CamUploadWorker::run, &camUploadWorker);
    CamUploadWorkerThread.detach();
    while (1){
        DIR *folder;
        struct dirent *entry;
        std::string file_up = "";
        std::string thumb_up = "";
        uint16_t i = 0;
        uint32_t time_check = OSFunc::getTimestamp();
        uint16_t file_num = 0;

        folder = opendir(cam_info->storageApp.origin_path.c_str());
        if(folder != NULL){
            uint32_t timeout = 0;
            while (entry=readdir(folder)){
                usleep(1000);
                timeout++;
                if(timeout > 10000){
                    std::cout << "CamUploadApp::run - timeout in check file upload" << std::endl;
                    // myLogger.sysLogger->info("timeout in check file upload for camera = {0}", cam_info->dev_serial);
                    break;
                }
                std::string file_name(entry->d_name);
                std::string spl[10];
                if(OSFunc::splitString(file_name, "_", spl) > 0){
                    std::string serial = spl[0];
                    if(cam_info->dev_serial == serial){
                        uint32_t get_time = atoi(spl[1].c_str());
                        if(get_time < time_check){
                            time_check = get_time;
                            file_up = file_name;

                            // get thumb file
                            std::string spl[10];
                            OSFunc::splitString(file_up, "_", spl);
                            std::string start_time = spl[1];
                            thumb_up = cam_info->dev_serial + "_" + start_time + "_0.jpg";
                        }
                        i++;
                    }
                }
            }
            //myLogger.sysLogger->info("total file in direction: {0}", std::to_string(i));
            if(i > 1){
                // request upload serv info
                usleep(100000);
                //myLogger.sysLogger->info("Starting Upload file = {0}", file_up);
                std::cout << "CamUploadApp::run - Starting Upload file = " << file_up << std::endl;

                CamUploadPacket packet;
                std::string file_name_spl[10];
                OSFunc::splitString(file_up, "_", file_name_spl);
                unsigned long long time_convert = atoi(file_name_spl[1].c_str());
                time_convert = time_convert * 1000;
                std::string time_start = std::to_string(time_convert);
                int time_duration = atoi(file_name_spl[2].c_str());                

                packet.dev_serial = cam_info->dev_serial;
                packet.name_file = file_up;
                packet.path_file_origin = cam_info->storageApp.origin_path + file_up;
                packet.path_file_backup = cam_info->storageApp.backup_path + file_up;
                packet.time_start = time_start;
                packet.time_duration = time_duration;
                packet.action = camUploadWorker.record_video;
                packet.path_sub_file = cam_info->storageApp.thumb_path + thumb_up;
                
                packet.upload_serv = cam_info->uploadApp;

                packet.fps = cam_info->fps;
                packet.resol_height = cam_info->resol_height;
                packet.resol_width = cam_info->resol_width;

                // extra-info
                #ifdef INTEL_NUC_BOX
                JSON::Object object;
                object.set("duration",time_duration);
                object.set("size", OSFunc::getSizeFile(packet.path_file_origin));
                object.set("fps", packet.fps);
                std::stringstream extra_info;
                object.stringify(extra_info);

                packet.extra_info = extra_info.str(); 
                #endif
                
                #ifdef WYZE_CAM_V2
                StringBuffer jsonBuffer;
                Writer<StringBuffer> writer(jsonBuffer);
                std::string jsonOut;

                writer.StartObject();
                writer.Key("duration");
                writer.Uint64(time_duration);
                writer.Key("size");
                writer.Uint64(OSFunc::getSizeFile(packet.path_file_origin));
                writer.Key("fps");
                writer.Uint(packet.fps);
                writer.EndObject();

                jsonOut = jsonBuffer.GetString();
                packet.extra_info = jsonOut;
                #endif
                
                camUploadWorker.pushPacket(&packet);
            }
        }
        if(i <= 1){
            if(this->is_stop == true){
                std::cout << "CamUploadApp::run - Stop Upload App" << std::endl;
                // myLogger.sysLogger->info("Stop Upload App for camera: serial = {0}", cam_info->dev_serial);
                closedir(folder);
                this->is_running = false;
                camUploadWorker.is_stop = true;
                return -1;
            }
        }

        closedir(folder);
        sleep(1);
    }
}