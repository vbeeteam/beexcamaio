/**
******************************************************************************
* @file           : image_cap.cpp
* @brief          : Source file of capture image
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/

#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

#include <iostream>
#include "cam_app/image_cap.h"

using namespace BeexCamApp;



int ImageCapture::xioctl(int fd, int request, void *arg){
  int r;

  do r = ioctl (fd, request, arg);
  while (-1 == r && EINTR == errno);

  return r;
}

bool ImageCapture::initDev(void){
  this->fd = open(this->v4l2Dev.c_str(), O_RDWR);
  if(this->fd < 0){
    std::cout << "ImageCapture::initDev - open dev error" << std::endl;
    return false;
  }

  struct v4l2_capability caps = {};
  if (-1 == xioctl(this->fd, VIDIOC_QUERYCAP, &caps)){
    std::cout << "ImageCapture::initDev - Querying Capabilities error" << std::endl;
    return false;
  }

  char fourcc[5] = {0};

  struct v4l2_format fmt = {0};
  fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  fmt.fmt.pix.width = 1920;
  fmt.fmt.pix.height = 1080;
  
  fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_JPEG;
  fmt.fmt.pix.field = V4L2_FIELD_NONE;
  fmt.fmt.pix.colorspace = V4L2_COLORSPACE_JPEG;

  if (-1 == xioctl(this->fd, VIDIOC_S_FMT, &fmt)){
    std::cout << "ImageCapture::initDev - Setting Pixel Format error" << std::endl;
    return false;
  }

  // init mmap
  struct v4l2_requestbuffers req = {0};
  req.count = 1;
  req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  req.memory = V4L2_MEMORY_MMAP;

  if (-1 == xioctl(this->fd, VIDIOC_REQBUFS, &req))
  {
    std::cout << "ImageCapture::initDev - Requesting Buffer error" << std::endl;
    return false;
  }

  struct v4l2_buffer buf = {0};
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  buf.index = 0;
  if(-1 == xioctl(this->fd, VIDIOC_QUERYBUF, &buf))
  {
    std::cout << "ImageCapture::initDev - Querying Buffer error" << std::endl;
    return false;
  }

  buffer = (uint8_t *)mmap (NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buf.m.offset);
//   log_info("Length: %d\nAddress: %p\n", buf.length, buffer);
//   log_info("Image Length: %d\n", buf.bytesused);


  // strncpy(fourcc, (char *)&fmt.fmt.pix.pixelformat, 4);  
  return true;
}


bool ImageCapture::capImageAsJPG(std::string &path){
  if(this->initDev() == false){
    return false;
  }

  int ret;

  struct v4l2_buffer buf = {0};
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  buf.index = 0;


  if(-1 == xioctl(this->fd, VIDIOC_QBUF, &buf))
  {
    std::cout << "ImageCapture::capImageAsJPG - Querying Buffer error" << std::endl;
    return false;
  }

  if(-1 == xioctl(this->fd, VIDIOC_STREAMON, &buf.type))
  {
    std::cout << "ImageCapture::capImageAsJPG - Start Capture error" << std::endl;
    return false;
  }

  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(this->fd, &fds);
  struct timeval tv = {0};
  tv.tv_sec = 2;
  int r = select(this->fd+1, &fds, NULL, NULL, &tv);
  if(-1 == r)
  {
    std::cout << "ImageCapture::capImageAsJPG - waiting frame error" << std::endl;
    return false;
  }

  if(-1 == xioctl(this->fd, VIDIOC_DQBUF, &buf))
  {
    std::cout << "ImageCapture::capImageAsJPG - retrieving frame error" << std::endl;
    return false;
  }


  // When retrieving the frame it will return the full number of bytes
  // even though it is a JPEG image. So look for the end of the JPEG marker
  // and only write out those bytes


  FILE * fp;
  fp = fopen(path.c_str(), "wb");

  uint8_t eof_jpeg_marker[2] = {0xff, 0xd9};

  char *last_needle = NULL;

  char *eof = (char *)memmem(buffer, buf.bytesused, eof_jpeg_marker, 2);

  if(eof == NULL) {
    std::cout << "ImageCapture::capImageAsJPG - Unable to find end of JPEG marker in retrieved frame error" << std::endl;
    return 1;
  }

  int jpeg_bytes = eof - (char *)buffer;
  //   log_info("Calculated size of JPEG as %d\n", jpeg_bytes);
  fwrite((void *)buffer, jpeg_bytes+2, 1, fp);
  fclose(fp);


  std::cout << "ImageCapture::capImageAsJPG - Image saved to = " << path << std::endl;

  return true;
}


bool ImageCapture::capImageAsBuffer(uint8_t *buffer){
  if(this->initDev() == false){
    return false;
  }

  int ret;

  struct v4l2_buffer buf = {0};
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  buf.index = 0;


  if(-1 == xioctl(this->fd, VIDIOC_QBUF, &buf))
  {
    std::cout << "ImageCapture::capImageAsBuffer - Querying Buffer error" << std::endl;
    return false;
  }

  if(-1 == xioctl(this->fd, VIDIOC_STREAMON, &buf.type))
  {
    std::cout << "ImageCapture::capImageAsBuffer - Start Capture error" << std::endl;
    return false;
  }

  fd_set fds;
  FD_ZERO(&fds);
  FD_SET(this->fd, &fds);
  struct timeval tv = {0};
  tv.tv_sec = 2;
  int r = select(this->fd+1, &fds, NULL, NULL, &tv);
  if(-1 == r)
  {
    std::cout << "ImageCapture::capImageAsBuffer - waiting frame error" << std::endl;
    return false;
  }

  if(-1 == xioctl(this->fd, VIDIOC_DQBUF, &buf))
  {
    std::cout << "ImageCapture::capImageAsBuffer - retrieving frame error" << std::endl;
    return false;
  }


  // When retrieving the frame it will return the full number of bytes
  // even though it is a JPEG image. So look for the end of the JPEG marker
  // and only write out those bytes


  std::cout << "ImageCapture::capImageAsBuffer - Image saved to buffer done!" << std::endl;

  return true;
}