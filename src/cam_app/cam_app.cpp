/**
******************************************************************************
* @file           : cam_app.cpp
* @brief          : Source file of camera application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_app/cam_app.h"
#include <sys/time.h>
#include "logger/webe_logger.h"
#include "ffmpeg_engine/boardCastReader_share.hpp"
#include "ffmpeg_engine/boardCastImplement.hpp"
#include "my_func/os_func.h"
#include <math.h>
#include "event_report/report_api.h"
#include "config/box_config.h"
#include <iostream>

WebeCamApp::WebeCamApp(){
    this->is_running = false;
    this->is_stop = false;
    this->cap_state = false;
    this->transcode_state = false;
    this->transcode_size = 80;
    this->producer = boardCastDataProvider::createFF_cap_rtsp();
    this->timeout_capture = OSFunc::getTimestamp();
#ifdef WYZE_CAM_V2
    this->restart_flag = false;
#endif

}

WebeCamApp::~WebeCamApp(){

}

int WebeCamApp::run(CamDevInfor *p_cam){
    this->is_running = true;
    this->is_stop = false;
    this->cam_infor.dev_serial = p_cam->dev_serial;
    std::cout << "WebeCamApp::run - Start application thread for Camera" << std::endl;
    ////myLogger.sysLogger->info("Start application thread for Camera = {0}", this->cam_infor.dev_serial);
    if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
        this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);
        // update app status
        this->cam_infor.is_running_app = true;
        camInforMap.insertDevMap(this->cam_infor);
    }
    else{
        return -1;
    }
#ifdef WYZE_CAM_V2
    std::thread HandleRestartThread(&WebeCamApp::handleRestart, this);
#endif
    //std::thread StreamingAppThread(&CamStreamingApp::run, &this->streamingApp, &this->cam_infor);
    // #ifdef INTEL_NUC_BOX
    // std::thread StreamingAppThread(&CamStreamingApp::MultiChanelQSVStreaming, &this->streamingApp, &this->cam_infor, this->producer, this->trans_code_eng, &this->transcode_state);
    // StreamingAppThread.detach();
    // std::thread GstStreamingThread(&CamStreamingApp::MultichanelGstSmartStreaming, &this->streamingApp, &this->cam_infor, this->producer, &this->cap_state);
    // GstStreamingThread.detach();    
    // #endif
    std::thread FfmpegStreamingThread(&CamStreamingApp::MultiChanelFfmpegStreaming, &this->streamingApp, &this->cam_infor, this->producer, &this->cap_state);
    FfmpegStreamingThread.detach();
    // std::thread RtspRelayStreamingThread(&CamStreamingApp::RtspRelayStreaming, &this->streamingApp, &this->cam_infor, this->producer, &this->cap_state);
    // RtspRelayStreamingThread.detach();
    // std::thread FfmpegTranscodeThread(&WebeCamApp::handleFfmpegTranscode, this);
    // FfmpegTranscodeThread.detach();

    // pthread_t storage_thread_handler;
    uint8_t cap_hang_num = 0;

    uint32_t timeout_check_error = 0;

    while (1){
        // check dev in Map
        if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
            this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);

            // check capture thread
            if(OSFunc::getTimestamp() - this->timeout_capture_thread > 5){
                ////myLogger.sysLogger->info("stop capture thread by hang for camera = {0}", this->cam_infor.dev_serial);
                std::cout << "WebeCamApp::run - stop capture thread by hang" << std::endl;
                
                //pthread_cancel(this->capture_thread_handler);
                this->cap_state = false;
                cap_hang_num++;
                if(cap_hang_num > 5){
                    if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                        this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);
                        // update app status
                        this->cam_infor.capture_state = false;
                        this->cam_infor.conn_state = WebeCamDevState::DISCONNECTED;
                        camInforMap.insertDevMap(this->cam_infor);        
                    }
                }
                else{
                    this->timeout_capture_thread = OSFunc::getTimestamp();
                    std::thread HandleFfmpegCaptureThread(&WebeCamApp::handleFfmpegAVPacketCapture, this);
                    this->capture_thread_handler = HandleFfmpegCaptureThread.native_handle();
                    HandleFfmpegCaptureThread.detach();
                    ////myLogger.sysLogger->info("start capture thread for camera = {0}", this->cam_infor.dev_serial);
                    std::cout << "WebeCamApp::run - start capture thread" << std::endl;
                }
            }
            if(this->cap_state == true){
                cap_hang_num = 0;
            }
            

            // check service for app  

            if(this->cam_infor.storageApp.active_state == WebeCamDevState::ACTIVE){
                // if((OSFunc::getTimestamp() - this->storageApp.time_check_ffmpeg) > 30){
                //     this->storageApp.time_check_ffmpeg = OSFunc::getTimestamp();
                //     //myLogger.sysLogger->info("WebeCamApp::run - stop storage by hang for camera = {0}", this->cam_infor.dev_serial);
                //     pthread_cancel(storage_thread_handler);
                //     std::thread FfmpegStorageThread(&CamStorageApp::runFfmpegStorage, &this->storageApp, &this->cam_infor, this->producer, &this->cap_state);
                //     storage_thread_handler = FfmpegStorageThread.native_handle();
                //     FfmpegStorageThread.detach();
                // }
                if(this->storageApp.is_running == false){
                    // start storage app
                    //std::thread StorageAppThread(&CamStorageApp::runStorageQsv, &this->storageApp, &this->cam_infor, this->producer, this->trans_code_eng, &this->transcode_state);
                    std::thread StorageAppThread(&CamStorageApp::runFfmpegStorage, &this->storageApp, &this->cam_infor, this->producer, &this->cap_state);
                    StorageAppThread.detach();
                }
            }
            
            if(this->cam_infor.uploadApp.active_status == WebeCamDevState::ACTIVE){
                if(this->uploadApp.is_running == false){
                    // start upload app
                    std::thread UploadAppThread(&CamUploadApp::run, &this->uploadApp, &this->cam_infor);
                    UploadAppThread.detach();
                }
            }
            // check case to stop app
            if((this->cam_infor.conn_state == WebeCamDevState::DISCONNECTED) || (this->cam_infor.owner_state == WebeCamDevState::NOT_OWNER)){
                // this->is_stop = true;
            }

        }
        else{
            this->is_stop = true;
        }

        if(this->is_stop == true){
            // stop all application before break
            this->storageApp.is_stop = true;
            this->streamingApp.is_stop = true;
            sleep(1);
            sleep(2);
            ////myLogger.sysLogger->warn("stop camApp for camera = {0}", this->cam_infor.dev_serial);
            this->is_running = false;
            if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);
                // update app status
                this->cam_infor.is_running_app = false;
                camInforMap.insertDevMap(this->cam_infor);        
            }
            break;
        }

        if(this->cam_infor.conn_state == WebeCamDevState::DISCONNECTED){
            if(OSFunc::getTimestamp() - timeout_check_error > 1800){
                ////myLogger.sysLogger->warn("WebeCamApp - report camera disconnected for camera = {0}", this->cam_infor.dev_serial);
                BeexConfig::BoxInfo boxInfo;
                boxConfig.getBoxInfo(&boxInfo);
                BeexEventReport::ReportApi reporter(this->cam_infor.dev_serial, boxInfo.serial);
                std::string type = "connection";
                std::string content = "camera disconnected";
                reporter.reportEvent(type, content);
                timeout_check_error = OSFunc::getTimestamp();
            }
        }
        usleep(100000);
    }
}


int WebeCamApp::handleFfmpegAVPacketCapture(void){
    //myLogger.sysLogger->info("Start Capture AVPacket for camera: {0}", this->cam_infor.dev_serial);
    std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - start" << std::endl;

    AVPacket packet;
	memset(&packet, 0, sizeof(packet));
	av_init_packet(&packet); 
    
	//memset(av_packet.packet, 0, sizeof(*av_packet.packet));
	//av_init_packet(av_packet.packet);   
    uint16_t check_open = 0;   
    uint32_t bitrate_estimate = 0; 
    while (1){
        
        try{
            this->timeout_capture_thread = OSFunc::getTimestamp();
            if(this->is_stop == true){
                //myLogger.sysLogger->info("Stop Capture AVPacket for camera = {0}", this->cam_infor.dev_serial);
                std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Stop Capture AVPacket" << std::endl;
                this->cap_state = false;
                return -1;
            }
            this->cap_state = false;

            std::string input_link = "";

            if((this->cam_infor.conn_state == WebeCamDevState::CONNECTED) && (this->cam_infor.rtsp_url.empty() == false)){
                input_link = this->cam_infor.rtsp_url;
                if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                    this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);                    
                    this->cam_infor.is_running_videosample = false;
                    camInforMap.insertDevMap(this->cam_infor);
                }                  
            }
            else{
                input_link = BEEX_PATH + std::string("waiting_video.mp4");
                if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                    this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);                    
                    this->cam_infor.is_running_videosample = true;
                    camInforMap.insertDevMap(this->cam_infor);
                }                  
            }
          
            
            if(this->producer->open(input_link.c_str()) == true){
                bitrate_estimate = 0;
                check_open = 0;
                //myLogger.sysLogger->info("Open Capture AVPacket for camera = {0} : opened", this->cam_infor.dev_serial);
                std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Open Capture AVPacket = " << input_link << std::endl;
                
                if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                    this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);
                    if(this->cam_infor.fps == 0){
                        this->cam_infor.fps = this->producer->get_fps();
                    }
                    //this->cam_infor.resol_width = in_size.width;
                    //this->cam_infor.resol_height = in_size.height;
                    this->cam_infor.capture_state = true;
                    camInforMap.insertDevMap(this->cam_infor);                    
                }
                //std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Estimate frame rate" << std::endl;
                //myLogger.sysLogger->info("Estimate frame rate = {0}, resolution = {1}:{2}", std::to_string(this->cam_infor.fps), std::to_string(in_size.width), std::to_string(in_size.height));

                uint16_t fps = 0;
                uint32_t time_stamp = OSFunc::getTimestamp();
                unsigned long time_check_fps = OSFunc::getTimestamp();
                uint32_t fps_cnt = 0;
                this->cap_state= true;
                int ret;
                uint16_t check_packet = 0;
                while (1)
                {
                    if(this->cam_infor.is_running_videosample){
                        usleep(100000);
                        if(this->cam_infor.conn_state == WebeCamDevState::CONNECTED){
                            break;
                        }
                    }
                    this->timeout_capture = OSFunc::getTimestamp();
                    this->timeout_capture_thread = OSFunc::getTimestamp();
                    bool flag_continue = true;
                    ret = this->producer->get_new_packet(packet);
                    if(ret < 0){
                        std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - get frame error" << std::endl;
                        //myLogger.sysLogger->error("Capture AVPacket for camera = {0}: get frame error", this->cam_infor.dev_serial);
                        check_packet++;
                        if(check_packet > 10){
                            std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Restart capture Frame" << std::endl;
                            //myLogger.sysLogger->info("Restart capture Frame for camera:", this->cam_infor.dev_serial);
                            if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                                this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);                    
                                this->cam_infor.conn_state = WebeCamDevState::DISCONNECTED;
                                camInforMap.insertDevMap(this->cam_infor);
                            }
                            break;
                        }
                        usleep(100000);
                    }
                    else{
                        check_packet = 0;
                        fps_cnt++;
                        fps++;
                        bitrate_estimate += packet.size;
                        this->storageApp.avPacketQueue.pushPacket(&packet, ret);
                        #ifdef INTEL_NUC_BOX
                        //this->transcodeQueue.pushPacket(&packet, ret);
                        //this->streamingApp.gstAVPacketQueue.pushPacket(&packet, ret);
                        #endif
                        this->streamingApp.avPacketQueue.pushPacket(&packet, ret);
                        this->streamingApp.avPacketQueueRelay.pushPacket(&packet, ret);
                    }
                    if((OSFunc::getTimestamp() - time_check_fps) >= 600){
                        if(((OSFunc::getTimestamp() - time_check_fps) % 600) == 0){
                            float fps_calcu = fps_cnt / (OSFunc::getTimestamp() - time_check_fps);
                            std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - caculator fps = " << std::to_string(fps_calcu) << std::endl;
                            //myLogger.sysLogger->info("caculator fps for camera = {0} - fps = {1}", this->cam_infor.dev_serial, std::to_string(fps_calcu));
                            if(fps_calcu <= 30){
                                if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                                    this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);
                                    this->cam_infor.fps = ceil(fps_calcu);
                                    camInforMap.insertDevMap(this->cam_infor);
                                }
                            }
                            fps_cnt = 0;
                            time_check_fps = OSFunc::getTimestamp();                            
                        }
                    }

                    if(OSFunc::getTimestamp() - time_stamp >= 10){
                        
                        time_stamp = OSFunc::getTimestamp();
                        std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Check Capture AVPacket = " << std::to_string(bitrate_estimate / 10000) << std::endl;
                        //myLogger.sysLogger->info("Check Capture AVPacket for camera = {0} : fps = {1} -  bitrate_estimate = {2}KB/s - resoluton = {3}:{4} - fps_estimate = {5}", this->cam_infor.dev_serial, std::to_string(fps), std::to_string(bitrate_estimate / 10000), std::to_string(this->cam_infor.resol_width), std::to_string(this->cam_infor.resol_height), std::to_string(this->cam_infor.fps));
                        if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                            this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);
                            this->cam_infor.strength = ((fps / 10.0) / this->cam_infor.fps) * 100;
                            this->cam_infor.bitrate_max = bitrate_estimate / 10000;
                            if(this->cam_infor.strength > 100){
                                this->cam_infor.strength = 100;
                            }
                            camInforMap.insertDevMap(this->cam_infor);
                        }
                        if(fps == 0){
                            std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Restart capture Frame" << std::endl;
                            //myLogger.sysLogger->info("Restart capture Frame for camera = {0}", this->cam_infor.dev_serial);
                            break;
                        }
                        fps = 0;
                        bitrate_estimate = 0;
                    }
                    av_packet_unref(&packet);
                    if(this->is_stop == true){
                        std::cout << "WebeCamApp::handleFfmpegAVPacketCapture - Stop Capture AVPacket" << std::endl;
                        //myLogger.sysLogger->info("Stop Capture AVPacket for camera = {0}", this->cam_infor.rtsp_url);
                        this->cap_state = false;
                        return -1;
                    }
                }                
            }
            else{
                std::cout << "WebeCamApp::handleFfmpegAVPacketCapture -  can't open inputLink" << std::endl;
                //myLogger.sysLogger->error("Capture AVPacket for camera camera = {0}: can't open inputLink", this->cam_infor.dev_serial);
                check_open++;
                if(check_open > 5){
                    if(camInforMap.checkDevMap(this->cam_infor.dev_serial) == true){
                        this->cam_infor = camInforMap.getDevMap(this->cam_infor.dev_serial);                    
                        this->cam_infor.conn_state = WebeCamDevState::DISCONNECTED;
                        camInforMap.insertDevMap(this->cam_infor);
                    }
                }
            }            
        }
        catch(const std::exception& e){
            std::string get_except(e.what());
            //myLogger.sysLogger->error("exception in Capture AVPacket for camera = {0}: {1}", this->cam_infor.dev_serial, get_except);
        }
        usleep(300000);
    }
}


#ifdef WYZE_CAM_V2
void WebeCamApp::handleRestart(void){
    uint32_t start_time = OSFunc::getTimestamp();
    while (1)
    {
        uint32_t current_time = OSFunc::getTimestamp();

        // check restart as schedule
        if(current_time - start_time > 86400){
            this->storageApp.is_stop = true;
            sleep(5);
            std::string cmd = "reboot";
            std::cout << "reboot camera as schedule" << std::endl;
        }
        if((current_time - this->timeout_capture > 10) || (current_time - this->storageApp.timeout_storage > 10)){
            this->restart_flag = true;
        }
        if(this->restart_flag){
            this->restart_flag = false;
            std::string cmd = "reboot";
            std::cout << "reboot camera as issue" << std::endl;
            system(cmd.c_str());
        }
        sleep(2);
    }
    
}
#endif