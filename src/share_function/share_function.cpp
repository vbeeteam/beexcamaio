/**
******************************************************************************
* @file           : share_function.h
* @brief          : source file of share function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "share_function/share_function.h"

#include <sys/time.h>
#include <stdio.h>

#include <unistd.h> 
#include <string.h> 
#include <stdio.h>  
#include <fcntl.h> 
#include <sys/stat.h>

uint32_t BeexShareFunction::getTimestamp(void){
	struct timeval tv;
	unsigned long get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_sec;
	return get_time;
}


uint32_t BeexShareFunction::getTimeMs(void){
	struct timeval tv;
	uint32_t get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_usec;
	return get_time / 1000;
}

uint8_t BeexShareFunction::splitString(std::string data, std::string key_split, std::string *data_split){
    uint16_t pos = 0;
    uint16_t i = 0;

    
    while ((pos = data.find(key_split)) != std::string::npos) {
        data_split[i] = data.substr(0, pos);
        data.erase(0, pos + 1);
        i++;
        if (i > 9) {
            return 9;
        }
    }
    return i;
}

bool BeexShareFunction::checkFile(std::string path){
    struct stat buffer;   
    return (stat (path.c_str(), &buffer) == 0); 
}


std::string BeexShareFunction::fileName(std::string path){
  size_t found = path.find_last_of("/\\");
  std::string name = path.substr(found+1); // check that is OK
  return name;
}

bool BeexShareFunction::creatFile(std::string path){
    std::ofstream outfile (path, std::fstream::app);
    outfile.close();
}