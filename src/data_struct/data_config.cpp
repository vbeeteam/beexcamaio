/**
******************************************************************************
* @file           : data_config.cpp
* @brief          : Source file of camera data config
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "data_struct/data_config.h"
#include "my_func/os_func.h"
#include "logger/webe_logger.h"
#include <sstream>
#include <share_function/share_function.h>


CamConfig camConfig;

bool CamConfig::writeConfig(DevInforMap *info){
    bool result = false;
    this->configLocker.lock();
    if(BeexShareFunction::checkFile(this->path_file)){
        // prepare data
        std::string json_data;
        std::stringstream out;
#ifdef INTEL_NUC_BOX        
        JSON::Object object;
        JSON::Array listCamArray;

        CamDevInfor list_cam_info[DevInforMap::cam_num_max];
        uint8_t list_cam_num = info->getAllDevMap(list_cam_info);
        for (uint8_t i = 0; i < list_cam_num; i++){
            JSON::Object camObject;
            camObject.set("serial", list_cam_info[i].dev_serial);

            JSON::Array listStreamArray;
            std::map<std::string, CamStreamingInfo>::iterator map;
            CamStreamingInfo list_stream_info[CamDevInfor::streaming_num_max];
            uint8_t list_stream_num = 0;
            for (map = list_cam_info[i].listStreaming.begin(); map != list_cam_info[i].listStreaming.end(); map++) {
                list_stream_info[list_stream_num] = map->second;
                if(list_stream_info[list_stream_num].is_gst_out_state == WebeCamDevState::ACTIVE){
                    JSON::Object streamObject;
                    streamObject.set("hostMedia", list_stream_info[list_stream_num].host_media);
                    streamObject.set("portMedia", list_stream_info[list_stream_num].port_media);
                    streamObject.set("zoomState", list_stream_info[list_stream_num].zoom_state);
                    if(list_stream_info[list_stream_num].is_ffmpeg_state == WebeCamDevState::ACTIVE){
                        streamObject.set("ffmpegState", 1);
                    }
                    else if(list_stream_info[list_stream_num].is_ffmpeg_state == WebeCamDevState::INACTIVE){
                        streamObject.set("ffmpegState", 0);
                    }
                    if(list_stream_info[list_stream_num].is_qsv_state == WebeCamDevState::ACTIVE){
                        streamObject.set("qsvState", 1);
                    }
                    else if(list_stream_info[list_stream_num].is_qsv_state == WebeCamDevState::INACTIVE){
                        streamObject.set("qsvState", 0);
                    }                    
                    if(list_stream_info[list_stream_num].is_gst_out_state == WebeCamDevState::ACTIVE){
                        streamObject.set("gstState", 1);
                    }
                    else if(list_strejson_dataCamDevInfor::streaming_num_max){
                    break;
                }
                
            }
            camObject.set("streaming", listStreamArray);
            listCamArray.add(camObject);
        }
        object.set("camera", listCamArray);

        object.stringify(out);
        json_data = out.str();
        //myLogger.sysLogger->info("CamConfig - write data config = {0}", json_data);
        // open config file
#endif        
        FILE *configFile = NULL;
        configFile = fopen(this->path_file.c_str(), "w");
        if(configFile != NULL){
            fprintf(configFile, json_data.c_str());
            result = true;
        }
        else{
            //myLogger.sysLogger->error("CamConfig - open config file to write error = {0}", this->path_file);
        }
        fclose(configFile);
    }
    else{
        //myLogger.sysLogger->error("CamConfig - config file to write not found = {0}", this->path_file);
        BeexShareFunction::creatFile(this->path_file);
    }
    this->configLocker.unlock();
    return result;
}


bool CamConfig::readConfig(DevInforMap *info){
    bool result = false;
    this->configLocker.lock();
    if(BeexShareFunction::checkFile(this->path_file)){
        // open file
        FILE *configFile = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        configFile = fopen(this->path_file.c_str(), "r");
        if(configFile != NULL){
            read = getline(&line, &len, configFile);
            if(read != -1){
                std::string json_data(line);
                //myLogger.sysLogger->info("CamConfig - read config file = {0}", json_data);

                try{
#ifdef INTEL_NUC_BOX                    
                    JSON::Parser parser;
                    Dynamic::Var var;
                    var = parser.parse(json_data);
                    JSON::Object::Ptr object = var.extract<JSON::Object::Ptr>();

                    Dynamic::Var listCamVar = object->get("camera");
                    JSON::Array::Ptr listCamArray = listCamVar.extract<JSON::Array::Ptr>();
                    uint8_t list_cam_num = listCamArray->size();
                    for (uint8_t i = 0; i < list_cam_num; i++){
                        CamDevInfor cam_info;
                        JSON::Object::Ptr camObject = listCamArray->getObject(i);
                        
                        std::string serial = camObject->get("serial");
                        cam_info.dev_serial = serial;

                        //list streaming
                        Dynamic::Var listStreamVar = camObject->get("streaming");
                        JSON::Array::Ptr listStreamArray = listStreamVar.extract<JSON::Array::Ptr>();
                        uint8_t list_stream_num = listStreamArray->size();
                        for (uint8_t j = 0; j < list_stream_num; j++){
                            CamStreamingInfo streaming_info;
                            JSON::Object::Ptr streamObject = listStreamArray->getObject(j);

                            std::string hostMedia = streamObject->get("hostMedia");
                            std::string portMedia = streamObject->get("portMedia");
                            uint8_t zoomState = streamObject->get("zoomState");
                            uint8_t ffmpegState = streamObject->get("ffmpegState");
                            uint8_t qsvState = streamObject->get("qsvState");
                            uint8_t gstState = streamObject->get("gstState");

                            if(gstState == 1){
                                streaming_info.host_media = hostMedia;
                                streaming_info.port_media = portMedia;
                                streaming_info.zoom_state = zoomState;
                                if(ffmpegState == 0){
                                    streaming_info.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                }
                                else{
                                    streaming_info.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                }
                                if(qsvState == 0){
                                    streaming_info.is_qsv_state = WebeCamDevState::INACTIVE;
                                }
                                else{
                                    streaming_info.is_qsv_state = WebeCamDevState::ACTIVE;
                                } 
                                if(gstState == 0){
                                    streaming_info.is_gst_out_state = WebeCamDevState::INACTIVE;
                                }
                                else{
                                    streaming_info.is_gst_out_state = WebeCamDevState::ACTIVE;
                                }
                                std::string key = hostMedia + ":" + portMedia;
                                cam_info.listStreaming[key] = streaming_info;
                            }
                        }
                        info->insertDevMap(cam_info);
                        result = true;
                    }
#endif                    
                }
                catch(const std::exception& e){
                    //myLogger.sysLogger->error("CamConfig - parse data from config file error = {0}, exception = {1}", this->path_file, e.what());
                }
                
            }
            else{
                //myLogger.sysLogger->error("CamConfig - read config error = {0}", this->path_file);
            }
        }
        else{
            //myLogger.sysLogger->error("CamConfig - open config file to read error = {0}", this->path_file);
        }
        fclose(configFile);
    }
    else{
        //myLogger.sysLogger->error("CamConfig - config file to read not found = {0}", this->path_file);
    }
    this->configLocker.unlock();
    return result;
}