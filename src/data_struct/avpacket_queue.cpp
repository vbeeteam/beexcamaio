/**
******************************************************************************
* @file           : avpacket_queue.cpp
* @brief          : Source file of AVPacket Queue
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "data_struct/avpacket_queue.h"

using namespace WebeCamAVPacket;

AVPacketQueue::AVPacketQueue(){
    this->queue_size = 1000;
}

AVPacketQueue::~AVPacketQueue(){
}

bool AVPacketQueue::pushPacket(AVPacket *pkt, int ret){
    bool result = false;

    this->locker.lock();
    if(this->avpacket_queue.size() < this->queue_size){
        AVPacket *packet  = (AVPacket *)av_malloc(sizeof(AVPacket));
        if(packet != NULL){
            int resp = 0;
            resp = av_copy_packet(packet, pkt);
            if(resp == 0){
                this->avpacket_queue.push(*packet);
                this->ret_queue.push(ret);
                result = true;
            }
        }
        av_free(packet);
    }
    this->locker.unlock();
    return result;
}


bool AVPacketQueue::getPacket(AVPacket *pkt, int *ret){
    bool result = false;
    
    this->locker.lock();
    if(this->avpacket_queue.size() > 0){
        AVPacket packet = this->avpacket_queue.front();
        *ret = this->ret_queue.front();
        this->avpacket_queue.pop();
        this->ret_queue.pop();
        av_copy_packet(pkt, &packet);
        result = true;
        //av_packet_unref(&packet);
        av_free_packet(&packet);
    }
    this->locker.unlock();
    return result;
}


bool AVPacketQueue::clearQueue(void){
    this->locker.lock();
    int ret;
    while (this->avpacket_queue.size() > 0){
        AVPacket packet = this->avpacket_queue.front();
        ret = this->ret_queue.front();
        this->avpacket_queue.pop();
        this->ret_queue.pop();
        av_free_packet(&packet);
    }
    this->locker.unlock();
}

uint16_t AVPacketQueue::getQueueSizeAvailable(void){
    return this->avpacket_queue.size();
}