/**
******************************************************************************
* @file           : cam_data_struct.cpp
* @brief          : Source file of camera data struct
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "data_struct/cam_data_struct.h"
#include "config/my_config.h"

/* Object and Varial --------------------------------------------------------*/
DevInforMap camInforMap;

/* Method -------------------------------------------------------------------*/

CamApp::CamApp(){
	this->active_state = WebeCamDevState::INACTIVE;
}

CamApp::~CamApp(){

}



CamStorageInfo::CamStorageInfo(){
	this->active_state = WebeCamDevState::ACTIVE;
	this->time_split = 600;
	this->fps = 15;
}

CamStorageInfo::~CamStorageInfo(){
}


CamUploadServ::CamUploadServ(){
    this->active_status = WebeCamDevState::INACTIVE;
	this->is_update = true;
	this->baseStorageUrl = "https://s4.beex.vn:8099/upload";
	this->baseStorageUrlBackup = "";
	this->baseStoragePrivateBackupUrl = "";
}

CamUploadServ::~CamUploadServ(){
	
}

CamUploadPacket::CamUploadPacket(){
}

CamUploadPacket::~CamUploadPacket(){
}




OnvifCamInfor::OnvifCamInfor(){
	this->authen_state = "require";
	this->userName = "";
	this->password = "";
	this->lastTimeUpdate = 0;
}

OnvifCamInfor::~OnvifCamInfor()
{
}

DevInforMap::DevInforMap()
{
	this->change_flag = false;
}

DevInforMap::~DevInforMap()
{
}


CamDevInfor::CamDevInfor(){
	this->camera_type = "streaming";
	this->conn_state = WebeCamDevState::DISCONNECTED;
	this->serial_state = WebeCamDevState::NOT_SERIAL;
	this->authen_state = WebeCamDevState::NOT_AUTHEN;
	this->owner_state = WebeCamDevState::NOT_OWNER;
	this->is_smart_streaming = WebeCamDevState::INACTIVE;
	this->capture_enable = false;
	this->is_running_app = false;
	this->capture_state = true;
	this->is_running_videosample = false;

	this->strength = 100;
	this->bitrate_max = 125; //KB/s

	this->last_time_update_serv = 0;
	this->is_update_serv = false;

	this->relayStreaming.is_relay_state = WebeCamDevState::ACTIVE;
	this->relayStreaming.host_media = "103.63.108.20";
	this->relayStreaming.port_media = "40002";
}

CamDevInfor::~CamDevInfor(){

}


/**
* @desc: check mapDev
* @param: dev serial - key
* @return: results
**/
bool DevInforMap::checkDevMap(std::string dev_serial) {
	if (this->devMap.find(dev_serial) == this->devMap.end())
	{
		return false;
	}
	return true;
}

/**
* @desc: get mapDev
* @param: dev serial - key
* @return: devMap
**/
CamDevInfor DevInforMap::getDevMap(std::string dev_serial) {
	CamDevInfor get_dev_map;
	this->devMapLocker.lock();
	get_dev_map = this->devMap[dev_serial];
	this->devMapLocker.unlock();
	return get_dev_map;
}

/**
* @desc: insert mapDev
* @param: devMap
* @return: result
**/
bool DevInforMap::insertDevMap(CamDevInfor dev_infor) {
	this->devMapLocker.lock();
	this->devMap[dev_infor.dev_serial] = dev_infor;
	this->change_flag = true;
	this->devMapLocker.unlock();
	return true;
}

/**
* @desc: insert mapDev
* @param: devMap
* @return: result
**/
bool DevInforMap::eraseDevMap(std::string dev_serial) {
	this->devMapLocker.lock();
	this->devMap.erase(dev_serial);
	this->change_flag = true;
	this->devMapLocker.unlock();
	return true;
}

/**
* @desc: load all devInfor
* @param: point of devInfor array
* @return: length
**/
uint16_t DevInforMap::getAllDevMap(CamDevInfor *dev_infor) {
	uint16_t len = 0;
	std::map<std::string, CamDevInfor>::iterator map;
	this->devMapLocker.lock();
	for (map = this->devMap.begin(); map != this->devMap.end(); map++) {
		*dev_infor = map->second;
		dev_infor++;
		len++;
	}
	this->devMapLocker.unlock();
	return len;
}