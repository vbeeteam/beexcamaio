/**
******************************************************************************
* @file           : my_config.cpp
* @brief          : source file of my configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "config/my_config.h"
#include "logger/webe_logger.h"
#include "share_function/share_function.h"
#include "config/box_config.h"
#include "data_struct/cam_data_struct.h"


using namespace BeexConfig;

MyConfig myConfig;

bool MyConfig::loadConfig(void){
    BoxInfo boxInfo;
    boxConfig.getBoxInfo(&boxInfo);
    this->filePath = boxInfo.appPath + BEEX_APP_DIR + BEEX_CONFIG_DIR;
    std::string configFile = this->filePath + this->fileName;
    std::string cmd = "sudo mkdir -p " + this->filePath;
    system(cmd.c_str());
    //myLogger.sysLogger->info("MyConfig::loadConfig - file = {0}", configFile);
    bool result = false;
    // load file config
    if(BeexShareFunction::checkFile(configFile)){
        FILE *file = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        file = fopen(configFile.c_str(), "r");
        if(file != NULL){
            read = getline(&line, &len, file);
            if(read != -1){
                std::string json_data(line);
                //myLogger.sysLogger->info("MyConfig::loadConfig - read config data = {0}", json_data);

                try{
#ifdef INTEL_NUC_BOX                    
                    JSON::Parser parser;
                    Dynamic::Var var;
                    var = parser.parse(json_data);
                    JSON::Object::Ptr object = var.extract<JSON::Object::Ptr>();

                    Dynamic::Var listCamVar = object->get("camera");
                    JSON::Array::Ptr listCamArray = listCamVar.extract<JSON::Array::Ptr>();
                    uint8_t list_cam_num = listCamArray->size();
                    for (uint8_t i = 0; i < list_cam_num; i++){
                        CamDevInfor cam_info;
                        JSON::Object::Ptr camObject = listCamArray->getObject(i);
                        
                        std::string serial = camObject->get("serial");
                        if(!serial.empty()){
                            cam_info.dev_serial = serial;

                            if(camObject->has("camera_type")){
                                std::string camera_type = camObject->get("camera_type");
                                cam_info.camera_type = camera_type;
                            }
                            if(camObject->has("mac_addr")){
                                std::string mac_addr = camObject->get("mac_addr");
                                cam_info.mac_addr = mac_addr;
                            }
                            if(camObject->has("rtsp_url")){
                                std::string rtsp_url = camObject->get("rtsp_url");
                                cam_info.rtsp_url = rtsp_url;
                            }
                            if(camObject->has("source")){
                                std::string source = camObject->get("source");
                                cam_info.source = source;
                            }
                            if(camObject->has("strength")){
                                uint8_t strength = camObject->get("strength");
                                cam_info.strength = strength;
                            }
                            if(camObject->has("fps")){
                                uint16_t fps = camObject->get("fps");
                                cam_info.fps = fps;
                            }
                            if(camObject->has("resol_width")){
                                uint16_t resol_width = camObject->get("resol_width");
                                cam_info.resol_width = resol_width;
                            }
                            if(camObject->has("resol_height")){
                                uint16_t resol_height = camObject->get("resol_height");
                                cam_info.resol_height = resol_height;
                            }
                            if(camObject->has("storageApp")){
                                JSON::Object::Ptr storageApp = camObject->getObject("storageApp");
                                if(storageApp->has("origin_path")){
                                    std::string origin_path = storageApp->get("origin_path");
                                    cam_info.storageApp.origin_path = origin_path;
                                }
                                if(storageApp->has("backup_path")){
                                    std::string backup_path = storageApp->get("backup_path");
                                    cam_info.storageApp.backup_path = backup_path;
                                }
                                if(storageApp->has("thumb_path")){
                                    std::string thumb_path = storageApp->get("thumb_path");
                                    cam_info.storageApp.thumb_path = thumb_path;
                                }                                                                

                            
                            }
                            if(camObject->has("uploadApp")){
                                JSON::Object::Ptr uploadApp = camObject->getObject("uploadApp");
                                if(uploadApp->has("active_status")){
                                    uint8_t active_status = uploadApp->get("active_status");
                                    if(active_status == 0){
                                        cam_info.uploadApp.active_status = WebeCamDevState::INACTIVE;
                                    }
                                    else{
                                        cam_info.uploadApp.active_status = WebeCamDevState::ACTIVE;
                                    }
                                }
                                if(uploadApp->has("baseStorageUrl")){
                                    std::string baseStorageUrl = uploadApp->get("baseStorageUrl");
                                    cam_info.uploadApp.baseStorageUrl = baseStorageUrl;
                                }
                                if(uploadApp->has("baseStorageUrlBackup")){
                                    std::string baseStorageUrlBackup = uploadApp->get("baseStorageUrlBackup");
                                    cam_info.uploadApp.baseStorageUrlBackup = baseStorageUrlBackup;
                                }
                                if(uploadApp->has("baseStoragePrivateBackupUrl")){
                                    std::string baseStoragePrivateBackupUrl = uploadApp->get("baseStoragePrivateBackupUrl");
                                    cam_info.uploadApp.baseStoragePrivateBackupUrl = baseStoragePrivateBackupUrl;
                                }
                            }                            
                            

                            //list streaming
                            if(camObject->has("streaming")){
                                Dynamic::Var listStreamVar = camObject->get("streaming");
                                JSON::Array::Ptr listStreamArray = listStreamVar.extract<JSON::Array::Ptr>();
                                uint8_t list_stream_num = listStreamArray->size();
                                for (uint8_t j = 0; j < list_stream_num; j++){
                                    CamStreamingInfo streaming_info;
                                    JSON::Object::Ptr streamObject = listStreamArray->getObject(j);

                                    if(streamObject->has("hostMedia")){
                                        std::string hostMedia = streamObject->get("hostMedia");
                                        streaming_info.host_media = hostMedia;
                                    }
                                    else{
                                        continue;
                                    }
                                    if(streamObject->has("portMedia")){
                                        std::string portMedia = streamObject->get("portMedia");
                                        streaming_info.port_media = portMedia;
                                    }
                                    else{
                                        continue;
                                    }                                    
                                    if(streamObject->has("zoomState")){
                                        uint8_t zoomState = streamObject->get("zoomState");
                                        streaming_info.zoom_state = zoomState;
                                    }
                                    else{
                                        continue;
                                    }                                    
                                    if(streamObject->has("ffmpegState")){
                                        uint8_t ffmpegState = streamObject->get("ffmpegState");
                                        if(ffmpegState == 0){
                                            streaming_info.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                        }
                                        else{
                                            streaming_info.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                        }                                        
                                    }
                                    else{
                                        continue;
                                    }                                    
                                    if(streamObject->has("qsvState")){
                                        uint8_t qsvState = streamObject->get("qsvState");
                                        if(qsvState == 0){
                                            streaming_info.is_qsv_state = WebeCamDevState::INACTIVE;
                                        }
                                        else{
                                            streaming_info.is_qsv_state = WebeCamDevState::ACTIVE;
                                        }
                                    }
                                    else{
                                        continue;
                                    }                                    
                                    if(streamObject->has("gstState")){
                                        uint8_t gstState = streamObject->get("gstState");
                                        if(gstState == 0){
                                            streaming_info.is_gst_out_state = WebeCamDevState::INACTIVE;
                                        }
                                        else{
                                            streaming_info.is_gst_out_state = WebeCamDevState::ACTIVE;
                                        }
                                        if(gstState == 1){
                                            std::string key = streaming_info.host_media + ":" + streaming_info.port_media;
                                            cam_info.listStreaming[key] = streaming_info;
                                        }
                                    }
                                    else{
                                        continue;
                                    }
                                }
                            }                            
                            this->configInfo.listCamera.insertDevMap(cam_info);
                        }
                        result = true;
                    }
#endif                    
                }
                catch(const std::exception& e){
                    //myLogger.sysLogger->error("MyConfig::loadConfig - exception when parse json data = {0}", json_data);
                    result = false;
                }
            }
            else{
                //myLogger.sysLogger->error("MyConfig::loadConfig - config file not read = {0}", configFile);
            }
        }
        else{
            //myLogger.sysLogger->error("MyConfig::loadConfig - config file not opened = {0}", configFile);
        }
        fclose(file);
    }
    else{
        //myLogger.sysLogger->error("MyConfig::loadConfig - config file not found = {0}", configFile);
        BeexShareFunction::creatFile(configFile);
    }
    return result;       
}

bool MyConfig::saveConfig(void){
    // prepare data
    std::string json_data;
#ifdef INTEL_NUC_BOX
    JSON::Object object;
    JSON::Array listCamArray;

    CamDevInfor list_cam_info[DevInforMap::cam_num_max];
    uint8_t list_cam_num = this->configInfo.listCamera.getAllDevMap(list_cam_info);
    for (uint8_t i = 0; i < list_cam_num; i++){
        JSON::Object camObject;
        camObject.set("serial", list_cam_info[i].dev_serial);

        camObject.set("camera_type", list_cam_info[i].camera_type);
        camObject.set("mac_addr", list_cam_info[i].mac_addr);
        camObject.set("rtsp_url", list_cam_info[i].rtsp_url);
        camObject.set("source", list_cam_info[i].source);
        camObject.set("strength", list_cam_info[i].strength);
        camObject.set("fps", list_cam_info[i].fps);
        camObject.set("resol_width", list_cam_info[i].resol_width);
        camObject.set("resol_height", list_cam_info[i].resol_height);
        
        JSON::Object storageApp;
        storageApp.set("origin_path", list_cam_info[i].storageApp.origin_path);
        storageApp.set("backup_path", list_cam_info[i].storageApp.backup_path);
        storageApp.set("thumb_path", list_cam_info[i].storageApp.thumb_path);
        
        camObject.set("storageApp", storageApp);

        JSON::Object uploadApp;
        if(list_cam_info[i].uploadApp.active_status == WebeCamDevState::ACTIVE){
            uploadApp.set("active_status", 1);
        }
        else{
            uploadApp.set("active_status", 0);
        }
        uploadApp.set("baseStorageUrl", list_cam_info[i].uploadApp.baseStorageUrl);
        uploadApp.set("baseStorageUrlBackup", list_cam_info[i].uploadApp.baseStorageUrl);
        uploadApp.set("baseStoragePrivateBackupUrl", list_cam_info[i].uploadApp.baseStorageUrl);

        camObject.set("uploadApp", uploadApp);

        JSON::Array listStreamArray;
        std::map<std::string, CamStreamingInfo>::iterator map;
        CamStreamingInfo list_stream_info[CamDevInfor::streaming_num_max];
        uint8_t list_stream_num = 0;
        for (map = list_cam_info[i].listStreaming.begin(); map != list_cam_info[i].listStreaming.end(); map++) {
            list_stream_info[list_stream_num] = map->second;
            if(list_stream_info[list_stream_num].is_gst_out_state == WebeCamDevState::ACTIVE){
                JSON::Object streamObject;
                streamObject.set("hostMedia", list_stream_info[list_stream_num].host_media);
                streamObject.set("portMedia", list_stream_info[list_stream_num].port_media);
                streamObject.set("zoomState", list_stream_info[list_stream_num].zoom_state);
                if(list_stream_info[list_stream_num].is_ffmpeg_state == WebeCamDevState::ACTIVE){
                    streamObject.set("ffmpegState", 1);
                }
                else if(list_stream_info[list_stream_num].is_ffmpeg_state == WebeCamDevState::INACTIVE){
                    streamObject.set("ffmpegState", 0);
                }
                if(list_stream_info[list_stream_num].is_qsv_state == WebeCamDevState::ACTIVE){
                    streamObject.set("qsvState", 1);
                }
                else if(list_stream_info[list_stream_num].is_qsv_state == WebeCamDevState::INACTIVE){
                    streamObject.set("qsvState", 0);
                }                    
                if(list_stream_info[list_stream_num].is_gst_out_state == WebeCamDevState::ACTIVE){
                    streamObject.set("gstState", 1);
                }
                else if(list_stream_info[list_stream_num].is_gst_out_state == WebeCamDevState::INACTIVE){
                    streamObject.set("gstState", 0);
                }                    
                listStreamArray.add(streamObject);
            }
            list_stream_num++;
            if(list_stream_num > CamDevInfor::streaming_num_max){
                break;
            }
            
        }
        camObject.set("streaming", listStreamArray);
        listCamArray.add(camObject);
    }
    object.set("camera", listCamArray);

    std::stringstream out;
    object.stringify(out);

    //std::cout << out.str() << std::endl;

    // save to file
    bool result = false;
    std::string configFile = this->filePath + this->fileName;
    FILE *file = NULL;

    file = fopen(configFile.c_str(), "w");
    if(file != NULL){
        fputs(out.str().c_str(), file);
        result = true;
    }
    fclose(file);
#endif
    return true;
}

bool MyConfig::init(void){
    return this->loadConfig();
}


void MyConfig::getConfigInfo(MyConfigInfo &info){
    this->updateLocker.lock();
    // copy list camera
    info.listCamera.devMap = this->configInfo.listCamera.devMap;
    this->updateLocker.unlock();
}

void MyConfig::updateListCam(DevInforMap &listCam){
    this->updateLocker.lock();
    this->configInfo.listCamera.devMap = listCam.devMap;
    this->saveConfig();
    this->updateLocker.unlock();
}
