/**
******************************************************************************
* @file           : box_config.cpp
* @brief          : source file of box configuration
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "config/box_config.h"
#include <iostream>
#include "share_function/share_function.h"

#ifdef WYZE_CAM_V2
#include "rapidjson/document.h"
using namespace rapidjson;
#endif

using namespace BeexConfig;

BoxConfig boxConfig;

void BoxConfig::init(void){
    std::cout << "BoxConfig - init" << std::endl;
    this->loadConfig();
}

bool BoxConfig::loadConfig(void){
    std::cout << "BoxConfig::loadConfig - start" << std::endl;

    bool result = false;
    // load file config
    if(BeexShareFunction::checkFile(BEEX_BOX_CONFIG_FILE)){
        FILE *file = NULL;
        size_t len;
        ssize_t read;
        char *line = NULL;
        file = fopen(BEEX_BOX_CONFIG_FILE, "r");
        if(file != NULL){
            read = getline(&line, &len, file);
            if(read != -1){
                std::string json_data(line);
                std::cout << "BoxConfig::loadConfig - read config file = " << json_data << std::endl;

                try{
#ifdef WYZE_CAM_V2
                    Document document;
                    document.Parse(json_data.c_str());
                    if(document.HasMember("serial")){
                        std::string serial = document["serial"].GetString();
                        if(!serial.empty()){
                            this->boxInfo.serial = serial;
                        }
                    }
                    if(document.HasMember("appPath")){
                        std::string appPath = document["appPath"].GetString();
                        if(!appPath.empty()){
                            this->boxInfo.appPath = appPath;
                        }
                    }
                    if(document.HasMember("firmwareOs")){
                        std::string firmwareOs = document["firmwareOs"].GetString();
                        if(!firmwareOs.empty()){
                            this->boxInfo.firmwareOs = firmwareOs;
                        }
                    }
#endif                    
#ifdef INTEL_NUC_BOX                    
                    JSON::Parser parser;
                    Dynamic::Var var;
                    var = parser.parse(json_data);
                    JSON::Object::Ptr object = var.extract<JSON::Object::Ptr>();

                    if(object->has("serial")){
                        std::string serial = object->get("serial");
                        if(!serial.empty()){
                            this->boxInfo.serial = serial;
                        }
                    }
                    if(object->has("appPath")){
                        std::string appPath = object->get("appPath");
                        if(!appPath.empty()){
                            this->boxInfo.appPath = appPath;
                        }
                    }
                    if(object->has("firmwareOs")){
                        std::string firmwareOs = object->get("firmwareOs");
                        if(!firmwareOs.empty()){
                            this->boxInfo.firmwareOs = firmwareOs;
                        }
                    }
#endif
                    result = true;
                }
                catch(const std::exception& e){
                    std::cout << "BoxConfig::loadConfig - exception when parse json data = " << json_data << std::endl;
                    result = false;
                }
            }
            else{
                std::cout << "BoxConfig::loadConfig - gateway config file not read" << std::endl;
            }
        }
        else{
            std::cout << "BoxConfig::loadConfig - gateway config file not opened" << std::endl;
        }
        fclose(file);
    }
    else{
        std::cout << "BoxConfig::loadConfig - gateway config file not found" << std::endl;
    }
    return result;
}


void BoxConfig::getBoxInfo(BoxInfo *info){
    *info = this->boxInfo;
}