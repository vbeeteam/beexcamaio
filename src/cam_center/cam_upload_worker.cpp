/**
******************************************************************************
* @file           : cam_upload_worker.cpp
* @brief          : Source file of worker for camera upload file
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_center/cam_upload_worker.h"
#include "logger/webe_logger.h"
#include "my_func/os_func.h"

#ifdef INTEL_NUC_BOX
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/FilePartSource.h>
#include "Poco/StreamCopier.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"
#include "Poco/SharedPtr.h"
#include "Poco/Net/KeyConsoleHandler.h"
#include "Poco/Net/ConsoleCertificateHandler.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"

#include "Poco/Net/AcceptCertificateHandler.h"

#include "Poco/JSON/Object.h"
#include <Poco/Exception.h>

using namespace Poco::Net;
using namespace Poco;

#endif
#include <curl/curl.h>
#include <sys/stat.h>
#include "event_report/report_api.h"
#include "config/box_config.h"
#include <unistd.h>



extern uint8_t check_streaming_num;

//CamUploadWorker camUploadWorker;


CamUploadWorker::CamUploadWorker(){
    this->url_upload_default = "https://s4.beex.vn:8099/upload";
    this->record_video = "record-camera";
    this->record_video_thumb = "record-video-thumbnail";    
    this->is_stop = false;
}

CamUploadWorker::~CamUploadWorker(){
}


void CamUploadWorker::run(void){
    // myLogger.sysLogger->info("Start Worker for upload file");
    //std::thread UpdateUrlThread(&CamUploadWorker::updateUrl, this);
    uint32_t timeout_check_streaming = OSFunc::getTimestamp();
    uint32_t timeout_check_error = 0;
    while (1){
        CamUploadPacket packet;
        if(this->getPacket(&packet)){
            // check action
            if(packet.action == this->record_video){
                // upload video storage
                // check file
                if(OSFunc::checkFile(packet.path_file_origin)){
                    std::string response;
                    
                    if(packet.time_duration > 0){
                        // check streaming
                        CamDevInfor cam_info[DevInforMap::cam_num_max];
                        uint8_t num = 0;
                        num = camInforMap.getAllDevMap(cam_info);
                        bool result_check = false;
                        // for (uint8_t i = 0; i < num; i++){
                        //     if((cam_info[i].streamingApp.is_ffmpeg_state == WebeCamDevState::ACTIVE) || (cam_info[i].streamingApp.is_gst_state == WebeCamDevState::ACTIVE)){
                        //         result_check = true;
                        //         break;
                        //     }
                        // }
                        if((check_streaming_num > 0) && 0){
                            result_check = true;
                        }
                        if(result_check == true){
                            if((OSFunc::getTimestamp() - timeout_check_streaming) >= 3600){
                                timeout_check_streaming = OSFunc::getTimestamp();
                                result_check = false;
                            }
                        }
                        if(result_check == false){
                            // myLogger.sysLogger->info("debug streaming num = {0} - {1} - {2}", std::to_string(check_streaming_num), std::to_string(OSFunc::getTimestamp()), std::to_string(timeout_check_streaming));
                            timeout_check_streaming = OSFunc::getTimestamp();
                            // upload origin file
                            uint32_t start_time = OSFunc::getTimestamp();
                            // continue;
                            if(this->uploadFile(&packet, &response)){
                                timeout_check_error = OSFunc::getTimestamp();
                                uint32_t end_time = OSFunc::getTimestamp();
                                uint32_t bandwidth_est = 0;
                                if(end_time - start_time > 0){
                                    bandwidth_est = packet.size / (end_time - start_time);
                                }
                                else{
                                    bandwidth_est = packet.size;
                                }
                                // myLogger.sysLogger->info("time = {0}, size = {1}", std::to_string(end_time - start_time), std::to_string(packet.size));
                                // myLogger.sysLogger->info("estimate bandwidth for upload = {0}:", std::to_string(bandwidth_est));
                                // mv to backup
                                if(rename(packet.path_file_origin.c_str(), packet.path_file_backup.c_str())){
                                    // myLogger.sysLogger->error("move to backup for file = {0}: error", packet.path_file_origin);
                                }
                                #ifdef INTEL_NUC_BOX
                                // upload thumb
                                JSON::Object object;
                                object.set("recordId", response);
                                std::stringstream out;
                                object.stringify(out);
                                packet.extra_info = out.str();
                                #endif

                                packet.action = this->record_video_thumb;
                                packet.path_file_origin = packet.path_sub_file;
                                // myLogger.sysLogger->info("Upload Thumb file of video for camera = {0}", packet.dev_serial);
                                // if(this->uploadFile(&packet, &response)){
                                //     // myLogger.sysLogger->info("Upload Thumb file of video for camera = {0} : success", packet.dev_serial);
                                // }
                                // else{
                                //     // myLogger.sysLogger->error("Upload Thumb file of video for camera = {0} : error", packet.dev_serial);
                                // }
                                sleep(1);
                            }
                            else{
                                if(OSFunc::getTimestamp() - timeout_check_error > 600){
                                    // myLogger.sysLogger->warn("CamUploadWorker - report upload error for camera = {0}", packet.dev_serial);
                                    BeexConfig::BoxInfo boxInfo;
                                    boxConfig.getBoxInfo(&boxInfo);
                                    BeexEventReport::ReportApi reporter(packet.dev_serial, boxInfo.serial);
                                    std::string type = "storage";
                                    std::string content = "error when upload file to: " + packet.upload_serv.baseStoragePrivateBackupUrl + ", "
                                                                                        + packet.upload_serv.baseStorageUrl + ", "
                                                                                        + packet.upload_serv.baseStorageUrlBackup;
                                    reporter.reportEvent(type, content);
                                    timeout_check_error = OSFunc::getTimestamp();
                                }  
                            }
                        }
                    }
                    else{
                        if(rename(packet.path_file_origin.c_str(), packet.path_file_backup.c_str())){
                            // myLogger.sysLogger->error("move to backup for file = {0}: error", packet.path_file_origin);
                        }                        
                    }
                }
            }
        }
        else{
            if(this->is_stop == true){
                //UpdateUrlThread.join();
                break;
            }
        }
        
        usleep(10000);
    }
    
}


void CamUploadWorker::updateUrl(void){
    while (1){
        CamDevInfor cam_info[DevInforMap::cam_num_max];
        uint8_t num = 0;
        num = camInforMap.getAllDevMap(cam_info);
        if(num > 0){
            for (uint8_t i = 0; i < num; i++)
            {
                if((cam_info[i].owner_state == WebeCamDevState::OWNER) && (cam_info[i].uploadApp.is_update)){
                    //this->url_upload_default = cam_info[i].uploadApp.baseStorageUrl;
                    break;
                }
            }
            
        }
        if(this->is_stop == true){
            break;
        }
        sleep(5);
    }
    
}


bool CamUploadWorker::uploadFile(CamUploadPacket *pkt, std::string *resp){
    std::string get_url = "";

    pkt->size = OSFunc::getSizeFile(pkt->path_file_origin);

    // try with baseStoragePrivateBackupUrl
    // if(pkt->upload_serv.baseStoragePrivateBackupUrl.find("https") != std::string::npos){
    //     get_url = pkt->upload_serv.baseStoragePrivateBackupUrl;
    //     if(this->uploadPacketByHttps(get_url, pkt, resp)){
    //         return true;
    //     }
    // }
    // else if(pkt->upload_serv.baseStoragePrivateBackupUrl.find("http") != std::string::npos){
    //     get_url = pkt->upload_serv.baseStoragePrivateBackupUrl;
    //     // if(this->uploadPacketByHttp(get_url, pkt, resp)){
    //     //     return true;
    //     // }
    // }

    // try with baseUrlOrigin
    if(pkt->upload_serv.baseStorageUrl.find("https") != std::string::npos){
        get_url = pkt->upload_serv.baseStorageUrl;
        if(this->uploadPacketByHttps(get_url, pkt, resp)){
            return true;
        }
    }
    else if(pkt->upload_serv.baseStorageUrl.find("http") != std::string::npos){
        get_url = pkt->upload_serv.baseStorageUrl;
        // if(this->uploadPacketByHttp(get_url, pkt, resp)){
        //     return true;
        // }
    }

    // if(check_streaming_num > 0){
    //     return false;
    // }

    // try with baseUrlBackup
    // if(pkt->upload_serv.baseStorageUrlBackup.find("https") != std::string::npos){
    //     get_url = pkt->upload_serv.baseStorageUrlBackup;
    //     if(this->uploadPacketByHttps(get_url, pkt, resp)){
    //         return true;
    //     }
    // }
    // else if(pkt->upload_serv.baseStorageUrlBackup.find("http") != std::string::npos){
    //     get_url = pkt->upload_serv.baseStorageUrlBackup;
    //     // if(this->uploadPacketByHttp(get_url, pkt, resp)){
    //     //     return true;
    //     // }
    // }     

    // if(check_streaming_num > 0){
    //     return false;
    // }       

    // if(get_url == ""){
    //     // try with urlUploadDefault
    //     if(this->url_upload_default.find("https") != std::string::npos){
    //         get_url = pkt->upload_serv.baseStorageUrl;
    //         if(this->uploadPacketByHttps(get_url, pkt, resp)){
    //             return true;
    //         }
    //     }
    //     else if(this->url_upload_default.find("http") != std::string::npos){
    //         get_url = pkt->upload_serv.baseStorageUrl;
    //         // if(this->uploadPacketByHttp(get_url, pkt, resp)){
    //         //     return true;
    //         // }
    //     }
    // }
    return false;
}

#ifdef INTEL_NUC_BOX
bool CamUploadWorker::uploadPacketByHttps(std::string url, CamUploadPacket *pkt, std::string *resp){
    try{
        myLogger.sysLogger->info("start upload file by Https: file = {0},  url = {1}", pkt->name_file, url);
        SSLInitializer sslInitializer;
    
        SharedPtr<InvalidCertificateHandler> ptrCert = new AcceptCertificateHandler(false);
        Context::Ptr ptrContext = new Context(Context::CLIENT_USE, "", "", BEEX_PATH + std::string("cacert.pem"), Context::VERIFY_NONE);
        SSLManager::instance().initializeClient(0, ptrCert, ptrContext);

        //Prepare request
        std::string url_upload = url + "?serial=" + pkt->dev_serial;
        Poco::URI uri(url_upload);

        Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort(), ptrContext);
        session.setKeepAlive(true);

        // check path
        std::string path(uri.getPathAndQuery());
        if (path.empty()){
            path = "/";
        }

        HTTPRequest request(HTTPRequest::HTTP_POST, path,    HTTPMessage::HTTP_1_1);
        request.setContentType("multipart/form-data;boundary=--------------------------801123347226233203649568");   

        // check extra-info
        myLogger.sysLogger->info("upload file = {0} by Https: extra-info = {1}", pkt->name_file, pkt->extra_info);

        HTMLForm form;
        form.setEncoding(HTMLForm::ENCODING_MULTIPART);
        form.set("file-upload", pkt->name_file);
        form.set("action", pkt->action);
        form.set("timestamp", pkt->time_start);
        form.set("extraInfo" , pkt->extra_info);
        form.addPart("file-upload", new FilePartSource(pkt->path_file_origin));
        form.prepareSubmit(request);

        HTTPSClientSession httpSession(uri.getHost(), uri.getPort(), ptrContext);
        // setup timeout for conn, send, recv
        httpSession.setTimeout(Poco::Timespan(10, 1), Poco::Timespan(10, 1), Poco::Timespan(10, 1));
        uint32_t time_limmit = 20000; // 400KB/s
        form.write(httpSession.sendRequest(request), &time_limmit);        

        myLogger.sysLogger->info("Upload File by Https - file = {0} : waiting a Response:", pkt->name_file);

        //bool result_upload = false;
        // if(this->recvResponseByHttps(&httpSession)){
        //     return true;
        // }
        //std::thread recvResponseByHttpsThread(&CamUploadWorker::recvResponseByHttps, this, &httpSession, &result_upload);
        //recvResponseByHttpsThread.detach();

        //sleep(1);
        
        // if(result_upload == false){
        //     myLogger.sysLogger->info("debug to stop httpresponse===================================================================");
        //     httpSession.abort();
        // }
        // recvResponseByHttpsThread.join();
        
        
        Poco::Net::HTTPResponse res;
        std::ostringstream msg_rsp;
        std::istream &is = httpSession.receiveResponse(res);
        Poco::StreamCopier::copyStream(is, msg_rsp);     

        //Get status code
        int statusCode = (int)res.getStatus();

        //Get status
        std::string status = res.getReason();

        httpSession.reset();
        session.reset();

        myLogger.sysLogger->info("Upload File by Https - file = {0}: MessageRsp = {1}, StatusCode = {2}, Status = {3}", pkt->name_file, msg_rsp.str(), std::to_string(statusCode), status);
        if((statusCode == 200)){
            *resp = msg_rsp.str();
            return true;
        }
        return false;        
    }
    catch(const std::exception& exception){
        myLogger.sysLogger->error("Exception occurred while uploading file by Https for file = {0},  url = {1} : {2}", pkt->name_file, url, exception.what());
        return false;
    }
    
}

bool CamUploadWorker::recvResponseByHttps(HTTPSClientSession *httpSession, bool *result){
    try
    {
        Poco::Net::HTTPResponse res;
        std::istream &is = httpSession->receiveResponse(res);
        std::ostringstream msg_rsp;
        std::string resp;
        Poco::StreamCopier::copyStream(is, msg_rsp);
        //Get status code
        int statusCode = (int)res.getStatus();

        //Get status
        std::string status = res.getReason();

        httpSession->reset();
        *result = true;

        myLogger.sysLogger->info("Upload File by Https - MessageRsp = {0}, StatusCode = {1}, Status = {2}", msg_rsp.str(), std::to_string(statusCode), status);
        if((statusCode == 200)){
            resp = msg_rsp.str();
            myLogger.sysLogger->info("resp = {0}", resp);
            return true;
        }
    }
    catch(const std::exception& e)
    {
        myLogger.sysLogger->error("Exception occurred while uploading file by Https = {0}", e.what());
    }
    

}


bool CamUploadWorker::uploadPacketByHttp(std::string url, CamUploadPacket *pkt, std::string *resp){
    try{
        myLogger.sysLogger->info("start upload file by Http: file = {0},  url = {1}", pkt->name_file, url);
        //Prepare request
        std::string url_upload = url + "?serial=" + pkt->dev_serial;
        Poco::URI uri(url_upload);

        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());

        session.setKeepAlive(true);

        // prepare path
        std::string path(uri.getPathAndQuery());
        if (path.empty()){
            path = "/";
        }

        // check extra-info
        myLogger.sysLogger->info("upload file by Http: extra-info = {0}", pkt->extra_info);

        HTTPRequest request(HTTPRequest::HTTP_POST, path,    HTTPMessage::HTTP_1_1);
        request.setContentType("multipart/form-data;boundary=--------------------------801123347226233203649568");
        HTMLForm form;
        form.setEncoding(HTMLForm::ENCODING_MULTIPART);
        form.set("file-upload", pkt->name_file);
        form.set("action", pkt->action);
        form.set("timestamp", pkt->time_start);
        form.set("extraInfo" , pkt->extra_info);
        form.addPart("file-upload", new FilePartSource(pkt->path_file_origin));
        form.prepareSubmit(request);

        HTTPClientSession httpSession(uri.getHost(), uri.getPort());
        // setup timeout for conn, send, recv
        httpSession.setTimeout(Poco::Timespan(10, 1), Poco::Timespan(10, 1), Poco::Timespan(10, 1));
        uint32_t time_limmit = 20000; // 400KB/s
        form.write(httpSession.sendRequest(request), &time_limmit);         

        Poco::Net::HTTPResponse res;
        std::ostringstream msg_rsp;
        std::istream &is = httpSession.receiveResponse(res);
        Poco::StreamCopier::copyStream(is, msg_rsp);      

        
        //Get status code
        int statusCode = (int)res.getStatus();

        //Get status
        std::string status = res.getReason();

        httpSession.reset();
        session.reset();

        myLogger.sysLogger->info("Upload File by Http : MessageRsp = {0}, StatusCode = {1}, Status = {2}", msg_rsp.str(), std::to_string(statusCode), status);
        if((statusCode == 200)){
            *resp = msg_rsp.str();
            return true;
        }
        return false;
        
    }
    catch(Poco::Exception& exception){
        //Set Response for Exception
        myLogger.sysLogger->error("Exception occurred while uploading file by Https for file = {0},  url = {1} : {2}", pkt->name_file, url, exception.what());
        return false;
    }        
}

#endif

#ifdef WYZE_CAM_V2

size_t CamUploadWorker::WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct UploadMemoryStruct *mem = (struct UploadMemoryStruct *)userp;

    mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
    if (mem->memory == NULL) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

bool CamUploadWorker::uploadPacketByHttps(std::string url, CamUploadPacket *pkt, std::string *resp){
    bool result = false;
    try{
        CURL *curl;
        CURLcode res;

        struct UploadMemoryStruct chunk;

        chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */
        chunk.size = 0;    /* no data at this point */


        struct curl_httppost *formpost = NULL;
        struct curl_httppost *lastptr = NULL;
        struct curl_slist *headerlist = NULL;
        static const char buf[] = "Expect:";      

        curl_global_init(CURL_GLOBAL_ALL);


        // prepare
        std::string url_upload = url + "?serial=" + pkt->dev_serial;

        /* Fill in the file upload field */
        curl_formadd(&formpost,
            &lastptr,
            CURLFORM_COPYNAME, "file-upload",
            CURLFORM_FILE, pkt->path_file_origin.c_str(),
            CURLFORM_END);

        curl_formadd(&formpost,
            &lastptr,
            CURLFORM_COPYNAME, "file-upload",
            CURLFORM_COPYCONTENTS, pkt->name_file.c_str(),
            CURLFORM_END);

        curl_formadd(&formpost,
            &lastptr,
            CURLFORM_COPYNAME, "action",
            CURLFORM_COPYCONTENTS, pkt->action.c_str(),
            CURLFORM_END);

        curl_formadd(&formpost,
            &lastptr,
            CURLFORM_COPYNAME, "timestamp",
            CURLFORM_COPYCONTENTS, pkt->time_start.c_str(),
            CURLFORM_END);
        curl_formadd(&formpost,
            &lastptr,
            CURLFORM_COPYNAME, "extraInfo",
            CURLFORM_COPYCONTENTS, pkt->extra_info.c_str(),
            CURLFORM_END);
            
        /* Fill in the submit field too, even if this is rarely needed */
        curl_formadd(&formpost,
            &lastptr,
            CURLFORM_COPYNAME, "submit",
            CURLFORM_COPYCONTENTS, "send",
            CURLFORM_END);

        curl = curl_easy_init();

        if (curl) {

            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

            /* send all data to this function  */
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, this->WriteMemoryCallback);

            /* we pass our 'chunk' struct to the callback function */
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

            /* some servers don't like requests that are made without a user-agent
            field, so we provide curl */
            curl_easy_setopt(curl, CURLOPT_USERAGENT, "my3dsoftware");

            /* initalize custom header list (stating that Expect: 100-continue is not
            wanted */
            headerlist = curl_slist_append(headerlist, buf);

            /* what URL that receives this POST */        
            //curl_easy_setopt(curl, CURLOPT_URL, "http://api.sketchfab.com/v1/models");
            curl_easy_setopt(curl, CURLOPT_URL, url_upload.c_str());
        
            // if ((argc == 2) && (!strcmp(argv[1], "noexpectheader")))
            //     /* only disable 100-continue header if explicitly requested */
            //     curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
            curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

            // show all info
            // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

            /* Perform the request, res will get the return code */
            res = curl_easy_perform(curl);
            /* Check for errors */
            if (res != CURLE_OK){
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
            }
            else {
            printf("%lu bytes retrieved\n", (long)chunk.size);
                printf("%s\n", chunk.memory);
                std::string msg(chunk.memory);
                *resp = msg;
                result = true;
            }

            /* always cleanup */
            curl_easy_cleanup(curl);

            /* then cleanup the formpost chain */
            curl_formfree(formpost);
            /* free slist */
            curl_slist_free_all(headerlist);
        }
    }
    catch(const std::exception& exception){
        // myLogger.sysLogger->error("Exception occurred while uploading file by Https for file = {0},  url = {1} : {2}", pkt->name_file, url, exception.what());
    }
    return result;
    
}

#endif


bool CamUploadWorker::pushPacket(CamUploadPacket *pkt){
    bool result = false;

    if(this->pktQueue.size() <= this->queue_size){
        this->myLocker.lock();
        this->pktQueue.push(*pkt);
        result = true;
        this->myLocker.unlock();
    }
    return result;
}


bool CamUploadWorker::getPacket(CamUploadPacket *pkt){
    bool result = false;

    if(this->pktQueue.size() > 0){
        this->myLocker.lock();
        *pkt = this->pktQueue.front();
        this->pktQueue.pop();
        result = true;
        this->myLocker.unlock();
    }
    return result;
}