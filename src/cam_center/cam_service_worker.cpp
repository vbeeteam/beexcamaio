/**
******************************************************************************
* @file           : cam_service_worker.cpp
* @brief          : Source file of worker for camera service
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_center/cam_service_worker.h"
#include "logger/webe_logger.h"
#include <unistd.h>
#include <string.h>
#include <thread>
#include <ostream>

#include "my_func/os_func.h"
#include "my_resource/box_resource.h"
#include "data_struct/data_config.h"
#include "config/box_config.h"
#include "data_struct/cam_data_struct.h"
#ifdef WYZE_CAM_V2
#include "rapidjson/document.h"
#include "rapidjson/writer.h"

using namespace rapidjson;
#endif
/* Object -------------------------------------------------------------------*/
CamServiceWorker camServiceWorker;


// check streaming
uint8_t check_streaming_num = 0;

/* Methods ------------------------------------------------------------------*/


IpcWBCorePacket::IpcWBCorePacket(){

}

IpcWBCorePacket::~IpcWBCorePacket(){

}

IpcWBCore::IpcWBCore(){
    this->conn_state = false;
    this->ipc_port = 8001;
    this->ipc_host = "127.0.0.1";
}

IpcWBCore::~IpcWBCore()
{
}

void IpcWBCore::run(void){
    //myLogger.sysLogger->info("Start IPC to WBCore.....OK!");
    while (1)
    {
        if(this->conn_state == false){
            this->ipc_client.close();
            this->ipc_client = this->ipc_socket.init();
            this->init();
        }
        sleep(1);
    }
    
}


void IpcWBCore::init(void){
    if(this->ipc_client.connect(this->ipc_host, this->ipc_port)){
        this->conn_state = true;
        //myLogger.sysLogger->info("IPC to WBCore on port = {0}: Connected", std::to_string(this->ipc_port));
    }
    //myLogger.sysLogger->info("IPC to WBCore on port = {0}: False", std::to_string(this->ipc_port));
}

bool IpcWBCore::sendPacket(IpcWBCorePacket *packet){
	uint8_t buff_send[TcpConnection::packet_len_max];
	uint16_t len = 0;
	std::string data_str;

    memset(buff_send, 0, sizeof(buff_send));
    packet->dev_type = CamDevInfor::dev_type;
	
	data_str = "DC*" + packet->serial + "*" + std::to_string(packet->serv_type) + "*" + 
		std::to_string(packet->dev_type) + "*" + std::to_string(packet->body_len) + "*";
	len = data_str.length();
	for (uint16_t i = 0; i < len; i++)
	{
		buff_send[i] = data_str.c_str()[i];
	}
	for (uint16_t i = 0; i < packet->body_len; i++)
	{
		buff_send[i + len] = packet->body[i];
	}
	len += packet->body_len;
    std::string get_body((char *)buff_send);
    if(this->conn_state == true){
        this->tcpLocker.lock();
        if (this->ipc_client.send(buff_send, len))
        {
            ////myLogger.sysLogger->info("IPC WBCore send packet: success {0}", get_body);
            usleep(20000);
            this->tcpLocker.unlock();
            return true;
        }
        else
        {
            //myLogger.sysLogger->info("IPC WBCore send packet: false");
            this->conn_state = false;
            this->tcpLocker.unlock();
            return false;
        }
    }
    return false;

}



bool IpcWBCore::recvPacket(IpcWBCorePacket *packet)
{
	char buff[TcpConnection::packet_len_max];
	int len = 0;
	
	memset(buff, 0, sizeof(buff));
	len = this->ipc_client.receive((uint8_t *)buff, TcpConnection::packet_len_max);
	if (len > 0)
	{
		std::string data_recv(buff);
		std::cout << "- Cam Comm: recv data - " << data_recv << std::endl;
		// check Magic:
		uint16_t get_magic = ((uint16_t)buff[0] << 8) | (uint16_t)buff[1];
		if (get_magic == 0x4443)
		{
			size_t pos = 0;
			std::string data_split[10];
			uint8_t i = 0;
			while ((pos = data_recv.find("*")) != std::string::npos){
				data_split[i] = data_recv.substr(0, pos);
				data_recv.erase(0, pos + 1);
				i++;
				if (i == 5){
					break;
				}
			}
			// get packet
			packet->serial = data_split[1];
			packet->serv_type = (uint16_t)atoi(data_split[2].c_str());
			packet->dev_type = (uint16_t)atoi(data_split[3].c_str());
			// body length
			uint16_t get_body_len = (uint16_t)atoi(data_split[4].c_str());
			if (get_body_len <= TcpConnection::packet_len_max)
			{
				packet->body_len = get_body_len;
			}
			else
			{
                //myLogger.sysLogger->info("IPC WBCore: get error packet");
				return 0;
			}
			for (uint16_t i = 0; i < packet->body_len; i++)
			{
				packet->body[i] = buff[len-packet->body_len + i];
			}
			return true;
		}
		else
		{
			//myLogger.sysLogger->info("IPC WBCore: get error packet");
		}
	}
	else
	{
		this->conn_state = false;
	}
	return false;
}

CamServiceWorker::CamServiceWorker(){
    this->sync_cam_status = WebeCamDevState::NOT_SYNC;
}

CamServiceWorker::~CamServiceWorker(){
}


void CamServiceWorker::run(void){
    //myLogger.sysLogger->info("Start Worker for CameraService........OK!");
    std::thread IpcWBCoreThread(&IpcWBCore::run, &this->ipcWBCore);
    std::thread HandleCamStatusThread(&CamServiceWorker::handleCamStatus, this);
    std::thread handleRecvServThread(&CamServiceWorker::handleRecvServ, this);

    handleRecvServThread.join();
}


void CamServiceWorker::handleCamStatus(void){
    while (1){
        CamDevInfor cam_dev_infor[DevInforMap::cam_num_max];
        uint8_t cam_num = 0;

        cam_num = camInforMap.getAllDevMap(cam_dev_infor);
        //myLogger.sysLogger->info("Monitor map for camera: number = {0}", std::to_string(cam_num));
        if(cam_num > 0){
            for (uint8_t i = 0; i < cam_num; i++){
                cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                //myLogger.sysLogger->info("serial = {0}, authenState = {1}, connState = {2}, camType = {3},is_running_upload = {4}, is_running_app = {5}, streamingNum = {6}", 
                // cam_dev_infor[i].dev_serial, std::to_string(cam_dev_infor[i].authen_state), 
                // std::to_string(cam_dev_infor[i].conn_state), cam_dev_infor[i].camera_type, 
                // std::to_string(cam_dev_infor[i].uploadApp.active_status), std::to_string(cam_dev_infor[i].is_running_app), std::to_string(check_streaming_num));

                if(1){
                    if(cam_dev_infor[i].dev_serial == cam_dev_infor[i].mac_addr){
                        // request serial
                        this->requestSerial(cam_dev_infor[i].mac_addr);
                    }
                    else{
                        // check authen
                        if(cam_dev_infor[i].authen_state == WebeCamDevState::NOT_AUTHEN){
                            //myLogger.sysLogger->info("request authen for camera: serial = {0}, MAC = {1}", cam_dev_infor[i].dev_serial, cam_dev_infor[i].mac_addr);
                            this->requestAuthen(cam_dev_infor[i].dev_serial);
                        }
                        else if(cam_dev_infor[i].authen_state == WebeCamDevState::AUTHEN){
                            //ping
                            this->ping(&cam_dev_infor[i]);

                            // request serviceInfor T=1h
                            if((OSFunc::getTimestamp() - cam_dev_infor[i].last_time_update_serv > 900) || (cam_dev_infor[i].is_update_serv == false)){
                                if(camInforMap.checkDevMap(cam_dev_infor[i].dev_serial)){
                                    CamDevInfor camera = camInforMap.getDevMap(cam_dev_infor[i].dev_serial);
                                    camera.last_time_update_serv = OSFunc::getTimestamp();
                                    camInforMap.insertDevMap(camera);                         
                                }                                
                                usleep(100000);
                                this->reqCamServInfo(cam_dev_infor[i].dev_serial);
                                this->reportInfoCam(&cam_dev_infor[i]);
                            }
                        }
                    }
                }
            }
        }

        // if(janusManageWorker.restart_janus_flag == WebeCamDevState::ACTIVE){
        //     this->reqRestartJanusEngine();
        // }

        // check sync for all camera
        if(this->sync_cam_status == WebeCamDevState::NOT_SYNC){
            //this->reqSyncCam();
            CamDevInfor cam_info;
            BeexConfig::BoxInfo boxInfo;
            boxConfig.getBoxInfo(&boxInfo);            
            cam_info.dev_serial = boxInfo.serial;
#ifdef WYZE_CAM_V2
            cam_info.mac_addr = "rtsp://localhost:8554/unicast-" + cam_info.dev_serial; 
            cam_info.rtsp_url = "rtsp://localhost:8554/unicast";
            cam_info.camera_type = "stream";
            cam_info.conn_state = WebeCamDevState::CONNECTED;
#endif
#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1
            cam_info.mac_addr = "";
            cam_info.rtsp_url = "";
            // cam_info.conn_state = WebeCamDevState::CONNECTED;
            cam_info.camera_type = "onvif";
            cam_info.capture_state = false;
#endif
            cam_info.source = cam_info.rtsp_url;
            cam_info.onvifInfor.userName = "";
            cam_info.onvifInfor.password = "";

            // cam_info.camera_type = CamDevInfor::dev_type;
            cam_info.authen_state = WebeCamDevState::NOT_AUTHEN;
            
            cam_info.owner_state = WebeCamDevState::OWNER;

            camInforMap.insertDevMap(cam_info);
            this->sync_cam_status = WebeCamDevState::CamDevState::SYNC;            

        }
        sleep(5);
    }
}


void CamServiceWorker::handleRecvServ(void){
    while (1)
    {
        IpcWBCorePacket packet;

        memset(packet.body, 0, sizeof(packet.body));
        if(this->ipcWBCore.recvPacket(&packet) == true){
            std::string get_body((char *)packet.body);
            //myLogger.sysLogger->info("get Service: - serial = {0} , serviceType = {1} , body = {2}", packet.serial, std::to_string(packet.serv_type), get_body);
            
            if(packet.serv_type == CamServiceWorker::request_serial){
                std::string get_mac_addr = "";
                std::string get_serial = "";
                uint8_t i=0;
                for(i=0; i<packet.body_len-2; i++){
                    if(packet.body[i+2] == ';'){
                        break;
                    }
                    get_mac_addr += packet.body[i+2];
                }
                for (i=i+1; i < packet.body_len-2; i++){
                    get_serial += packet.body[i+2];
                }
                //myLogger.sysLogger->info("get Request Seria - ACK: - mac_addr = {0}, - serial = {1}", get_mac_addr, get_serial);
                if(get_serial != ""){
                    CamDevInfor cam_dev_infor[DevInforMap::cam_num_max];
                    uint8_t cam_num = 0;
                    cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                    for (i = 0; i < cam_num; i++){
                        if(cam_dev_infor[i].mac_addr == get_mac_addr){
                            ////myLogger.sysLogger->info("erase dev: {0}", cam_dev_infor[i].dev_serial);
                            camInforMap.eraseDevMap(cam_dev_infor[i].dev_serial);

                            CamDevInfor new_cam = cam_dev_infor[i];
                            new_cam.dev_serial = get_serial;
                            new_cam.serial_state = WebeCamDevState::SERIAL;
                            new_cam.authen_state = WebeCamDevState::NOT_AUTHEN;
                            camInforMap.insertDevMap(new_cam);
                        }
                    }
                }
            }
            else if(packet.serv_type == CamServiceWorker::authen_service){
                if(camInforMap.checkDevMap(packet.serial) == true){
                    CamDevInfor cam_infor;
                    if(camInforMap.checkDevMap(packet.serial) == true){
                        cam_infor = camInforMap.getDevMap(packet.serial);
                        if(packet.body[1] == 1){
                            //myLogger.sysLogger->info("get Authen Service_ACK: - serial = {0}, result = true", packet.serial);
                            cam_infor.authen_state = WebeCamDevState::AUTHEN;
                            cam_infor.owner_state = WebeCamDevState::OWNER;
                            camInforMap.insertDevMap(cam_infor);
                        }
                    }
                }
            }

            else if(packet.serv_type == CamServiceWorker::start_view_service){
                //myLogger.sysLogger->info("get Start View Service: - serial = {0}", packet.serial);
                if(camInforMap.checkDevMap(packet.serial) == true){
                    CamDevInfor cam_infor;
                    if(camInforMap.checkDevMap(packet.serial) == true){
                        cam_infor = camInforMap.getDevMap(packet.serial);
                        
                        std::string json_data = "";

                        for (uint16_t i = 0; i < packet.body_len; i++)
                        {
                            json_data += (char )packet.body[i];
                        }
                        try
                        {
#ifdef WYZE_CAM_V2
// {"hostRtsp":"","streamId":7555,"strength":80,"hostMedia":"203.171.21.16","width":1280,"portMedia":14607,"serviceId":4,"height":720,"hostPrivateMedia":"172.10.10.16"}
                            Document document;
                            document.Parse(json_data.c_str());
                            if(document.HasMember("hostMedia") && document.HasMember("portMedia") && document.HasMember("serviceId")){
                                std::string get_host = document["hostMedia"].GetString();
                                uint32_t get_port = document["portMedia"].GetUint64();
                                uint16_t serviceId = document["serviceId"].GetUint64();
                                /// check streaming info
                                CamStreamingInfo streamingInfo;
                                std::string key_streaming = get_host + ":" + std::to_string(get_port);
                                bool check_streaming = true;

                                if(cam_infor.listStreaming.find(key_streaming) != cam_infor.listStreaming.end()){
                                    //myLogger.sysLogger->info("streaming already exist");
                                    streamingInfo = cam_infor.listStreaming[key_streaming];
                                    streamingInfo.serviceId = serviceId;
                                }
                                else{
                                    //myLogger.sysLogger->info("new streaming");
                                    streamingInfo.host_media = get_host;
                                    streamingInfo.port_media = std::to_string(get_port);
                                    streamingInfo.serviceId = serviceId;

                                    // check old streaming duplicate host and  in camera + serviceId
                                    CamStreamingInfo listStreaming[CamDevInfor::streaming_num_max];
                                    uint8_t streaming_num = 0;
                                    std::map<std::string, CamStreamingInfo>::iterator map;   

                                    for (map = cam_infor.listStreaming.begin(); map != cam_infor.listStreaming.end(); map++) {
                                        listStreaming[streaming_num] = map->second;

                                        if((listStreaming[streaming_num].host_media == get_host) && 
                                            (listStreaming[streaming_num].port_media != std::to_string(get_port)) && 
                                            (listStreaming[streaming_num].serviceId == serviceId)){
                                                CamDevInfor getCam = camInforMap.getDevMap(cam_infor.dev_serial);
                                                std::string key = map->first;
                                                CamStreamingInfo getStream = getCam.listStreaming[key];
                                                getStream.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                                getStream.is_qsv_state = WebeCamDevState::INACTIVE;
                                                getStream.is_gst_out_state = WebeCamDevState::INACTIVE;
                                                cam_infor.listStreaming[key] = getStream;
                                                camInforMap.insertDevMap(cam_infor);
                                        }

                                        streaming_num++;
                                        if(streaming_num > CamDevInfor::streaming_num_max){
                                            break;
                                        }
                                    }                                

                                    if(cam_infor.listStreaming.size() == CamDevInfor::streaming_num_max){
                                        //myLogger.sysLogger->info("streaming limited for camera = {0}", cam_infor.dev_serial);
                                        check_streaming = false;
                                    }
                                    else{
                                        check_streaming_num++;
                                    }
                                }
                                // check media info duplicate
                                CamDevInfor cam_dev_infor[DevInforMap::cam_num_max];
                                uint8_t cam_num = 0;

                                cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                if(cam_num > 0){
                                    for (uint8_t i = 0; i < cam_num; i++){
                                        cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                        if(cam_dev_infor[i].dev_serial == packet.serial){
                                            continue;
                                        }
                                        else{
                                            if(cam_dev_infor[i].listStreaming.find(key_streaming) != cam_dev_infor[i].listStreaming.end()){
                                                //myLogger.sysLogger->info("stop streaming duplicate for camera = {0} - keyStreaming = {1}", cam_dev_infor[i].dev_serial, key_streaming);
                                                CamStreamingInfo stream = cam_dev_infor[i].listStreaming[key_streaming];
                                                stream.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                                stream.is_qsv_state = WebeCamDevState::INACTIVE;
                                                stream.is_gst_out_state = WebeCamDevState::INACTIVE;
                                                cam_dev_infor[i].listStreaming[key_streaming] = stream;
                                                camInforMap.insertDevMap(cam_dev_infor[i]);  
                                            }
                                            
                                        }
                                    }
                                }                            
                                if(check_streaming){
                                    // choose type Streaming
                                    int streamingType = CamServiceId::normal_streaming;
                                    BeexBoxResource::BoxResource resource;
                                    if(cam_infor.is_smart_streaming == WebeCamDevState::ACTIVE){
                                        if(resource.getCpuUsage() < 200){
                                            //streamingInfo.is_qsv_state = WebeCamDevState::ACTIVE;
                                            streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                            streamingInfo.is_gst_out_state = WebeCamDevState::ACTIVE; 
                                            //streamingType = CamServiceId::smart_streaming;
                                            #ifdef PI_BOX
                                            streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                            streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;                                       
                                            #endif
                                        }
                                        else{
                                            streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                            //streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;
                                            streamingInfo.is_gst_out_state = WebeCamDevState::ACTIVE;
                                        }
                                    }
                                    else
                                    {
                                        streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                        //streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;   
                                        streamingInfo.is_gst_out_state = WebeCamDevState::ACTIVE;                             
                                    }

                                    cam_infor.listStreaming[key_streaming] = streamingInfo;
                                    camInforMap.insertDevMap(cam_infor);   

                                    // update to file config
                                    camConfig.writeConfig(&camInforMap);
                                    
                                    // ACK
                                    IpcWBCorePacket packet;
                                    StringBuffer jsonBuffer;
                                    Writer<StringBuffer> writer(jsonBuffer);
                                    std::string jsonString;

                                    writer.StartObject();
                                    writer.Key("serviceId");
                                    writer.Uint64(streamingType);
                                    writer.Key("portMedia");
                                    writer.Uint64(get_port);
                                    writer.Key("hostMedia");
                                    writer.String(get_host.c_str());
                                    writer.EndObject();

                                    jsonString = jsonBuffer.GetString();

                                    memset(packet.body, 0, sizeof(packet.body));
                                    ////myLogger.sysLogger->info("Ping for camera: serial = {0}", dev_serial);
                                    packet.serial = cam_infor.dev_serial;
                                    packet.serv_type = CamServiceWorker::start_view_service;    
                                    if(jsonString.length() < IpcWBCorePacket::body_max){
                                        packet.body_len = jsonString.length();
                                        for(uint8_t i=0; i<packet.body_len; i++){
                                            packet.body[i] = jsonString.c_str()[i];
                                        }
                                    }
                                    this->ipcWBCore.sendPacket(&packet);
                                }                                
                            }
#endif                            
#ifdef INTEL_NUC_BOX                             
                            Poco::JSON::Parser parse;
                            Poco::Dynamic::Var result = parse.parse(json_data);
                            Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();

                            std::string get_host = object->get("hostMedia");
                            std::string get_port = object->get("portMedia");
                            uint16_t serviceId = object->get("serviceId");

                            /// check streaming info
                            CamStreamingInfo streamingInfo;
                            std::string key_streaming = get_host + ":" + get_port;
                            bool check_streaming = true;

                            if(cam_infor.listStreaming.find(key_streaming) != cam_infor.listStreaming.end()){
                                //myLogger.sysLogger->info("streaming already exist");
                                streamingInfo = cam_infor.listStreaming[key_streaming];
                                streamingInfo.serviceId = serviceId;
                            }
                            else{
                                //myLogger.sysLogger->info("new streaming");
                                streamingInfo.host_media = get_host;
                                streamingInfo.port_media = get_port;
                                streamingInfo.serviceId = serviceId;

                                // check old streaming duplicate host and  in camera + serviceId
                                CamStreamingInfo listStreaming[CamDevInfor::streaming_num_max];
                                uint8_t streaming_num = 0;
                                std::map<std::string, CamStreamingInfo>::iterator map;   

                                for (map = cam_infor.listStreaming.begin(); map != cam_infor.listStreaming.end(); map++) {
                                    listStreaming[streaming_num] = map->second;

                                    if((listStreaming[streaming_num].host_media == get_host) && 
                                        (listStreaming[streaming_num].port_media != get_port) && 
                                        (listStreaming[streaming_num].serviceId == serviceId)){
                                            CamDevInfor getCam = camInforMap.getDevMap(cam_infor.dev_serial);
                                            std::string key = map->first;
                                            CamStreamingInfo getStream = getCam.listStreaming[key];
                                            getStream.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                            getStream.is_qsv_state = WebeCamDevState::INACTIVE;
                                            getStream.is_gst_out_state = WebeCamDevState::INACTIVE;
                                            cam_infor.listStreaming[key] = getStream;
                                            camInforMap.insertDevMap(cam_infor);
                                    }

                                    streaming_num++;
                                    if(streaming_num > CamDevInfor::streaming_num_max){
                                        break;
                                    }
                                }                                

                                if(cam_infor.listStreaming.size() == CamDevInfor::streaming_num_max){
                                    //myLogger.sysLogger->info("streaming limited for camera = {0}", cam_infor.dev_serial);
                                    check_streaming = false;
                                }
                                else{
                                    check_streaming_num++;
                                }
                            }
                            // check media info duplicate
                            CamDevInfor cam_dev_infor[DevInforMap::cam_num_max];
                            uint8_t cam_num = 0;

                            cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                            if(cam_num > 0){
                                for (uint8_t i = 0; i < cam_num; i++){
                                    cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                    if(cam_dev_infor[i].dev_serial == packet.serial){
                                        continue;
                                    }
                                    else{
                                        if(cam_dev_infor[i].listStreaming.find(key_streaming) != cam_dev_infor[i].listStreaming.end()){
                                            //myLogger.sysLogger->info("stop streaming duplicate for camera = {0} - keyStreaming = {1}", cam_dev_infor[i].dev_serial, key_streaming);
                                            CamStreamingInfo stream = cam_dev_infor[i].listStreaming[key_streaming];
                                            stream.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                            stream.is_qsv_state = WebeCamDevState::INACTIVE;
                                            stream.is_gst_out_state = WebeCamDevState::INACTIVE;
                                            cam_dev_infor[i].listStreaming[key_streaming] = stream;
                                            camInforMap.insertDevMap(cam_dev_infor[i]);  
                                        }
                                        
                                    }
                                }
                            }                            
                            if(check_streaming){
                                // choose type Streaming
                                int streamingType = CamServiceId::normal_streaming;
                                BeexBoxResource::BoxResource resource;
                                if(cam_infor.is_smart_streaming == WebeCamDevState::ACTIVE){
                                    if(resource.getCpuUsage() < 200){
                                        //streamingInfo.is_qsv_state = WebeCamDevState::ACTIVE;
                                        streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                        streamingInfo.is_gst_out_state = WebeCamDevState::ACTIVE; 
                                        //streamingType = CamServiceId::smart_streaming;
                                        #ifdef PI_BOX
                                        streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                        streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;                                       
                                        #endif
                                    }
                                    else{
                                        streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                        //streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;
                                        streamingInfo.is_gst_out_state = WebeCamDevState::ACTIVE;
                                    }
                                }
                                else
                                {
                                    streamingInfo.is_ffmpeg_state = WebeCamDevState::ACTIVE;
                                    //streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;   
                                    streamingInfo.is_gst_out_state = WebeCamDevState::ACTIVE;                             
                                }

                                cam_infor.listStreaming[key_streaming] = streamingInfo;
                                camInforMap.insertDevMap(cam_infor);   

                                // update to file config
                                camConfig.writeConfig(&camInforMap);
                                
                                // ACK
                                IpcWBCorePacket packet;
                                Poco::JSON::Object object;
                                std::stringstream out;

                                object.set("serviceId", streamingType);
                                object.set("portMedia", get_port);
                                object.set("hostMedia", get_host);

                                object.stringify(out);

                                memset(packet.body, 0, sizeof(packet.body));
                                ////myLogger.sysLogger->info("Ping for camera: serial = {0}", dev_serial);
                                packet.serial = cam_infor.dev_serial;
                                packet.serv_type = CamServiceWorker::start_view_service;    
                                if(out.str().length() < IpcWBCorePacket::body_max){
                                    packet.body_len = out.str().length();
                                    for(uint8_t i=0; i<packet.body_len; i++){
                                        packet.body[i] = out.str().c_str()[i];
                                    }
                                }
                                this->ipcWBCore.sendPacket(&packet);
                            }
#endif                            
                        }
                        catch(const std::exception& e){
                            //myLogger.sysLogger->error("exception when parse json in startView: {0}", e.what());
                        }
                    }
                }
            }
            
            else if(packet.serv_type == CamServiceWorker::zoom_state_view_service){
                //myLogger.sysLogger->info("get Zoom State View Service: - serial = {0}", packet.serial);
                if(camInforMap.checkDevMap(packet.serial) == true){
                    CamDevInfor cam_infor;
                    if(camInforMap.checkDevMap(packet.serial) == true){
                        cam_infor = camInforMap.getDevMap(packet.serial);
                        
                        std::string json_data = "";

                        for (uint16_t i = 0; i < packet.body_len; i++)
                        {
                            json_data += (char )packet.body[i];
                        }
                        try
                        {
#ifdef WYZE_CAM_V2
                            Document document;
                            document.Parse(json_data.c_str());
                            if(document.HasMember("hostMedia") && document.HasMember("portMedia") && document.HasMember("serviceId") && document.HasMember("zoomState")){
                                std::string get_host = document["hostMedia"].GetString();
                                uint32_t get_port = document["portMedia"].GetUint64();
                                uint16_t serviceId = document["serviceId"].GetUint64();
                                uint16_t get_zoom_state = document["zoomState"].GetUint();
                                /// check streaming info
                                CamStreamingInfo streamingInfo;
                                std::string key_streaming = get_host + ":" + std::to_string(get_port);
                                bool check_streaming = true;

                                if(cam_infor.listStreaming.find(key_streaming) != cam_infor.listStreaming.end()){
                                    
                                    streamingInfo = cam_infor.listStreaming[key_streaming];
                                    streamingInfo.zoom_state_pre = streamingInfo.zoom_state;
                                    if(cam_infor.is_smart_streaming == WebeCamDevState::ACTIVE){
                                        if(get_zoom_state == 1){
                                            streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_FULL;
                                        }
                                        else if(get_zoom_state == 3){
                                            streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_30;
                                        }

                                        if(streamingInfo.zoom_state_pre != streamingInfo.zoom_state){
                                            streamingInfo.is_zoom_switching = true;
                                        }
                                    }
                                    else{
                                        if(get_zoom_state == 1){
                                            streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_60;
                                        }
                                        else if(get_zoom_state == 3){
                                            streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_30;
                                        }

                                        if(streamingInfo.zoom_state_pre != streamingInfo.zoom_state){
                                            streamingInfo.is_zoom_switching = true;
                                        }
                                    }
                                    //myLogger.sysLogger->info("find streaming to update zoom state = {0}", std::to_string(streamingInfo.zoom_state));
                                    cam_infor.listStreaming[key_streaming] = streamingInfo;
                                    camInforMap.insertDevMap(cam_infor);  

                                    // update to file config
                                    camConfig.writeConfig(&camInforMap);                                                                 
                                }
                            }                                

#endif                                                            
#ifdef INTEL_NUC_BOX                            
                            Poco::JSON::Parser parse;
                            Poco::Dynamic::Var result = parse.parse(json_data);
                            Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();
                            
                            int serviceId = object->get("serviceId");
                            std::string get_host = object->get("hostMedia");
                            std::string get_port = object->get("portMedia");

                            int get_zoom_state = object->get("zoomState");

                            /// check streaming info
                            CamStreamingInfo streamingInfo;
                            std::string key_streaming = get_host + ":" + get_port;
                            bool check_streaming = true;

                            if(cam_infor.listStreaming.find(key_streaming) != cam_infor.listStreaming.end()){
                                
                                streamingInfo = cam_infor.listStreaming[key_streaming];
                                streamingInfo.zoom_state_pre = streamingInfo.zoom_state;
                                if(cam_infor.is_smart_streaming == WebeCamDevState::ACTIVE){
                                    if(get_zoom_state == 1){
                                        streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_FULL;
                                    }
                                    else if(get_zoom_state == 3){
                                        streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_30;
                                    }

                                    if(streamingInfo.zoom_state_pre != streamingInfo.zoom_state){
                                        streamingInfo.is_zoom_switching = true;
                                    }
                                }
                                else{
                                    if(get_zoom_state == 1){
                                        streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_60;
                                    }
                                    else if(get_zoom_state == 3){
                                        streamingInfo.zoom_state = CamStreamZoomState::ZOOM_STATE_30;
                                    }

                                    if(streamingInfo.zoom_state_pre != streamingInfo.zoom_state){
                                        streamingInfo.is_zoom_switching = true;
                                    }
                                }
                                //myLogger.sysLogger->info("find streaming to update zoom state = {0}", std::to_string(streamingInfo.zoom_state));
                                cam_infor.listStreaming[key_streaming] = streamingInfo;
                                camInforMap.insertDevMap(cam_infor);  

                                // update to file config
                                camConfig.writeConfig(&camInforMap);                                                                 
                            }   
#endif                                                     
                        }
                        catch(const std::exception& e){
                            //myLogger.sysLogger->error("exception when parse json in zoom state view: {0}", e.what());
                        }
                    }
                }
            }

            else if(packet.serv_type == CamServiceWorker::stop_view_service){
                //myLogger.sysLogger->info("get Stop View Service: - serial = {0}", packet.serial);
                if(camInforMap.checkDevMap(packet.serial) == true){
                    CamDevInfor cam_infor;
                    if(camInforMap.checkDevMap(packet.serial) == true){
                        cam_infor = camInforMap.getDevMap(packet.serial);
                        std::string json_data = "";

                        for (uint16_t i = 0; i < packet.body_len; i++){
                            json_data += (char )packet.body[i];
                        }
                        try{
#ifdef WYZE_CAM_V2
                            Document document;
                            document.Parse(json_data.c_str());
                            if(document.HasMember("hostMedia") && document.HasMember("portMedia")){
                                std::string get_host = document["hostMedia"].GetString();
                                uint32_t get_port = document["portMedia"].GetUint64();
                                /// check streaming info
                                CamStreamingInfo streamingInfo;
                                std::string key_streaming = get_host + ":" + std::to_string(get_port);
                                if(cam_infor.listStreaming.find(key_streaming) != cam_infor.listStreaming.end()){
                                    //myLogger.sysLogger->info("streaming already exist");
                                    streamingInfo = cam_infor.listStreaming[key_streaming];
                                    streamingInfo.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                    streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;
                                    streamingInfo.is_gst_out_state = WebeCamDevState::INACTIVE;

                                    cam_infor.listStreaming[key_streaming] = streamingInfo;
                                    camInforMap.insertDevMap(cam_infor);  
                                    // update to file config
                                    camConfig.writeConfig(&camInforMap);                                  

                                    if(check_streaming_num > 0){
                                        check_streaming_num--;
                                    }                                
                                    
                                    // check streaming to restart janus
                                    bool restart_janus = true;
                                    CamDevInfor cam_dev_infor[DevInforMap::cam_num_max];
                                    uint8_t cam_num = 0;

                                    cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                    if(cam_num > 0){
                                        for (uint8_t i = 0; i < cam_num; i++){
                                            cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                            uint8_t streaming_num = 0;
                                            std::map<std::string, CamStreamingInfo>::iterator map; 
                                            CamStreamingInfo streamingInfo[CamDevInfor::streaming_num_max];  

                                            for (map = cam_dev_infor[i].listStreaming.begin(); map != cam_dev_infor[i].listStreaming.end(); map++) {
                                                streamingInfo[streaming_num] = map->second;
                                                if((streamingInfo[streaming_num].is_ffmpeg_state == WebeCamDevState::ACTIVE) || (streamingInfo[streaming_num].is_qsv_state == WebeCamDevState::ACTIVE) || (streamingInfo[streaming_num].is_gst_out_state == WebeCamDevState::ACTIVE)){
                                                    restart_janus = false;
                                                    break;
                                                }
                                                streaming_num++;
                                                if(streaming_num > CamDevInfor::streaming_num_max){
                                                    break;
                                                }
                                            }
                                            if(restart_janus == false){
                                                break;
                                            }                                          
                                        }
                                        // if(restart_janus == true){
                                        //     janusManageWorker.restart_janus_flag = WebeCamDevState::ACTIVE;
                                        //     this->reqRestartJanusEngine();
                                        // }
                                    }                                
                                }
                            }
#endif
#ifdef INTEL_NUC_BOX                            
                            Poco::JSON::Parser parse;
                            Poco::Dynamic::Var result = parse.parse(json_data);
                            Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();

                            std::string get_host = object->get("hostMedia");
                            std::string get_port = object->get("portMedia");

                            /// check streaming info
                            CamStreamingInfo streamingInfo;
                            std::string key_streaming = get_host + ":" + get_port;
                            if(cam_infor.listStreaming.find(key_streaming) != cam_infor.listStreaming.end()){
                                //myLogger.sysLogger->info("streaming already exist");
                                streamingInfo = cam_infor.listStreaming[key_streaming];
                                streamingInfo.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                                streamingInfo.is_qsv_state = WebeCamDevState::INACTIVE;
                                streamingInfo.is_gst_out_state = WebeCamDevState::INACTIVE;

                                cam_infor.listStreaming[key_streaming] = streamingInfo;
                                camInforMap.insertDevMap(cam_infor);  
                                // update to file config
                                camConfig.writeConfig(&camInforMap);                                  

                                if(check_streaming_num > 0){
                                    check_streaming_num--;
                                }                                
                                
                                // check streaming to restart janus
                                bool restart_janus = true;
                                CamDevInfor cam_dev_infor[DevInforMap::cam_num_max];
                                uint8_t cam_num = 0;

                                cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                if(cam_num > 0){
                                    for (uint8_t i = 0; i < cam_num; i++){
                                        cam_num = camInforMap.getAllDevMap(cam_dev_infor);
                                        uint8_t streaming_num = 0;
                                        std::map<std::string, CamStreamingInfo>::iterator map; 
                                        CamStreamingInfo streamingInfo[CamDevInfor::streaming_num_max];  

                                        for (map = cam_dev_infor[i].listStreaming.begin(); map != cam_dev_infor[i].listStreaming.end(); map++) {
                                            streamingInfo[streaming_num] = map->second;
                                            if((streamingInfo[streaming_num].is_ffmpeg_state == WebeCamDevState::ACTIVE) || (streamingInfo[streaming_num].is_qsv_state == WebeCamDevState::ACTIVE) || (streamingInfo[streaming_num].is_gst_out_state == WebeCamDevState::ACTIVE)){
                                                restart_janus = false;
                                                break;
                                            }
                                            streaming_num++;
                                            if(streaming_num > CamDevInfor::streaming_num_max){
                                                break;
                                            }
                                        }
                                        if(restart_janus == false){
                                            break;
                                        }                                          
                                    }
                                    // if(restart_janus == true){
                                    //     janusManageWorker.restart_janus_flag = WebeCamDevState::ACTIVE;
                                    //     this->reqRestartJanusEngine();
                                    // }
                                }                                
                            }  
#endif                                                      
                        }
                        catch(const std::exception& e){
                            //myLogger.sysLogger->error("exception when parse json in stopView: {0}", e.what());
                        }
                        // cam_infor.streamingApp.is_ffmpeg_state = WebeCamDevState::INACTIVE;
                        // cam_infor.streamingApp.is_gst_state = WebeCamDevState::INACTIVE;
                        camInforMap.insertDevMap(cam_infor);
                        
                    }
                }
            }

            else if(packet.serv_type == CamServiceWorker::scanCamera){
                //myLogger.sysLogger->info("get request to scan camera");
                // this->respScanCam(&packet);
            }

            else if(packet.serv_type == CamServiceWorker::genCamSerial){
                //myLogger.sysLogger->info("get Add camera service");
                // this->parseAddCam(&packet);
            }

            else if(packet.serv_type == CamServiceWorker::check_stream){
                //myLogger.sysLogger->info("get request to check RTSP camera");
                // this->respCheckStream(&packet);
            }

            else if(packet.serv_type == CamServiceWorker::remove_camera){
                // this->removeCamera(&packet);
            }

            else if(packet.serv_type == CamServiceWorker::req_cam_serv_info){
                this->parseCamServInfo(&packet);
            }

            else if(packet.serv_type == CamServiceWorker::sync_cam){
                this->parseSyncCam(&packet);
            }

            else if(packet.serv_type == CamServiceWorker::reload_service){
                this->parseReloadServ(packet.serial);
            }
            else if(packet.serv_type == CamServiceWorker::restart_janus){
                std::string cmd = "systemctl restart janus.service";
                //system(cmd.c_str());
                //myLogger.sysLogger->info("get ACK to restart Janus Engine");
            }
            else if(packet.serv_type == CamServiceWorker::pan_til_control){
                std::string json_data = "";
                for (uint16_t i = 0; i < packet.body_len; i++){
                    json_data += (char )packet.body[i];
                }
                //myLogger.sysLogger->info("get request to Pan Til camera = {0}, value = {1}", packet.serial, json_data);
                if(camInforMap.checkDevMap(packet.serial)){
                    CamDevInfor camInfo = camInforMap.getDevMap(packet.serial);
                    // cameraControlWorker.sendPTZCmd(camInfo.source, json_data);
                    //onvifServiceWorker.sendPanTilControl(camInfo.dev_serial, camInfo.source, json_data);
                }

            }
        }
        usleep(100000);
    }
    
}


void CamServiceWorker::requestSerial(std::string mac_addr){
    IpcWBCorePacket packet;

    memset(packet.body, 0, sizeof(packet.body));
    //myLogger.sysLogger->info("request serial for camera: MAC = {0}", mac_addr);
    packet.serv_type = CamServiceWorker::request_serial;
    packet.body_len = mac_addr.length() + 2;
    packet.body[0] = (uint8_t)(CamDevInfor::dev_type >> 8);
    packet.body[1] = (uint8_t)CamDevInfor::dev_type;
    for(uint8_t i=0; i<mac_addr.length(); i++){
        packet.body[i+2] = mac_addr.c_str()[i];
    }
    this->ipcWBCore.sendPacket(&packet);
}

void CamServiceWorker::requestAuthen(std::string dev_serial){
    IpcWBCorePacket packet;

    memset(packet.body, 0, sizeof(packet.body));
    packet.serial = dev_serial;
    packet.serv_type = CamServiceWorker::authen_service;
    packet.body_len = 16;
    for(uint8_t i=0; i<16; i++){
        packet.body[i] = 0;
    }
    this->ipcWBCore.sendPacket(&packet);    
}

void CamServiceWorker::ping(CamDevInfor *cam_info){
    IpcWBCorePacket packet;
    try{
        memset(packet.body, 0, sizeof(packet.body));
        ////myLogger.sysLogger->info("Ping for camera: serial = {0}", dev_serial);
        packet.serial = cam_info->dev_serial;
        packet.serv_type = CamServiceWorker::ping_service;
#ifdef WYZE_CAM_V2
        StringBuffer jsonBuffer;
        Writer<StringBuffer> writer(jsonBuffer);
        std::string jsonString;

        writer.StartObject();
        writer.Key("strength");
        writer.Uint(cam_info->strength);
        writer.EndObject();
        jsonString = jsonBuffer.GetString();

        if(jsonString.length() < IpcWBCorePacket::body_max){
            packet.body_len = jsonString.length();
            for(uint8_t i=0; i<packet.body_len; i++){
                packet.body[i] = jsonString.c_str()[i];
            }
        }        
#endif        
#ifdef INTEL_NUC_BOX
        std::stringstream out;
        Poco::JSON::Object object;

        object.set("strength", cam_info->strength);
        object.stringify(out);
        if(out.str().length() < IpcWBCorePacket::body_max){
            packet.body_len = out.str().length();
            for(uint8_t i=0; i<packet.body_len; i++){
                packet.body[i] = out.str().c_str()[i];
            }
        }
#endif        
        this->ipcWBCore.sendPacket(&packet);
    }
    catch(const std::exception& e)
    {
        //myLogger.sysLogger->error("exception when ping service");
    }
}


void CamServiceWorker::reqCamServInfo(std::string dev_serial){
    IpcWBCorePacket packet;

    //myLogger.sysLogger->info("request CamServInfo by camera = {0}", dev_serial);

    packet.serial = dev_serial;
    packet.serv_type = CamServiceWorker::req_cam_serv_info;
    packet.body_len = 4;
    
    for (uint8_t i = 0; i < 3; i++)
    {
        packet.body[i] = 0;
    }
    packet.body[3] = 1;
    
    
    this->ipcWBCore.sendPacket(&packet);
}


void CamServiceWorker::parseCamServInfo(IpcWBCorePacket *packet){
    std::string json_data = "";
    std::string get_serial = packet->serial;
    if(camInforMap.checkDevMap(get_serial) == true){
        CamDevInfor cam_info = camInforMap.getDevMap(get_serial);
        for (uint16_t i = 0; i < packet->body_len; i++){
            json_data += packet->body[i];
        }

        //myLogger.sysLogger->info("get CamServInfor for camera = {0}  - body = {1}",packet->serial, json_data);

        //parse josn data
        try{
#ifdef WYZE_CAM_V2
            Document document;
            bool check_smart_streaming = false;
            document.Parse(json_data.c_str());
            if(document.IsArray()){
                uint8_t array_num = document.Size();
                for(uint8_t i = 0; i < array_num; i++){
                    const Value &object = document[i];
                    if(object.IsObject()){
                        uint16_t serviceID = object["serviceId"].GetUint64();
                        if(serviceID == CamServiceId::storage_serv){
                            std::string url = object["baseStorageUrl"].GetString();
                            std::string url_backup = object["baseStorageBackupUrl"].GetString();
                            std::string auto_backup = object["auto-backup"].GetString();
                            uint16_t fps = object["fps"].GetUint64();
                            uint32_t time_split = object["time-split"].GetUint64();
                            std::string resol = object["resolution"].GetString();
                            std::string type_up = object["type-upload"].GetString();

                            if(object.HasMember("baseStoragePrivateBackupUrl")){
                                std::string url_private = object["baseStoragePrivateBackupUrl"].GetString();
                                cam_info.uploadApp.baseStoragePrivateBackupUrl = url_private;
                            }
                            else{
                                cam_info.uploadApp.baseStoragePrivateBackupUrl = "";
                            }

                            cam_info.storageApp.fps = fps;
                            cam_info.storageApp.time_split = time_split;
                            cam_info.storageApp.resolution = resol;
                            if(time_split > 0){
                                cam_info.storageApp.active_state = WebeCamDevState::ACTIVE;
                            }
                            else{
                                cam_info.storageApp.active_state = WebeCamDevState::INACTIVE;
                            }
                            

                            cam_info.uploadApp.baseStorageUrl = url;
                            cam_info.uploadApp.baseStorageUrlBackup = url_backup;
                            cam_info.uploadApp.auto_backup = auto_backup;                    
                            cam_info.uploadApp.type_upload = type_up;
                            if(url.length() > 0){
                                cam_info.uploadApp.active_status = WebeCamDevState::ACTIVE;
                                cam_info.uploadApp.is_update = true;
                            }
                            else{
                                cam_info.uploadApp.active_status = WebeCamDevState::INACTIVE;
                                cam_info.uploadApp.is_update = false;
                            }
                            camInforMap.insertDevMap(cam_info);
                        }
                        else if(serviceID == CamServiceId::smart_streaming){
                            cam_info.is_smart_streaming = WebeCamDevState::ACTIVE;
                            check_smart_streaming = true;
                            camInforMap.insertDevMap(cam_info);
                        }                        
                    }
                }
                cam_info.is_update_serv = true;
                if(check_smart_streaming == false){
                    cam_info.is_smart_streaming = WebeCamDevState::INACTIVE;
                }
                camInforMap.insertDevMap(cam_info);                
            }
#endif            
#ifdef INTEL_NUC_BOX            
            Poco::JSON::Parser parser;
            Poco::Dynamic::Var result = parser.parse(json_data);
            Poco::JSON::Array::Ptr array = result.extract<Poco::JSON::Array::Ptr>();

            bool check_smart_streaming = false;

            uint8_t num = array->size();
            for (uint8_t i = 0; i < num; i++){
                Poco::JSON::Object::Ptr object = array->getObject(i);
                uint16_t serviceID = object->get("serviceId");
                if(serviceID == CamServiceId::storage_serv){
                    std::string url = object->get("baseStorageUrl");
                    std::string url_backup = object->get("baseStorageBackupUrl");
                    std::string auto_backup = object->get("auto-backup");
                    uint16_t fps = object->get("fps");
                    uint32_t time_split = object->get("time-split");
                    std::string resol = object->get("resolution");
                    std::string type_up = object->get("type-upload");

                    if(object->has("baseStoragePrivateBackupUrl")){
                        std::string url_private = object->get("baseStoragePrivateBackupUrl");
                        cam_info.uploadApp.baseStoragePrivateBackupUrl = url_private;
                    }
                    else{
                        cam_info.uploadApp.baseStoragePrivateBackupUrl = "";
                    }

                    cam_info.storageApp.fps = fps;
                    cam_info.storageApp.time_split = time_split;
                    cam_info.storageApp.resolution = resol;
                    if(time_split > 0){
                        cam_info.storageApp.active_state = WebeCamDevState::ACTIVE;
                    }
                    else{
                        cam_info.storageApp.active_state = WebeCamDevState::INACTIVE;
                    }
                    

                    cam_info.uploadApp.baseStorageUrl = url;
                    cam_info.uploadApp.baseStorageUrlBackup = url_backup;
                    cam_info.uploadApp.auto_backup = auto_backup;                    
                    cam_info.uploadApp.type_upload = type_up;
                    if(url.length() > 0){
                        cam_info.uploadApp.active_status = WebeCamDevState::ACTIVE;
                        cam_info.uploadApp.is_update = true;
                    }
                    else{
                        cam_info.uploadApp.active_status = WebeCamDevState::INACTIVE;
                        cam_info.uploadApp.is_update = false;
                    }
                    camInforMap.insertDevMap(cam_info);
                }
                else if(serviceID == CamServiceId::smart_streaming){
                    cam_info.is_smart_streaming = WebeCamDevState::ACTIVE;
                    check_smart_streaming = true;
                    camInforMap.insertDevMap(cam_info);
                }
            }
            cam_info.is_update_serv = true;
            if(check_smart_streaming == false){
                cam_info.is_smart_streaming = WebeCamDevState::INACTIVE;
            }
            camInforMap.insertDevMap(cam_info);
#endif            
        }
        catch(const std::exception& e){
            //myLogger.sysLogger->error("parseCamServInfo: {0}", e.what());
            cam_info.uploadApp.active_status = WebeCamDevState::INACTIVE;
            cam_info.uploadApp.is_update = false;
            camInforMap.insertDevMap(cam_info);
        }
    }
}


void CamServiceWorker::reqSyncCam(void){
    IpcWBCorePacket packet;
#ifdef WYZE_CAM_V2
    StringBuffer jsonBuffer;
    Writer<StringBuffer> writer(jsonBuffer);
    std::string jsonOut;

    writer.StartObject();
    writer.Key("deviceTypeId");
    writer.Uint64(CamDevInfor::dev_type);
    writer.EndObject();
    jsonOut = jsonBuffer.GetString();

    packet.serial = "000000";
    packet.serv_type = CamServiceWorker::sync_cam;
    packet.body_len = jsonOut.length();
    for (uint8_t i = 0; i < packet.body_len; i++){
        packet.body[i] = jsonOut.c_str()[i];
    }
#endif    
#ifdef INTEL_NUC_BOX    
    Poco::JSON::Object object;
    std::ostringstream out;

    //myLogger.sysLogger->info("request Sync information for camera ");

    packet.serial = "000000";
    packet.serv_type = CamServiceWorker::sync_cam;

    uint16_t dev_type = CamDevInfor::dev_type;
    object.set("deviceTypeId", dev_type);
    object.stringify(out);
    packet.body_len = out.str().length();
    for (uint8_t i = 0; i < packet.body_len; i++){
        packet.body[i] = out.str().c_str()[i];
    }
#endif
    this->ipcWBCore.sendPacket(&packet);    
}

void CamServiceWorker::parseSyncCam(IpcWBCorePacket *packet){
    //myLogger.sysLogger->info("get Sync information for camera");
    std::cout << "CamServiceWorker::parseSyncCam - get Sync information for camera" << std::endl;

    std::string json_data;
    for (uint16_t i = 0; i < packet->body_len; i++){
        json_data += packet->body[i];
    }

    try{
#ifdef WYZE_CAM_V2
        Document document;
        document.Parse(json_data.c_str());
        uint16_t dev_type = document["deviceTypeId"].GetUint64();
        if(dev_type == CamDevInfor::dev_type){
            if(document["devices"].IsArray()){
                const Value &arrayCam = document["devices"].GetArray();
                uint8_t numCam = arrayCam.Size();
                if(numCam == 1){
                    DevInforMap map_cam_config;
                    //camConfig.readConfig(&map_cam_config);
                    const Value &subObject = arrayCam[0];

                    std::string serial = subObject["serial"].GetString();
                    std::string factorySerial = subObject["factorySerial"].GetString();
                    std::string typeCamera = subObject["typeCamera"].GetString();
                    std::string source = subObject["source"].GetString();
                    std::string username = subObject["username"].GetString();
                    std::string password = subObject["password"].GetString();
                    // check and sync to list
                    CamDevInfor cam_info;
                    if(camInforMap.checkDevMap(serial)){
                        cam_info = camInforMap.getDevMap(serial);
                    }
                    cam_info.dev_serial = serial;
                    cam_info.mac_addr = factorySerial;
                    if(source.length() > 0){
                        cam_info.rtsp_url = source;
                        std::size_t found = source.find("//");
                        if((!username.empty()) && (!password.empty())){
                            cam_info.rtsp_url = source.substr(0,found) + "//" + username + ":" + password + "@" + source.substr(found+2);
                        }
                        else{
                            cam_info.rtsp_url = source;
                        }
                    }
                    cam_info.source = source;
                    cam_info.onvifInfor.userName = username;
                    cam_info.onvifInfor.password = password;
                    // cam_info.conn_state = WebeCamDevState::CONNECTED;
                    cam_info.camera_type = typeCamera;
                    
                    cam_info.owner_state = WebeCamDevState::OWNER;

                    // load streaming info from file config
                    if(map_cam_config.checkDevMap(cam_info.dev_serial)){
                        CamDevInfor cam_config;
                        cam_config = map_cam_config.getDevMap(cam_info.dev_serial);
                        cam_info.listStreaming = cam_config.listStreaming;
                    }
                    camInforMap.insertDevMap(cam_info);
                    this->sync_cam_status = WebeCamDevState::CamDevState::SYNC;
                }
            }
        }

#endif        
#ifdef INTEL_NUC_BOX        
        Poco::JSON::Parser parser;
        Poco::Dynamic::Var result = parser.parse(json_data);
        Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();

        uint16_t dev_type = object->get("deviceTypeId");
        if(dev_type == CamDevInfor::dev_type){
            Poco::Dynamic::Var list_cam = object->get("devices");
            Poco::JSON::Array::Ptr array = list_cam.extract<Poco::JSON::Array::Ptr>();

            uint16_t num = array->size();
            DevInforMap map_cam_config;
            if(num > 0){
                camConfig.readConfig(&map_cam_config);
            }
            for (uint16_t i = 0; i < num; i++){
                Poco::JSON::Object::Ptr subObject = array->getObject(i);
                std::string serial = subObject->get("serial");
                std::string factorySerial = subObject->get("factorySerial");
                std::string typeCamera = subObject->get("typeCamera");
                std::string source = subObject->get("source");
                std::string username = subObject->get("username");
                std::string password = subObject->get("password");                

                // check and sync to list
                CamDevInfor cam_info;
                if(camInforMap.checkDevMap(serial)){
                    cam_info = camInforMap.getDevMap(serial);
                }
                cam_info.dev_serial = serial;
                cam_info.mac_addr = factorySerial;
                if(source.length() > 0){
                    cam_info.rtsp_url = source;
                    std::size_t found = source.find("//");
                    if((!username.empty()) && (!password.empty())){
                        cam_info.rtsp_url = source.substr(0,found) + "//" + username + ":" + password + "@" + source.substr(found+2);
                    }
                    else{
                        cam_info.rtsp_url = source;
                    }
                }
                cam_info.source = source;
                cam_info.onvifInfor.userName = username;
                cam_info.onvifInfor.password = password;
                // cam_info.conn_state = WebeCamDevState::CONNECTED;
                cam_info.camera_type = typeCamera;
                
                cam_info.owner_state = WebeCamDevState::OWNER;

                // load streaming info from file config
                if(map_cam_config.checkDevMap(cam_info.dev_serial)){
                    CamDevInfor cam_config;
                    cam_config = map_cam_config.getDevMap(cam_info.dev_serial);
                    cam_info.listStreaming = cam_config.listStreaming;
                }
                camInforMap.insertDevMap(cam_info);
            }
            this->sync_cam_status = WebeCamDevState::CamDevState::SYNC;
            //myLogger.sysLogger->info("Sync camera: success");
        }
        else{
            //myLogger.sysLogger->error("error when parseSyncCam: devType error");
        }
#endif        
    }
    catch(const std::exception& e){
        std::cerr << e.what() << '\n';
        //myLogger.sysLogger->error("exception when parseSyncCam: {0}", e.what());
    }
}


void CamServiceWorker::parseReloadServ(std::string dev_serial){
    if(camInforMap.checkDevMap(dev_serial)){
        CamDevInfor cam_info;
        this->reqCamServInfo(dev_serial);
        cam_info = camInforMap.getDevMap(dev_serial);
        cam_info.is_update_serv = false;
        camInforMap.insertDevMap(cam_info);
    }
}


void CamServiceWorker::reportInfoCam(CamDevInfor *cam_info){
    IpcWBCorePacket packet;
    try{
        memset(packet.body, 0, sizeof(packet.body));
        packet.serial = cam_info->dev_serial;
        packet.serv_type = CamServiceWorker::report_info_camera;
#ifdef WYZE_CAM_V2
        StringBuffer jsonBuffer;
        Writer<StringBuffer> writer(jsonBuffer);
        std::string jsonOut;

        writer.StartObject();
        writer.Key("fps");
        writer.Uint64(cam_info->fps);
        writer.Key("resolution");
        writer.Uint64(cam_info->resol_height);
        writer.Key("typeCamera");
        writer.String(cam_info->camera_type.c_str());

        jsonOut = jsonBuffer.GetString();
        if(jsonOut.length() < IpcWBCorePacket::body_max){
            packet.body_len = jsonOut.length();
            for(uint8_t i=0; i<packet.body_len; i++){
                packet.body[i] = jsonOut.c_str()[i];
            }
        }
#endif        
#ifdef INTEL_NUC_BOX
        std::stringstream out;
        Poco::JSON::Object object;

        object.set("fps", cam_info->fps);
        object.set("resolution", cam_info->resol_height);
        object.set("typeCamera", cam_info->camera_type);
        object.stringify(out);
        if(out.str().length() < IpcWBCorePacket::body_max){
            packet.body_len = out.str().length();
            for(uint8_t i=0; i<packet.body_len; i++){
                packet.body[i] = out.str().c_str()[i];
            }
        }
#endif        
        this->ipcWBCore.sendPacket(&packet);
    }
    catch(const std::exception& e)
    {
        //myLogger.sysLogger->error("exception when ping service");
    }
}