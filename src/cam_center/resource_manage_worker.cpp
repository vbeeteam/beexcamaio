/**
******************************************************************************
* @file           : resource_manage_worker.cpp
* @brief          : Source file of worker for manage resource
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_center/resource_manage_worker.h"
#include "data_struct/cam_data_struct.h"
#include "logger/webe_logger.h"
#include "my_resource/box_resource.h"
#include "my_func/os_func.h"
#include <dirent.h>
#include <string>
#include <unistd.h>

ResourceManageWorker resourceManageWorker;

void ResourceManageWorker::run(void){
    // myLogger.sysLogger->info("Start Worker for manage resource");

    while (1){
        this->manageStorage();
        sleep(1);
    }
    
}

void ResourceManageWorker::manageStorage(void){
    // myLogger.sysLogger->info("Start manage storage memory");

    BeexBoxResource::BoxResource boxResource;

    while (1){
        float disk_available = boxResource.getDiskAvailble(BEEX_PATH);
        float disk_total = boxResource.getDiskTotal(BEEX_PATH);
        // std::cout << "percent = " << std::to_string((((disk_total - disk_available) / disk_total) * 100)) << "disk_available = " << std::to_string(disk_available) << " - disk_total = " << std::to_string(disk_total) << std::endl;
        if(((disk_total - disk_available) / disk_total) * 100 > 80){
            uint32_t time_check = OSFunc::getTimestamp();
            std::string file_delete = "";
            std::string storage_path = BEEX_PATH + std::string("storage/");
            DIR *dir = opendir(storage_path.c_str());
            struct dirent *entry = readdir(dir);
            
            // myLogger.sysLogger->warn("storage memory overload = {0}", std::to_string(((disk_total - disk_available) / disk_total) * 100));
            printf("storage memory overload = %f\n", (((disk_total - disk_available) / disk_total) * 100));
            while (entry != NULL){
                if(entry->d_type == DT_DIR){
                    if(std::string(entry->d_name).length() > 6){
                        //myLogger.sysLogger->info("get folder: {0}", entry->d_name);
                        printf("get folder: %s\n", entry->d_name);
                        std::string backup_path = storage_path + std::string(entry->d_name) + "/backup/";
                        DIR *dir_backup = opendir(backup_path.c_str());
                        struct dirent *entry_backup = readdir(dir_backup);
                        while (entry_backup != NULL){
                            if(std::string(entry_backup->d_name).length() > 6){
                                //myLogger.sysLogger->info("get backup file: {0} = {1}", entry->d_name, entry_backup->d_name);
                                printf("get backup file: %s = %s\n", entry->d_name, entry_backup->d_name);
                                std::string file_name(entry_backup->d_name);
                                std::string spl[10];
                                if(OSFunc::splitString(file_name, "_", spl) > 0){
                                    uint32_t get_time = atoi(spl[1].c_str());
                                    if(get_time < time_check){
                                        time_check = get_time;
                                        file_delete = backup_path + file_name;
                                    }
                                }                                
                            }
                            entry_backup = readdir(dir_backup);
                        }
                        closedir(dir_backup);
                    }
                }
                entry = readdir(dir);
            }
            // myLogger.sysLogger->info("get file to delete = {0}", file_delete);
            remove(file_delete.c_str());
            closedir(dir);
        }
        sleep(2);
    }
}