/**
******************************************************************************
* @file           : cam_conn_worker.cpp
* @brief          : Source file of worker for camera connection
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include <unistd.h>
#include "cam_center/cam_conn_worker.h"
#include "logger/webe_logger.h"
#include <iostream>
#include "socket/tcp_socket.h"
#include "my_func/os_func.h"
#include "ffmpeg_engine/boardCastReader_share.hpp"
#include "ffmpeg_engine/boardCastImplement.hpp"
#include <thread>

/* Object -------------------------------------------------------------------*/
CamConnWorker camConnWorker;

/* Menthod ------------------------------------------------------------------*/

CamConnWorker::CamConnWorker(){
}

CamConnWorker::~CamConnWorker(){
}

void CamConnWorker::run(void){

    std::thread CheckCameraConnThread(&CamConnWorker::hanldeCamConn, this);

    CheckCameraConnThread.join();
    
}


bool CamConnWorker::hanldeCamConn(void){
    // myLogger.sysLogger->info("start to check camera connection thread");
    
    while (1){
        CamDevInfor cam_info[DevInforMap::cam_num_max];
        uint8_t num = camInforMap.getAllDevMap(cam_info);
        if(num > 0){
            for (uint8_t i = 0; i < num; i++){
                usleep(100000);
                if((cam_info[i].owner_state == WebeCamDevState::OWNER) && (cam_info[i].conn_state == WebeCamDevState::DISCONNECTED)){
                    uint32_t timeout = OSFunc::getTimestamp();
                    int result = -1;
                    pthread_t capture_thread_handler;
                    std::thread CheckCapThread(&CamConnWorker::checkCapRtsp, this, &cam_info[i], &timeout, &result);
                    capture_thread_handler = CheckCapThread.native_handle();
                    this->checkCapRtsp(&cam_info[i], &timeout, &result);

                    while (1){
                        if(result == 0){
                            CheckCapThread.join();
                            break;
                        }
                        else if(result == 1){
                            if(camInforMap.checkDevMap(cam_info[i].dev_serial)){
                                CamDevInfor camera = camInforMap.getDevMap(cam_info[i].dev_serial);
                                camera.conn_state = WebeCamDevState::CONNECTED;
                                camInforMap.insertDevMap(camera);
                                std::cout << "Check Connection for camera: connected" << std::endl;
                                // myLogger.sysLogger->info("Check Connection for camera = {0} : connected", cam_info[i].dev_serial);                            
                            }
                            CheckCapThread.join();
                            break;
                        }
                        if(OSFunc::getTimestamp() - timeout > 3){
                            pthread_cancel(capture_thread_handler);
                            break;
                        }
                    }
                    
                }
            }
            
        }
        sleep(5);
    }
    
}

void CamConnWorker::checkCapRtsp(CamDevInfor *camInfo, uint32_t *timeout, int *result){
    boardCastDataProvider::ffmpeg_cap *producer;
    producer = boardCastDataProvider::createFF_cap_rtsp();

    if(camInfo->rtsp_url.empty()){
        *result = 0;
    }
    else{
        if(producer->open(camInfo->rtsp_url.c_str()) == true){
            int ret;
            AVPacket packet;
            memset(&packet, 0, sizeof(packet));
            av_init_packet(&packet); 
            uint32_t check_packet_num = 0;
            uint32_t time_check = OSFunc::getTimestamp();
            while (1){
                *timeout = OSFunc::getTimestamp();
                bool flag_continue = true;
                ret = producer->get_new_packet(packet);
                if(ret < 0){
                    check_packet_num = 0;
                }
                else{
                    check_packet_num++;
                }
                av_packet_unref(&packet);
                if(check_packet_num > 10){
                    *result = 1;
                    break;
                }
                if(OSFunc::getTimestamp() - time_check > 3){
                    *result = 0;
                    break;
                }
            }
            if(producer != NULL){
                delete producer;
                producer = NULL;
            }        
        }
        else{
            *result = 0;
        }
    }
}

bool CamConnWorker::getOnvifInfor(CamDevInfor *cam_infor){

    return true;
}