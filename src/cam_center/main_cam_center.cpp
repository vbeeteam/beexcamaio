/**
******************************************************************************
* @file           : main_cam_center.cpp
* @brief          : source file of main of camera center
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "cam_center/main_cam_center.h"
#include "cam_center/cam_service_worker.h"
#include "cam_center/cam_app_worker.h"
#include "cam_center/resource_manage_worker.h"
#include "logger/webe_logger.h"
#include <thread>
#include "cam_center/cam_onvif_control.h"
#include "cam_center/cam_conn_worker.h"


/* Object -------------------------------------------------------------------*/
MainCamCenter camCenter;
#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1
using namespace BeexCameraCenter;
#endif

/* Method -------------------------------------------------------------------*/

MainCamCenter::MainCamCenter()
{
}

MainCamCenter::~MainCamCenter()
{
}

void MainCamCenter::run(void){
    //myLogger.sysLogger->info("Start Main of Camera Center.....OK!");

    std::thread CamAppWorkerThread(&CamAppWorker::run, &camAppWorker);
    std::thread CamServiceWorkerThread(&CamServiceWorker::run, &camServiceWorker);
#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1
    std::thread CamOnvifWorkerThread(&CameraOnvifControl::run, &cameraOnvifWorker);
#endif
    std::thread CamConnWorkerThread(&CamConnWorker::run, &camConnWorker);
    std::thread ResourceManageWorkerThread(&ResourceManageWorker::run, &resourceManageWorker);


    CamAppWorkerThread.join();
    CamServiceWorkerThread.join();
}