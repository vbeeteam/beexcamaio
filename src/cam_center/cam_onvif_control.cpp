/**
******************************************************************************
* @file           : cam_onvif_control.cpp
* @brief          : Source file of worker for Camera Onvif Control
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#ifdef CAMBOX_ORANGEPIZERO_H2_ORANGEPIUBUNTU_1

#include "cam_center/cam_onvif_control.h"
#include "api_service/api_httpclient.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include <iostream>
#include "data_struct/cam_data_struct.h"
#include "my_func/os_func.h"

using namespace BeexCameraCenter;
using namespace BeexApiService;
using namespace rapidjson;

CameraOnvifControl cameraOnvifWorker;

bool CameraOnvifControl::getListOnvifCamera(std::string *msgResp){
    bool result = false;
    ApiHttpClient httpClient;
    
    std::string api = this->HOST_HTTP_SERVER_ONVIF_MODULE + ":" + this->PORT_HTTP_SERVER_ONVIF_MODULE + "/getListCamera";
    std::string authorHeader = "";
    
    if(httpClient.requestGetByHttp(api, authorHeader, msgResp)){
        result = true;
    }

    return result;
}


bool CameraOnvifControl::updateAuthenOnvifCamera(std::string &ipOnvif, std::string &username, std::string &pass){
    bool result = false;
    ApiHttpClient httpClient;
    
    std::string api = this->HOST_HTTP_SERVER_ONVIF_MODULE + ":" + this->PORT_HTTP_SERVER_ONVIF_MODULE + "/checkAuthenCamera";
    std::string authorHeader = "";
    std::string dataRaw = "";
    StringBuffer stringBuffer;
    Writer<StringBuffer> writer;

    writer.StartObject();
    writer.Key("onvifIp");
    writer.String(ipOnvif.c_str());
    writer.Key("username");
    writer.String(username.c_str());
    writer.Key("password");
    writer.String(pass.c_str());
    writer.EndObject();

    dataRaw = stringBuffer.GetString();
    std::string msgResp;
    
    if(httpClient.requestPostJsonByHttp(api, authorHeader, dataRaw, &msgResp)){
        result = true;
    }

    return result;
}


void CameraOnvifControl::run(void){

    while (1){
        sleep(1);
        std::string jsonData;
        CamDevInfor camInfo[1];
        camInforMap.getAllDevMap(camInfo);
        if(camInfo[0].conn_state == WebeCamDevState::DISCONNECTED){
            if(this->getListOnvifCamera(&jsonData)){
                Document document;
                try{
                    document.Parse(jsonData.c_str());
                    if(document.HasMember("listCamera")){
                        const Value &listCamera = document["listCamera"].GetArray();
                        if(listCamera.IsArray()){
                            const Value &onvifCam = listCamera[0];

                            std::string onvifIp = onvifCam["onvifIp"].GetString();
                            std::string onvifAnthen = onvifCam["onvifAnthen"].GetString();

                            // std::string username = onvifCam["username"].GetString();
                            // std::string password = onvifCam["password"].GetString();
                            std::string sourceRtsp = onvifCam["sourceRtsp"].GetString();
                            
                            CamDevInfor newInfo;
                            newInfo = camInforMap.getDevMap(camInfo[0].dev_serial);
                            newInfo.rtsp_url = sourceRtsp;
                            newInfo.mac_addr = newInfo.rtsp_url + "-" + newInfo.dev_serial;
                            newInfo.onvifInfor.authen_state = onvifAnthen;
                            newInfo.onvifInfor.lastTimeUpdate = OSFunc::getTimestamp();
                            // newInfo.conn_state = WebeCamDevState::CONNECTED;
                            camInforMap.insertDevMap(newInfo);
                        }
                    }
                }
                catch(const std::exception& e)
                {
                    std::cerr << e.what() << '\n';
                }
                
            }
        }
    }
    
}

#endif