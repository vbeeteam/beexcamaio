/**
******************************************************************************
* @file           : cam_app_worker.cpp
* @brief          : Source file of worker for Camera Application
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/* Include -----------------------------------------------------------------*/
#include "cam_center/cam_app_worker.h"
#include "logger/webe_logger.h"
#include "data_struct/cam_data_struct.h"
#include "cam_app/cam_app.h"
#include "ffmpeg_engine/ffmpeg_support.hpp"
#include "config/my_config.h"
#include <unistd.h>
#include <iostream>

/* Object -------------------------------------------------------------------*/
CamAppWorker camAppWorker;

CamAppWorker::CamAppWorker()
{
}

CamAppWorker::~CamAppWorker()
{
}

void CamAppWorker::run(void){
    //myLogger.sysLogger->info("Start Worker for Camera Application.....OK!");
    
    WebeCamApp camApp[DevInforMap::cam_num_max];
    while (1){
        CamDevInfor cam_infor[DevInforMap::cam_num_max];
        uint8_t cam_num = 0;
        cam_num = camInforMap.getAllDevMap(cam_infor);
        if(cam_num > 0){
            for (uint8_t i = 0; i < cam_num; i++){
                if((cam_infor[i].is_running_app == false) && (cam_infor[i].owner_state == WebeCamDevState::OWNER)){
                    for (uint8_t j = 0; j < DevInforMap::cam_num_max; j++){
                        if(camApp[j].is_running == false){
                            camApp[j].is_running = true;
                            std::thread CamAppThread(&WebeCamApp::run, &camApp[j], &cam_infor[i]);
                            CamAppThread.detach();
                            //myLogger.sysLogger->info("debug app thread i = {0}, j = {1}", std::to_string(i), std::to_string(j));
                            //sleep(2);
                            break;
                        }
                    }
                }
            }
        }
        if(camInforMap.change_flag == true){
            camInforMap.change_flag = false;
            myConfig.updateListCam(camInforMap);
        }
        usleep(1000000);
    }
    
}