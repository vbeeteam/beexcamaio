/**
******************************************************************************
* @file           : api_httpclient.cpp
* @brief          : source file of api httpclient
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
#include "api_service/api_httpclient.h"
#include <iostream>
#include <string.h>

#include <curl/curl.h>



using namespace BeexApiService;


size_t ApiHttpClient::WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp){
    size_t realsize = size * nmemb;
    struct HttpClientMemoryStruct *mem = (struct HttpClientMemoryStruct *)userp;

    mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
    if (mem->memory == NULL) {
        /* out of memory! */
        // printf("not enough memory (realloc returned NULL)\n");
        std::cout << "not enough memory (realloc returned NULL)" << std::endl;
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

bool ApiHttpClient::requestGetByHttp(std::string &api, std::string &authorHeader, std::string *msgResp){
    bool result = false;
    try{
        CURL *curl;
        CURLcode res;

        struct HttpClientMemoryStruct chunk;

        chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */
        chunk.size = 0;    /* no data at this point */


        struct curl_httppost *formpost = NULL;
        struct curl_httppost *lastptr = NULL;
        struct curl_slist *headerlist = NULL;
        static const char buf[] = "Expect:";      

        curl_global_init(CURL_GLOBAL_ALL);

        curl = curl_easy_init();

        if (curl) {

            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

            /* send all data to this function  */
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, this->WriteMemoryCallback);

            /* we pass our 'chunk' struct to the callback function */
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

            /* some servers don't like requests that are made without a user-agent
            field, so we provide curl */
            // curl_easy_setopt(curl, CURLOPT_USERAGENT, "my3dsoftware");

            /* initalize custom header list (stating that Expect: 100-continue is not
            wanted */
            std::string httpHeader = "Authorization: " + authorHeader;
            headerlist = curl_slist_append(headerlist, httpHeader.c_str());
            httpHeader = "Content-Type: application/json";
            headerlist = curl_slist_append(headerlist, httpHeader.c_str());

            /* what URL that receives this POST */        
            //curl_easy_setopt(curl, CURLOPT_URL, "http://api.sketchfab.com/v1/models");
            curl_easy_setopt(curl, CURLOPT_URL, api.c_str());
        
            // if ((argc == 2) && (!strcmp(argv[1], "noexpectheader")))
            //     /* only disable 100-continue header if explicitly requested */
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);

            // show all info
            // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

            /* Perform the request, res will get the return code */
            res = curl_easy_perform(curl);
            /* Check for errors */
            if (res != CURLE_OK){
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
            }
            else {
                printf("%lu bytes retrieved\n", (long)chunk.size);
                printf("%s\n", chunk.memory);
                std::string msg(chunk.memory);
                *msgResp = msg;
                result = true;
            }

            /* always cleanup */
            curl_easy_cleanup(curl);

            /* then cleanup the formpost chain */
            curl_formfree(formpost);
            /* free slist */
            curl_slist_free_all(headerlist);
        }
    }
    catch(const std::exception& exception){
        // myLogger.sysLogger->error("Exception occurred while uploading file by Https for file = {0},  url = {1} : {2}", pkt->name_file, url, exception.what());
    }
    return result;
}

bool ApiHttpClient::requestPostJsonByHttp(std::string &api, std::string &authorHeader, std::string &jsonData, std::string *msgResp){
    bool result = false;
    try{
        CURL *curl;
        CURLcode res;

        struct HttpClientMemoryStruct chunk;

        chunk.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */
        chunk.size = 0;    /* no data at this point */


        struct curl_httppost *formpost = NULL;
        struct curl_httppost *lastptr = NULL;
        struct curl_slist *headerlist = NULL;
        static const char buf[] = "Expect:";      

        curl_global_init(CURL_GLOBAL_ALL);

        curl = curl_easy_init();

        if (curl) {

            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

            /* send all data to this function  */
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, this->WriteMemoryCallback);

            /* we pass our 'chunk' struct to the callback function */
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

            /* some servers don't like requests that are made without a user-agent
            field, so we provide curl */
            // curl_easy_setopt(curl, CURLOPT_USERAGENT, "my3dsoftware");

            /* initalize custom header list (stating that Expect: 100-continue is not
            wanted */
            std::string httpHeader = "Authorization: " + authorHeader;
            headerlist = curl_slist_append(headerlist, httpHeader.c_str());
            httpHeader = "Content-Type: application/json";
            headerlist = curl_slist_append(headerlist, httpHeader.c_str());

            /* what URL that receives this POST */        
            //curl_easy_setopt(curl, CURLOPT_URL, "http://api.sketchfab.com/v1/models");
            curl_easy_setopt(curl, CURLOPT_URL, api.c_str());
        
            // if ((argc == 2) && (!strcmp(argv[1], "noexpectheader")))
            //     /* only disable 100-continue header if explicitly requested */
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, jsonData.c_str());

            // show all info
            // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

            /* Perform the request, res will get the return code */
            res = curl_easy_perform(curl);
            /* Check for errors */
            if (res != CURLE_OK){
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
            }
            else {
                printf("%lu bytes retrieved\n", (long)chunk.size);
                printf("%s\n", chunk.memory);
                std::string msg(chunk.memory);
                *msgResp = msg;
                result = true;
            }

            /* always cleanup */
            curl_easy_cleanup(curl);

            /* then cleanup the formpost chain */
            curl_formfree(formpost);
            /* free slist */
            curl_slist_free_all(headerlist);
        }
    }
    catch(const std::exception& exception){
        // myLogger.sysLogger->error("Exception occurred while uploading file by Https for file = {0},  url = {1} : {2}", pkt->name_file, url, exception.what());
    }
    return result;
}