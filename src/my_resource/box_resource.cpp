/**
******************************************************************************
* @file           : box_resource.cpp
* @brief          : source file of resource in box
******************************************************************************
******************************************************************************
*/
/* Include ------------------------------------------------------------------*/
#include "my_resource/box_resource.h"
#include <sys/time.h>
#include <stdio.h>

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include <iomanip>
#include <sys/statvfs.h>
#include <my_func/os_func.h>

using namespace BeexBoxResource;


inline size_t fast_strlen(const char *str)
{
    int i;   // Cannot return 'i' if inside for loop
    for(i = 0; str[i] != '\0'; ++i);

    return i;
}

static inline void mem_init(const char *meminfo[], const char buf[])
{
    const char *var[]  = {
        "MemTotal:",
        "MemFree:",
        "Buffers:",
        "Cached:",
        0
    };

    for	(size_t i = 0; var[i];)
    {
        // TODO: work on making own faster strstr func.
	meminfo[i] = strstr(buf, var[i]) + fast_strlen(var[i]);

        ++i;
    }
}

static inline unsigned long fast_strtoul(const char *c)
{   
    int x = 0;

    // move pointer to first number
    for(; *c < 48 || *c > 57; )
        c++;
   
    // we are on a number, so convert it 
    for(; *c > 47 && *c < 58;)
    {   
        // shifting more efficient than multiplication
        x = (x << 1) + (x << 3) + *c - 48;

        c++;
    }
    
    return x;
}

static inline float mem(int * fd, const char **valp, char * mem_buff)
{
    lseek(*fd, 0, SEEK_SET);
    read(*fd, mem_buff, 256);
    float get_mem = ((((fast_strtoul(*valp) - fast_strtoul(*(valp + 1) ))
          - (fast_strtoul(*(valp + 2)) + fast_strtoul(*(valp + 3)))) / 1024) / 6000.0) * 100.0; 

    return get_mem;

}

BoxLocation::BoxLocation(){
	this->lat = 0;
	this->lon = 0;
}

BoxLocation::~BoxLocation(){
}


BoxResource::BoxResource(){
}

BoxResource::~BoxResource(){
}

uint8_t BoxResource::getCpuUsage(void){
	std::vector<CPUData> entries1;
	std::vector<CPUData> entries2;

    CpuMonitor cpu;

	// snapshot 1
	cpu.ReadStatsCPU(entries1);

	// 100ms pause
	usleep(100000);

	// snapshot 2
	cpu.ReadStatsCPU(entries2);

	// print output
    const CPUData & e1 = entries1[0];
    const CPUData & e2 = entries2[0];    
    const float ACTIVE_TIME	= static_cast<float>(cpu.GetActiveTime(e2) - cpu.GetActiveTime(e1));
    const float IDLE_TIME	= static_cast<float>(cpu.GetIdleTime(e2) - cpu.GetIdleTime(e1));
    const float TOTAL_TIME	= ACTIVE_TIME + IDLE_TIME;    
    uint8_t value = 100.f * ACTIVE_TIME / TOTAL_TIME;
	//std::cout << "get cpu usage: " << std::to_string(value) << "%" << std::endl;
    return value;
}

uint8_t BoxResource::getRamUsage(void){
    int mem_file          = open("/proc/meminfo", 0);
    const char *meminfo[] = {0, 0, 0, 0, 0};
    char mem_buff[256]; // Just take what's needed
    read(mem_file, mem_buff, 256);
    mem_init(meminfo, mem_buff);

	uint8_t get_mem = mem(&mem_file, meminfo, mem_buff);

    close(mem_file);	

	return get_mem;
}


int BoxResource::getCpuTemp(void){
    std::string val;
    std::string preparedTemp;
    float temperature;

    std::ifstream temperatureFile ("/sys/class/thermal/thermal_zone4/temp");

    if (temperatureFile.fail()){
	// cout for testing only. return value of -1 should prompt error
	// message.
		std::ifstream temperatureFile ("/sys/class/thermal/thermal_zone0/temp");
		temperatureFile >> val;
		temperatureFile.close();
		preparedTemp = val.insert(2, 1, '.');
		temperature = std::stod(preparedTemp);
		return (int)(temperature);
    }

    // The temperature is stored in 5 digits.  The first two are degrees
    // in C. The rest are decimal precision.
	temperatureFile >> val;
	temperatureFile.close();
	preparedTemp = val.insert(2, 1, '.');
	temperature = std::stod(preparedTemp);
	return (int)(temperature);
}


uint16_t BoxResource::getDiskAvailble(const char *path){
    DiskMonitor disk;
    //return (disk.getAvailableSpace(path) / 1000000000.0);
#ifdef INTEL_NUC_BOX
	return (disk.getAvailableSpace(path));
#endif
#ifdef WYZE_CAM_V2
    return (disk.getAvailableSpace(path) / 1000);
#endif
}


uint16_t BoxResource::getDiskTotal(const char *path){
    DiskMonitor disk;
    //return (disk.getTotalSpace(path) / 1000000000.0);
#ifdef INTEL_NUC_BOX
	return (disk.getTotalSpace(path));
#endif
#ifdef WYZE_CAM_V2
    return (disk.getTotalSpace(path) / 1000);
#endif
}


bool BoxResource::getNetwork(BoxNetworkInfo *net_info, uint8_t *num){
    FILE *fp = fopen("/proc/net/dev", "r");
    char buf[200], ifname[20];
    unsigned long int r_bytes, t_bytes, r_packets, t_packets;

    // skip first two lines
    for (int i = 0; i < 2; i++) {
        fgets(buf, 200, fp);
    }
    *num = 0;
    uint8_t i = 0;
    while (fgets(buf, 200, fp)) {
        sscanf(buf, "%[^:]: %lu %lu %*lu %*lu %*lu %*lu %*lu %*lu %lu %lu",
               ifname, &r_bytes, &r_packets, &t_bytes, &t_packets);
        //printf("%s: rbytes: %lu rpackets: %lu tbytes: %lu tpackets: %lu\n",
        //       ifname, r_bytes, r_packets, t_bytes, t_packets);
        net_info[i].name = std::string(ifname);
        net_info[i].rx_byte = r_bytes;
        net_info[i].tx_byte = t_bytes;
        (*num)++;
        i++;
    }

    fclose(fp);
}


bool BoxResource::requestHttp(std::string api, std::string *msg){
#ifdef INTEL_NUC_BOX    
    Poco::URI uri(api);
    std::string path(uri.getPathAndQuery());
    if(path.empty()){
        path = "/";
    }
    Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
    Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_GET, path, Poco::Net::HTTPMessage::HTTP_1_1);
    Poco::Net::HTTPResponse response;
    std::ostringstream message;

    session.sendRequest(request);
    std::istream &rsp = session.receiveResponse(response);
    std::stringstream messgae;
    Poco::StreamCopier::copyStream(rsp, message);

    if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
        *msg = message.str();
        return true;
    }
#endif
    return false;

}


bool BoxResource::getMyPublicIP(std::string *ip){
    std::string urt_01 = "http://checkip.amazonaws.com/";
    std::string url_02 = "http://203.205.21.25:8089/checkIp";
    std::string message = "";

    if(this->requestHttp(urt_01, &message) == true){
        *ip = message;
        return true;
    }
    else if(this->requestHttp(url_02, &message) == true){
        *ip = message;
        return true;
    }

    return false;
}


BoxLocation BoxResource::getLocationByIp(void){
    
    std::string my_ip;
	BoxLocation location;

    if(this->getMyPublicIP(&my_ip) == true){
        std::string url = "http://api.ipstack.com/" + my_ip + "?access_key=34a205598735e0b80794384811e421d9";
        std::string message = "";
        if(this->requestHttp(url, &message) == true){
            try
            {
#ifdef INTEL_NUC_BOX                
                Poco::JSON::Parser paser;
                Poco::Dynamic::Var result = paser.parse(message);
                Poco::JSON::Object::Ptr object = result.extract<Poco::JSON::Object::Ptr>();
                location.lat = object->get("latitude");
                location.lon = object->get("longitude");
#endif                
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }
        }
    }
    return location;
}


BoxLocation BoxResource::getLocation(void){
	return this->getLocationByIp();
}


CpuMonitor::CpuMonitor()
{
}

CpuMonitor::~CpuMonitor()
{
}

void CpuMonitor::ReadStatsCPU(std::vector<CPUData> & entries)
{
	std::ifstream fileStat("/proc/stat");

	std::string line;

	const std::string STR_CPU("cpu");
	const std::size_t LEN_STR_CPU = STR_CPU.size();
	const std::string STR_TOT("tot");

	while(std::getline(fileStat, line))
	{
		// cpu stats line found
		if(!line.compare(0, LEN_STR_CPU, STR_CPU))
		{
			std::istringstream ss(line);

			// store entry
			entries.emplace_back(CPUData());
			CPUData & entry = entries.back();

			// read cpu label
			ss >> entry.cpu;

			// remove "cpu" from the label when it's a processor number
			if(entry.cpu.size() > LEN_STR_CPU)
				entry.cpu.erase(0, LEN_STR_CPU);
			// replace "cpu" with "tot" when it's total values
			else
				entry.cpu = STR_TOT;

			// read times
			for(int i = 0; i < NUM_CPU_STATES; ++i)
				ss >> entry.times[i];
		}
	}
}

size_t CpuMonitor::GetIdleTime(const CPUData & e)
{
	return	e.times[S_IDLE] + 
			e.times[S_IOWAIT];
}

size_t CpuMonitor::GetActiveTime(const CPUData & e)
{
	return	e.times[S_USER] +
			e.times[S_NICE] +
			e.times[S_SYSTEM] +
			e.times[S_IRQ] +
			e.times[S_SOFTIRQ] +
			e.times[S_STEAL] +
			e.times[S_GUEST] +
			e.times[S_GUEST_NICE];
}

void CpuMonitor::PrintStats(const std::vector<CPUData> & entries1, const std::vector<CPUData> & entries2)
{
	const size_t NUM_ENTRIES = entries1.size();

	for(size_t i = 0; i < NUM_ENTRIES; ++i)
	{
		const CPUData & e1 = entries1[i];
		const CPUData & e2 = entries2[i];

		std::cout.width(3);
		std::cout << e1.cpu << "] ";

		const float ACTIVE_TIME	= static_cast<float>(GetActiveTime(e2) - GetActiveTime(e1));
		const float IDLE_TIME	= static_cast<float>(GetIdleTime(e2) - GetIdleTime(e1));
		const float TOTAL_TIME	= ACTIVE_TIME + IDLE_TIME;

		std::cout << "active: ";
		std::cout.setf(std::ios::fixed, std::ios::floatfield);
		std::cout.width(6);
		std::cout.precision(2);
		std::cout << (100.f * ACTIVE_TIME / TOTAL_TIME) << "%";

		std::cout << " - idle: ";
		std::cout.setf(std::ios::fixed, std::ios::floatfield);
		std::cout.width(6);
		std::cout.precision(2);
		std::cout << (100.f * IDLE_TIME / TOTAL_TIME) << "%" << std::endl;
	}
}


DiskMonitor::DiskMonitor(){
}

DiskMonitor::~DiskMonitor(){
}

long DiskMonitor::getAvailableSpace(const char* path){
#ifdef INTEL_NUC_BOX
    struct statvfs stat;

    if (statvfs(path, &stat) != 0) {
    // error happens, just quits here
    return -1;
    }

    // the available size is f_bsize * f_bavail
    return (stat.f_bsize * stat.f_bavail);
#endif
#ifdef WYZE_CAM_V2
    std::string cmd = "df " + std::string(path) + " | awk 'NR == 2 { print $4 }'";
    std::string get_out = OSFunc::exeCommand(cmd.c_str());
    if(!get_out.empty()){
        return (std::stol(get_out));
    }
    return 0;
#endif
}

long DiskMonitor::getTotalSpace(const char *path){
#ifdef INTEL_NUC_BOX
    struct statvfs stat;

    if (statvfs(path, &stat) != 0) {
    // error happens, just quits here
    return -1;
    }

    // the available size is f_bsize * f_bavail
    return (stat.f_bsize * stat.f_blocks);  
#endif
#ifdef WYZE_CAM_V2
    std::string cmd = "df " + std::string(path) + " | awk 'NR == 2 { print $2 }'";
    std::string get_out = OSFunc::exeCommand(cmd.c_str());
    if(!get_out.empty()){
        return (std::stol(get_out));
    }
    return 0;
#endif
}