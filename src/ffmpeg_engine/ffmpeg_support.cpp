/*
 * Ffmpeg_support.c
 *
 *  Created on: Mar 13, 2020
 *      Author: kien
 */

#include "ffmpeg_support.hpp"

namespace boardCastDataProvider{
#ifdef _dbg_using_spdlog_

void log_packet(AVRational *time_base, const AVPacket *pkt, const char *tag)
{
    char temp_str[AV_TS_MAX_STRING_SIZE];
    std::string outputStr;
    if(tag != NULL){
        outputStr += std::string(tag) + std::string(": ");
    }

    char *pts_str = av_ts_make_string(temp_str,pkt->pts);
    outputStr += std::string("pts:") + std::string(pts_str) + std::string(" ");
    pts_str = av_ts_make_time_string(temp_str,pkt->pts,time_base);
    outputStr += std::string("pts_time:") + std::string(pts_str) + std::string(" ");

    char *dts_str = av_ts_make_string(temp_str,pkt->dts);
    outputStr += std::string("dts:") + std::string(dts_str) + std::string(" ");
    dts_str = av_ts_make_time_string(temp_str,pkt->dts,time_base);
    outputStr += std::string("dts_time:") + std::string(dts_str) + std::string(" ");

    char *duration_str = av_ts_make_string(temp_str,pkt->duration);
    outputStr += std::string("duration:") + std::string(duration_str) + std::string(" ");
    duration_str = av_ts_make_time_string(temp_str,pkt->duration,time_base);
    outputStr += std::string("duration_time:") + std::string(duration_str) + std::string(" ");

    outputStr += std::string("stream_index:") + std::to_string(pkt->stream_index);
    std::cout << outputStr << std::endl;

}

void showFError(int ret){
    char err_str[AV_ERROR_MAX_STRING_SIZE];
    if (ret < 0 && ret != AVERROR_EOF) {
        fprintf(stderr, "Error occurred: %s\n", av_make_error_string(err_str,AV_ERROR_MAX_STRING_SIZE,ret));
    }
}

bool getFError(int ret, std::string &outErrStr)
{
    if(ret < 0 && ret != AVERROR_EOF){
        char err_str[AV_ERROR_MAX_STRING_SIZE];
        av_make_error_string(err_str,AV_ERROR_MAX_STRING_SIZE,ret);
        outErrStr = std::string(err_str);
        return true;
    }
    return false;
}
#endif
}
