#include<iostream>
extern "C"{
#include <libavutil/opt.h>
#include <libavdevice/avdevice.h>

}
#include "boardcast_ffmpeg_io.h"
// #include "boardCastReader_FFmpeg_qsv.h"

using namespace std;

namespace boardCastDataProvider{

float ffmpeg_cap::get_fps(){
    int video_stream = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (video_stream < 0) {
        fprintf(stderr, "Cannot find a video stream in the input file\n");
        return -1;
    }
    AVStream *video = ifmt_ctx->streams[video_stream];
    AVRational r_frame_rate = video->r_frame_rate;
    if(r_frame_rate.den > 0){
        float test_fps = float(r_frame_rate.num)/r_frame_rate.den;
        if(max_fps > 0 && test_fps > max_fps){
            test_fps = max_fps;
        }
        return test_fps;
    }
    return -1;
}

AVRational ffmpeg_cap::getAVFps(){
    int video_stream = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (video_stream < 0) {
        fprintf(stderr, "Cannot find a video stream in the input file\n");
        return AVRational{0,0};
    }
    AVStream *video = ifmt_ctx->streams[video_stream];
    AVRational r_frame_rate = video->r_frame_rate;
    if(r_frame_rate.den > 0){
        float test_fps = float(r_frame_rate.num)/r_frame_rate.den;
        if(max_fps > 0 && test_fps > max_fps){
            r_frame_rate.num = max_fps * r_frame_rate.den;
        }

    }else{
        if(max_fps > 0){
            r_frame_rate.num = max_fps * r_frame_rate.den;
        }
    }
    return r_frame_rate;
}

bool ffmpeg_cap::open(const char* input){
    bool init_rs = false;
    int ret;
    refresh();
#ifdef using_ffmpeg_3_4_1
    initComponents();
#endif
#ifdef _test_GTIMEOUT_
    ret = av_dict_set(&d_t, "stimeout", "10000000", 0);
    if(ret < 0){
        printf("Set timeout fails!\n");
        return init_rs;
    }
    if ((ret = avformat_open_input(&ifmt_ctx, input, 0, &d_t)) < 0)
#else
    if ((ret = avformat_open_input(&ifmt_ctx, input, 0, NULL)) < 0)
#endif
    {
        fprintf(stderr, "Could not open input file '%s'\n", input);
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Could not open input file '{}'",__FUNCTION__,__LINE__,input);
#endif
        return init_rs;
    }

    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        fprintf(stderr, "Failed to retrieve input stream information");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Failed to retrieve input stream information",__FUNCTION__,__LINE__);
#endif
        return init_rs;
    }
#ifdef _dbg_ffmpeg_t_
//    cout << "Name: " << ifmt_ctx->iformat->name;
//    cout << " | Long Name: " << ifmt_ctx->iformat->long_name;
//    cout << " | Mime Type: " << ifmt_ctx->iformat->mime_type << endl;
    av_dump_format(ifmt_ctx, 0, input, 0);
#endif
    mSt = MSTREAM_FILE;
    return true;
}

int ffmpeg_cap::get_new_packet(AVPacket &pkt){
#ifdef _test_GTIMEOUT_
    int ret = av_dict_set(&d_t, "stimeout", "100", 0);
    if(ret < 0){
        printf("Set timeout fails!\n");
        return ret;
    }
#endif
    int ret = av_read_frame(ifmt_ctx, &pkt);
//    printf("pkt pts " "%" PRId64 ,pkt.pts);
//    printf(" --- pkt dts " "%" PRId64 "\n",pkt.dts);
    return ret;
}

bool ffmpeg_cap_rtsp::open(const char* input){
    bool init_rs = false;
    int ret;
    refresh();
    AVDictionary *d = NULL;
    //Using tcp to connect rtsp
    ret = av_dict_set(&d, "rtsp_transport", "tcp", 0);
    if(ret < 0){
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Try to setting rtsp camera to tcp fails!",__FUNCTION__,__LINE__);
#endif
        return init_rs;
    }
    //set socket timeout in micro-secords
//    ret = av_dict_set_int(&d, "stimeout", (int64_t)5000000, 0);
#if USE_AV_INTERRUPT_CALLBACK
    interrupt_metadata.timeout_after_ms = LIBAVFORMAT_INTERRUPT_OPEN_TIMEOUT_MS * 10;
    get_monotonic_time(&interrupt_metadata.value);
    ifmt_ctx = avformat_alloc_context();
    ifmt_ctx->interrupt_callback.callback = _opencv_ffmpeg_interrupt_callback;
    ifmt_ctx->interrupt_callback.opaque = (void*)(&interrupt_metadata);
#endif
    if ((ret = avformat_open_input(&ifmt_ctx, input, 0, &d)) < 0)
    {
        fprintf(stderr, "Could not open input file '%s'\n", input);
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Could not open input file '{}'",__FUNCTION__,__LINE__,input);
#endif
        return init_rs;
    }
    if(d != NULL){
#ifdef _dbg_ffmpeg_t_
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
#endif
        av_dict_free(&d);
    }

    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        fprintf(stderr, "Failed to retrieve input stream information");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Failed to retrieve input stream information",__FUNCTION__,__LINE__);
#endif
        return init_rs;
    }
#ifdef _dbg_ffmpeg_t_
    cout << "Name: " << ifmt_ctx->iformat->name;
    cout << " | Long Name: " << ifmt_ctx->iformat->long_name;
    cout << " | Mime Type: " << ifmt_ctx->iformat->mime_type << endl;
#endif
    mSt = MSTREAM_RTSP;
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ifmt_ctx, 0, input, 0);
#endif
    return true;
}

ffmpeg_cap* createFF_cap_rtsp(){
    return new ffmpeg_cap_rtsp;
}

bool ffmpeg_cap_v1::open(const char* input){
    if(ffmpeg_cap_rtsp::open(input)){
        int ret = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
        if (ret < 0) {
            fprintf(stderr, "Cannot find a video stream in the input file\n");
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->error("{}:{}: Cannot find a video stream in the input file",__FUNCTION__,__LINE__);
#endif
            return false;
        }
        vid_s = ret;
        float r_fps = -1;
        AVRational r_frame_rate = ifmt_ctx->streams[vid_s]->r_frame_rate;
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->info("{}:{}: Frame rate {}:{}",__FUNCTION__,__LINE__,r_frame_rate.num,r_frame_rate.den);
#endif
        if(r_frame_rate.den > 0){
            r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        }
        float r_time_b = -1;
        AVRational t_timeBase = ifmt_ctx->streams[vid_s]->time_base;
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->info("{}:{}: Time base {}:{}",__FUNCTION__,__LINE__,t_timeBase.num,t_timeBase.den);
#endif
        if(t_timeBase.num > 0){
            r_time_b = float(t_timeBase.den)/t_timeBase.num;
        }
        if(r_time_b > 0 && r_fps > 0){
            if(r_fps > max_fps){
                r_fps = max_fps;
            }
            diff_dts = int64_t(r_time_b/r_fps);
        }

#ifdef _dbg_cap_au_pf_
        printf("Initial difference dts " "%" PRId64 "\n",diff_dts);
#endif
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->info("{}:{}: Initial difference dts {}",__FUNCTION__,__LINE__,diff_dts);
#endif
        return true;
    }
    return false;
}

bool ffmpeg_cap_v2::open(const char* input){
    bool init_rs = false;
    int ret;
    refresh();
#ifdef using_ffmpeg_3_4_1
    avdevice_register_all();
    initComponents();
#else
    avdevice_register_all();
#endif
    AVInputFormat *inputFormat = av_find_input_format("v4l2");
    if ((ret = avformat_open_input(&ifmt_ctx, input, inputFormat, NULL)) < 0)
    {
        fprintf(stderr, "Could not open input file '%s'\n", input);
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Could not open input file '{}'",__FUNCTION__,__LINE__,input);
#endif
        return init_rs;
    }

    if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
        fprintf(stderr, "Failed to retrieve input stream information");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Failed to retrieve input stream information",__FUNCTION__,__LINE__);
#endif
        return init_rs;
    }
#ifdef _dbg_ffmpeg_t_
//    cout << "Name: " << ifmt_ctx->iformat->name;
//    cout << " | Long Name: " << ifmt_ctx->iformat->long_name;
//    cout << " | Mime Type: " << ifmt_ctx->iformat->mime_type << endl;
    av_dump_format(ifmt_ctx, 0, input, 0);
#endif
    mSt = MSTREAM_WCAM;
    return true;
}

ffmpeg_cap* createFF_cap_v2(){
    return new ffmpeg_cap_v2;
}

#ifdef _dbg_cap_au_pf_
int ffmpeg_cap_v1::get_new_packet(AVPacket &pkt,int &nPadFrame){
    nPadFrame = 0;
    int ret = av_read_frame(ifmt_ctx, &pkt);
    if(ret == 0){
        if( vid_s == pkt.stream_index )
        {
            if(flag_t == 0){
                int testN = rand() % 100 + 1;
                if(testN % 23 == 0 && frame_c > 20){
                    cout << "Test lost " << testN << endl;
                    flag_t = testN;
                }
                int64_t temp_dts = prev_dts;
                if(temp_dts != AV_NOPTS_VALUE){
                    temp_dts = pkt.dts - temp_dts;
                    assert(diff_dts != AV_NOPTS_VALUE);
                    if(diff_dts != AV_NOPTS_VALUE){
                        double r_dts = double(temp_dts)/double(diff_dts);
                        if(r_dts >= 2.0){
                            nPadFrame = rint(r_dts) - 1;
                            printf("pkt r_dts %d\n",nPadFrame);
                        }
//			else if(temp_dts > 0){
//                            diff_dts = (diff_dts + temp_dts)/2;
//                        }
                    }
//                    else if(temp_dts > 0){
//                        diff_dts = temp_dts;
//                    }
                }
                prev_dts = pkt.dts;
            }else{
                ret = flag_t;
                flag_t -= 1;
            }
            frame_c += 1;
        }
    }
    return ret;
}
#endif

int ffmpeg_cap_v1::get_new_packet(AVPacket &pkt){
#if USE_AV_INTERRUPT_CALLBACK
    interrupt_metadata.timeout_after_ms = LIBAVFORMAT_INTERRUPT_READ_TIMEOUT_MS;
    get_monotonic_time(&interrupt_metadata.value);
#endif
    int ret = av_read_frame(ifmt_ctx, &pkt);
    if(ret == 0){
        if( vid_s == pkt.stream_index )
        {
            frame_c += 1;
            //            For estimate number frame lost
            int64_t temp_dts = prev_dts;
            if(temp_dts != AV_NOPTS_VALUE){
                temp_dts = pkt.dts - temp_dts;
                if(diff_dts != AV_NOPTS_VALUE){
                    double r_dts = double(temp_dts)/double(diff_dts);
                    if(r_dts >= 2.0){
                        r_dts = rint(r_dts) - 1;
//                        if(r_dts < std::numeric_limits<int>::max())
                        if(r_dts < max_fps)
                        {
                            ret = r_dts;
                            diff_dts = (diff_dts + temp_dts)/2;
                        }
#ifdef _dbg_using_spdlog_
                        dbg_spdlogger.sysLogger->warn("{}:{}Lost Input packet detected! Estimate {} frame lost!",__FUNCTION__,__FILE__,r_dts);
#endif
                    }
#ifdef _testAu_Est_dts_
                    else if(temp_dts > 0){
                        diff_dts = (diff_dts + temp_dts)/2;
                    }
#endif
                }
#ifdef _testAu_Est_dts_
                else if(temp_dts > 0){
                    diff_dts = temp_dts;
                }
#endif
            }
            prev_dts = pkt.dts;
        }
    }
    return ret;
}

ffmpeg_cap* createFF_cap_v1(){
    return new ffmpeg_cap_v1;
}

void ffmpeg_out_ipl::refresh(){
    pPkt.pts = AV_NOPTS_VALUE;
    pPkt.dts = AV_NOPTS_VALUE;
    //frame_counter = 0;
    in_tBase.clear();
    if(ofmt_ctx != NULL && ofmt != NULL){
        if(!(ofmt->flags & AVFMT_NOFILE)){
            avio_closep(&ofmt_ctx->pb);
        }
    }
    ofmt = NULL;
    avformat_free_context(ofmt_ctx);
    ofmt_ctx = NULL;
    av_freep(&stream_mapping);
}

bool ffmpeg_out_ipl::open(const char* output,ffmpeg_cap &cap,int flag){
    int ret;
    unsigned int i;
    int stream_index = 0;
    refresh();

    if(flag == 1){
        avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtp", output);
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->info("{}:{}: Output type is rtp!",__FUNCTION__,__LINE__);
#endif
    }else if(flag == 2){
        avformat_alloc_output_context2(&ofmt_ctx, NULL, "mpegts", output);
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->info("{}:{}: Output type is tcp!",__FUNCTION__,__LINE__);
#endif
    }
    else{
        avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    }

    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Could not create output context",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }

    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

    stream_mapping_size = ifmt_ctx->nb_streams;
    stream_mapping = (int*)av_mallocz_array(stream_mapping_size, sizeof(*stream_mapping));
    if (!stream_mapping) {
        fprintf(stderr, "Out of memory\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Out of memory",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR(ENOMEM);
        return false;
    }

    ofmt = ofmt_ctx->oformat;


#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    int f_noAudio = 0;
    if(strncmp(ofmt->name,"mp4",3) == 0){
        f_noAudio = 1;
    }else if(strncmp(ofmt->name,"rtp",3) == 0){
        f_noAudio = 2;
    }

    for (i = 0; i < ifmt_ctx->nb_streams; i++) {
        AVStream *out_stream;
        AVStream *in_stream = ifmt_ctx->streams[i];
        AVCodecParameters *in_codecpar = in_stream->codecpar;

        in_tBase.push_back(in_stream->time_base);

        if ( (in_codecpar->codec_type != AVMEDIA_TYPE_AUDIO || f_noAudio != 0) &&
                in_codecpar->codec_type != AVMEDIA_TYPE_VIDEO &&
                in_codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE) {
            stream_mapping[i] = -1;
            continue;
        }


        stream_mapping[i] = stream_index++;

        out_stream = avformat_new_stream(ofmt_ctx, NULL);
        if (!out_stream) {
            fprintf(stderr, "Failed allocating output stream\n");
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->error("{}:{}: Failed allocating output stream",__FUNCTION__,__LINE__);
#endif
            ret = AVERROR_UNKNOWN;
            return false;
        }

        ret = avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
        if (ret < 0) {
            fprintf(stderr, "Failed to copy codec parameters\n");
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->error("{}:{}: Failed to copy codec parameters",__FUNCTION__,__LINE__);
#endif
            return false;
        }

        out_stream->codecpar->codec_tag = 0;
        //ofmt_ctx->oformat->flags |= AVFMT_NOTIMESTAMPS;
        if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
            out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    }

    av_dump_format(ofmt_ctx, 0, output, 1);
    if (!(ofmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'", output);
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->error("{}:{}: Could not open output file '{}'",__FUNCTION__,__LINE__,output);
#endif
            return false;
        }
    }
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Error occurred when opening output file",__FUNCTION__,__LINE__);
#endif
        return false;
    }
    return true;
}

bool ffmpeg_out_ipl::open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag){
    int ret;
    unsigned int i;
    int stream_index = 0;
    refresh();

    if(flag == 1){
        avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtp", output);
    }else{
        avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    }

    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Could not create output context",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }

    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

    stream_mapping_size = ifmt_ctx->nb_streams;
    stream_mapping = (int*)av_mallocz_array(stream_mapping_size, sizeof(*stream_mapping));
    if (!stream_mapping) {
        fprintf(stderr, "Out of memory\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Out of memory",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR(ENOMEM);
        return false;
    }

    ofmt = ofmt_ctx->oformat;


#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    int f_noAudio = 0;
    if(strncmp(ofmt->name,"mp4",3) == 0){
        f_noAudio = 1;
    }else if(strncmp(ofmt->name,"rtp",3) == 0){
        f_noAudio = 2;
    }

    for (i = 0; i < ifmt_ctx->nb_streams; i++) {
        AVStream *out_stream;
        AVStream *in_stream = ifmt_ctx->streams[i];
        AVCodecParameters *in_codecpar = in_stream->codecpar;



        if ( (in_codecpar->codec_type != AVMEDIA_TYPE_AUDIO || f_noAudio != 0) &&
                in_codecpar->codec_type != AVMEDIA_TYPE_VIDEO &&
                in_codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE) {
            stream_mapping[i] = -1;
            in_tBase.push_back(in_stream->time_base);
            continue;
        }


        stream_mapping[i] = stream_index++;

        out_stream = avformat_new_stream(ofmt_ctx, NULL);
        if (!out_stream) {
            fprintf(stderr, "Failed allocating output stream\n");
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->error("{}:{}: Failed allocating output stream",__FUNCTION__,__LINE__);
#endif
            ret = AVERROR_UNKNOWN;
            return false;
        }
        if(in_codecpar->codec_type == AVMEDIA_TYPE_VIDEO){
            in_tBase.push_back(tsc->encoder_ctx->time_base);
            avcodec_parameters_from_context(out_stream->codecpar, tsc->encoder_ctx);
            out_stream->time_base =  tsc->encoder_ctx->time_base;
            //ofmt_ctx->oformat->flags |= AVFMT_NOTIMESTAMPS;
            if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
                out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        }else{
            in_tBase.push_back(in_stream->time_base);
            ret = avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
            if (ret < 0) {
                fprintf(stderr, "Failed to copy codec parameters\n");
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy codec parameters {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy codec parameters",__FUNCTION__,__LINE__);
                }
#endif
                return false;
            }
        }



        out_stream->codecpar->codec_tag = 0;


    }

    av_dump_format(ofmt_ctx, 0, output, 1);
    if (!(ofmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'", output);
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}: Error {}",__FUNCTION__,__LINE__,output,outErrStr);
            }
            else{
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}",__FUNCTION__,__LINE__,output);
            }
#endif
            return false;
        }
    }
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file: Error {}",__FUNCTION__,__LINE__,outErrStr);
        }
        else{
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file",__FUNCTION__,__LINE__);
        }
#endif
        return false;
    }
    return true;
}

bool ffmpeg_out_ipl::write_data_old(int ret,AVPacket &in_pkt,bool f_continue){
    AVStream *out_stream;//*in_stream,
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }else{
        if(f_continue == false){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
            return true;
        }
        AVPacket *pkt = av_packet_clone(&in_pkt);
        if (pkt->stream_index >= stream_mapping_size ||
                stream_mapping[pkt->stream_index] < 0) {
            av_packet_unref(pkt);
        }
        else{
            AVRational in_Base = in_tBase[pkt->stream_index];
            pkt->stream_index = stream_mapping[pkt->stream_index];
            out_stream = ofmt_ctx->streams[pkt->stream_index];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = 0;
                    pkt->dts = 0;
                }
                else{
                    pkt->pts -= pPkt.pts;
                    pkt->dts -= pPkt.dts;
                }
            }
//				else{
//					printf("first dts %ld\n",out_stream->first_dts);
//					printf("Cur dts %ld\n",out_stream->cur_dts);
//					printf("Last IP pts %ld\n",out_stream->last_IP_pts);
//					printf("Last_IP_duration %d\n",out_stream->last_IP_duration);
//				}

            //pkt->pts = pkt->dts = frame_counter;

            if (pkt->pts != (int64_t)AV_NOPTS_VALUE)
                pkt->pts = av_rescale_q_rnd(pkt->pts, in_Base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            if (pkt->dts != (int64_t)AV_NOPTS_VALUE)
                pkt->dts = av_rescale_q_rnd(pkt->dts, in_Base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            if (pkt->duration)
                pkt->duration = av_rescale_q(pkt->duration, in_Base, out_stream->time_base);
            //av_packet_rescale_ts(pkt, in_Base, out_stream->time_base);

            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            //log_packet(ofmt_ctx, &pkt, "out");

            log_packet(ofmt_ctx, pkt, "out");
#endif
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
#ifdef _dbg_ffmpeg_t_
            if (ret < 0) {

                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


            }
#endif
            av_packet_unref(pkt);
            //frame_counter += 1;
        }
        return true;
    }
}

bool ffmpeg_out_ipl::write_data(int ret,AVPacket &in_pkt,bool f_continue){
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }else{
        if(f_continue == false){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
            return true;
        }
        if (in_pkt.stream_index < stream_mapping_size &&
                stream_mapping[in_pkt.stream_index] >= 0) {
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
                printf("Try to alloc temp data fails!\n");
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
                printf("Try make packet from data fails!\n");
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try make packet from data fails!",__FUNCTION__,__LINE__);
#endif
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            AVRational in_Base = in_tBase[in_pkt.stream_index];
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = stream_mapping[in_pkt.stream_index];
            out_stream = ofmt_ctx->streams[in_pkt.stream_index];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = 0;
                    pkt->dts = 0;
                }
                else{
                    pkt->pts -= pPkt.pts;
                    pkt->dts -= pPkt.dts;
                }
            }
//				else{
//					printf("first dts %ld\n",out_stream->first_dts);
//					printf("Cur dts %ld\n",out_stream->cur_dts);
//					printf("Last IP pts %ld\n",out_stream->last_IP_pts);
//					printf("Last_IP_duration %d\n",out_stream->last_IP_duration);
//				}

            //pkt->pts = pkt->dts = frame_counter;

            if (pkt->pts != (int64_t)AV_NOPTS_VALUE)
                pkt->pts = av_rescale_q_rnd(pkt->pts, in_Base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            if (pkt->dts != (int64_t)AV_NOPTS_VALUE)
                pkt->dts = av_rescale_q_rnd(pkt->dts, in_Base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
            if (pkt->duration)
                pkt->duration = av_rescale_q(pkt->duration, in_Base, out_stream->time_base);
            //av_packet_rescale_ts(pkt, in_Base, out_stream->time_base);

            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            //log_packet(ofmt_ctx, &pkt, "out");

            log_packet(ofmt_ctx, pkt, "out");
#endif
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
            if (ret < 0) {
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Write frame error!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");
#endif
            }
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
        }
        return true;
    }
}

ffmpeg_out* createFF_out(){
    return new ffmpeg_out_ipl;
}

bool ffmpeg_out_rtpS::open(const char* output,ffmpeg_cap &cap,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtsp", output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Could not create output context!",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (vid < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Cannot find a video stream in the input file!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Cannot find a video stream in the input file. ");
        showFError(ret);
#else
        fprintf(stderr, "Cannot find a video stream in the input file.\n");
#endif
        return false;
    }
    AVStream *in_stream = ifmt_ctx->streams[vid];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed allocating output stream!",__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase.clear();
    in_tBase.push_back(in_stream->time_base);
#ifdef _dbg_ffmpeg_t_
    printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
#endif
    ret = avcodec_parameters_copy(out_stream->codecpar,in_codecpar);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters!",__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}:{}",__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}",__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }

    diff_ts = 0;
    AVRational r_frame_rate = in_stream->r_frame_rate;
#ifdef _dbg_cap_au_pf_
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
#endif
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        diff_ts = double(in_tBase[0].den)/double(in_tBase[0].num * r_fps);
#ifdef _dbg_cap_au_pf_
        printf("Output difference ts %g\n",diff_ts);
#endif
    }
#ifdef _dbg_using_spdlog_
    dbg_spdlogger.sysLogger->info("{}:{}:{}: Open output file {} with estimate fps {}:{} --- "
                                  "time base {}:{}",__FILE__,__FUNCTION__,__LINE__,output
                                  ,r_frame_rate.num,r_frame_rate.den,in_tBase[0].num,in_tBase[0].den);
#endif

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "rtsp_transport", "tcp", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using rtsp_transport fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif

    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file!",__FUNCTION__,__LINE__);
#endif
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }
    return true;
}

bool ffmpeg_out_rtpS::open(const char* output,ffmpeg_M2out *encode_out,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtsp", output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not create output context!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    vid = 0;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, (AVCodec*)encode_out->getEncodec());
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed allocating output stream!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    AVCodecContext *encoder_ctx = (AVCodecContext *)encode_out->getCodecContext();
    in_tBase = vector<AVRational>(1,encoder_ctx->time_base);
    ret = avcodec_parameters_from_context(out_stream->codecpar, encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}! {}",__FILE__,__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}!",__FILE__,__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }
    diff_ts = 0;
    AVRational r_frame_rate = encoder_ctx->framerate;
#ifdef _dbg_cap_au_pf_
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
#endif
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        diff_ts = double(in_tBase[0].den)/double(in_tBase[0].num * r_fps);
#ifdef _dbg_cap_au_pf_
        printf("Output difference ts %g\n",diff_ts);
#endif
    }
#ifdef _dbg_using_spdlog_
    dbg_spdlogger.sysLogger->info("{}:{}:{}: Open output file {} with estimate fps {}:{} --- "
                                  "time base {}:{}",__FILE__,__FUNCTION__,__LINE__,output
                                  ,r_frame_rate.num,r_frame_rate.den,in_tBase[0].num,in_tBase[0].den);
#endif
    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "rtsp_transport", "tcp", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using rtsp_transport fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif


    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->warn("{}:{}: Set key {} : value {} fails!",__FUNCTION__,__LINE__,t->key,t->value);
#endif
        }
        av_dict_free(&d);
    }
    return true;
}

void ffmpeg_out_ipl1::refresh(){
    internal_signal = MOUTFILE_WAIT_KEY_FRAME;
    pPkt.pts = AV_NOPTS_VALUE;
    pPkt.dts = AV_NOPTS_VALUE;
    if(ofmt_ctx != NULL && ofmt != NULL){
        if(!(ofmt->flags & AVFMT_NOFILE)){
            avio_closep(&ofmt_ctx->pb);
        }
    }
    ofmt = NULL;
    avformat_free_context(ofmt_ctx);
    ofmt_ctx = NULL;
}

bool ffmpeg_out_ipl1::open(const char* output,ffmpeg_cap &cap,int correct_fps)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Could not create output context!",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (vid < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Cannot find a video stream in the input file!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Cannot find a video stream in the input file. ");
        showFError(ret);
#else
        fprintf(stderr, "Cannot find a video stream in the input file.\n");
#endif
        return false;
    }
    AVStream *in_stream = ifmt_ctx->streams[vid];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed allocating output stream!",__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase.clear();
    in_tBase.push_back(in_stream->time_base);
#ifdef _dbg_ffmpeg_t_
    printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
#endif
    ret = avcodec_parameters_copy(out_stream->codecpar,in_codecpar);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters!",__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}:{}",__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}",__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }
    if(correct_fps > _BC_MAX_FPS_){
        correct_fps = -1;
    }
    diff_ts = 0;
    AVRational r_frame_rate = in_stream->r_frame_rate;
#ifdef _dbg_ffmpeg_t_
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
#endif
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        if(correct_fps > 0){
            r_fps = correct_fps;
        }
        diff_ts = double(in_tBase[0].den)/double(in_tBase[0].num * r_fps);
#ifdef _dbg_ffmpeg_t_
        printf("Output difference ts %g"\n",diff_ts);
#endif
    }
#ifdef _dbg_using_spdlog_
    dbg_spdlogger.sysLogger->info("{}:{}:Open output file with estimate fps {}:{} --- "
                                  "time base {}:{} --- Step ts {}",__FUNCTION__,__LINE__
                                  ,r_frame_rate.num,r_frame_rate.den,in_tBase[0].num,in_tBase[0].den,diff_ts);
#endif

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif

    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file!",__FUNCTION__,__LINE__);
#endif
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }
    return true;
}

bool ffmpeg_out_ipl1::open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Could not create output context!",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = 0;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, tsc->enc_codec);
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed allocating output stream!",__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase = vector<AVRational>(1,tsc->encoder_ctx->time_base);
    //FIXME: Replace this will more abstract value
//    in_tBase = vector<AVRational>(1,av_make_q(1,90000));
    //		printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_from_context(out_stream->codecpar, tsc->encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        tsc->encoder_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}! {}",__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}!",__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }
    diff_ts = 0;
    AVRational r_frame_rate = tsc->encoder_ctx->framerate;
#ifdef _dbg_cap_au_pf_
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
#endif
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        diff_ts = double(in_tBase[0].den)/double(in_tBase[0].num * r_fps);
#ifdef _dbg_cap_au_pf_
        printf("Output difference ts %g\n",diff_ts);
#endif
    }
#ifdef _dbg_using_spdlog_
    dbg_spdlogger.sysLogger->info("{}:{}:Open output file with estimate fps {}:{} --- "
                                  "time base {}:{} --- Step ts {}",__FUNCTION__,__LINE__
                                  ,r_frame_rate.num,r_frame_rate.den,in_tBase[0].num,in_tBase[0].den,diff_ts);
#endif
    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif


    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file!",__FUNCTION__,__LINE__);
        }
#endif
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }
    return true;
}

bool ffmpeg_out_ipl1::write_data_good(int ret,AVPacket &in_pkt,my_output_signal out_signal){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(out_signal == MOUTFILE_END){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }else{
        if(out_signal == MOUTFILE_END){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
            return true;
        }else{
            if(out_signal == MOUTFILE_INTERUPT){
                internal_signal = MOUTFILE_WAIT_KEY_FRAME;
            }
            if(in_pkt.stream_index != vid){
                return true;
            }
            if(internal_signal == MOUTFILE_WAIT_KEY_FRAME){
                if(in_pkt.flags & AV_PKT_FLAG_KEY){
                    internal_signal = MOUTFILE_NORMAL;
                }else{
                    return true;
                }
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Try to alloc temp data fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Try make packet from data fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = 0;
                    pkt->dts = 0;
                }
                else{
                    pkt->pts -= pPkt.pts;
                    pkt->dts -= pPkt.dts;
                }

            }
            av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            log_packet(ofmt_ctx, pkt, "out");
#endif
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
            if (ret < 0) {
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Write frame fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_


                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


#endif
            }
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }

    }
}

bool ffmpeg_out_ipl1::write_data(int ret,AVPacket &in_pkt,bool f_continue){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }else{
        if(f_continue == false){
            //            av_write_frame(ofmt_ctx, NULL);
            av_interleaved_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try make packet from data fails!",__FUNCTION__,__LINE__);
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
                    pPkt.pts = 0;
                    pPkt.dts = 0;
                    pkt->pts = 0;
                    pkt->dts = 0;
                }
                else{
                    pPkt.pts += diff_ts;
                    pPkt.dts += diff_ts;
                    pkt->pts = pPkt.pts;
                    pkt->dts = pPkt.dts;
                }

            }
            av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            log_packet(ofmt_ctx, pkt, "out");
#endif
//            ret = av_write_frame(ofmt_ctx,pkt);
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
            if (ret < 0) {
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Write frame fails!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_


                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


#endif
            }

            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }

    }
}

ffmpeg_out* createFF_out_ts(){
    return new ffmpeg_out_ipl1;
}

bool ffmpeg_out_rtpT::initBSFC(AVCodecContext *in_ctx,const char* bsf_name){
    const AVBitStreamFilter *bsf = av_bsf_get_by_name(bsf_name);
    if (!bsf) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:av_bsf_get_by_name fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
        fprintf(stderr,"av_bsf_get_by_name() failed\n");
        return false;
    }
    int ret = av_bsf_alloc(bsf, &bsfc);
    if(ret < 0){
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed to alloc bit stream filter!",__FILE__,__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr,"Failed to alloc bit stream filter.");
        showFError(ret);
#else
        fprintf(stderr,"Failed to alloc bit stream filter.\n");
#endif
        return false;
    }
    ret = avcodec_parameters_from_context(bsfc->par_in, in_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }
    ret = av_bsf_init(bsfc);
    if(ret < 0){
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed to initial the bit stream filter!",__FILE__,__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to initial the bit stream filter.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to initial the bit stream filter.\n");
#endif
        return false;
    }
    return true;
}

bool ffmpeg_out_rtpM::write_data(int ret,AVPacket &in_pkt,bool f_continue){

    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }
    if(f_continue == false){
        av_interleaved_write_frame(ofmt_ctx, NULL);
        return true;
    }
    AVStream *out_stream;//*in_stream,
    AVPacket *pkt = av_packet_alloc();
    uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
    if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
        printf("Try to alloc temp data fails!\n");
        av_packet_free(&pkt);
        return false;
    }
    memcpy(data_temp,in_pkt.data,in_pkt.size);
    ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
    if(ret != 0){
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:av_packet_from_data fails! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:av_packet_from_data fails!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
        printf("Try make packet from data fails!\n");
        av_freep(&data_temp);
        av_packet_free(&pkt);
        return false;
    }
    pkt->dts = in_pkt.dts;
    pkt->pts = in_pkt.pts;
    pkt->duration = in_pkt.duration;
    pkt->flags = in_pkt.flags;
    pkt->stream_index = 0;

    ret = av_bsf_send_packet(bsfc, pkt);
    if(ret < 0){
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred av_bsf_send_packet! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred av_bsf_send_packet!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        av_packet_unref(pkt);
        av_packet_free(&pkt);
        return false;
    }
    ret = av_bsf_receive_packet(bsfc, pkt);
    while(ret == 0){
        pkt->stream_index = 0;
        out_stream = ofmt_ctx->streams[0];
        pkt_counter += 1;
        pkt->pts = pkt_counter;
        pkt->dts = pkt_counter;
        pkt->duration = 0;
        av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
        printf("pkt pts " "%" PRId64 ,pkt->pts);
        printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
        printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
        pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
        log_packet(ofmt_ctx, pkt, "out");
#endif
        ret = av_interleaved_write_frame(ofmt_ctx, pkt);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred av_interleaved_write_frame! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred av_interleaved_write_frame!",__FILE__,__FUNCTION__,__LINE__);
            }
#endif
#ifdef _dbg_ffmpeg_t_


            showFError(ret);
            fprintf(stderr, "Error muxing packet\n");
#endif
        }

        av_packet_unref(pkt);
        ret = av_bsf_receive_packet(bsfc, pkt);
    }
    av_packet_unref(pkt);
    av_packet_free(&pkt);
    if(ret != AVERROR(EAGAIN)){
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when writing output file! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when writing output file!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        return false;
    }
    return true;
}

bool ffmpeg_out_rtpM::open(const char* output,ffmpeg_M2out *encode_out,int flag){
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtp", output);
    if (!ofmt_ctx) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Could not create output context!",__FILE__,__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    AVCodecContext *encoder_ctx = (AVCodecContext *)encode_out->getCodecContext();
    if(initBSFC(encoder_ctx) == false){
        return false;
    };
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx,(AVCodec *)encode_out->getEncodec());
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed allocating output stream!",__FILE__,__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase = vector<AVRational>(1,encoder_ctx->time_base);
    //		printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_from_context(out_stream->codecpar, encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (!(ofmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Could not open output file '{}'!",__FILE__,__FUNCTION__,__LINE__,output);
#endif
            fprintf(stderr, "Could not open output file '%s'", output);
            return false;
        }
    }

//    av_dump_format(ofmt_ctx, 0, output, 1);
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when opening output file!",__FILE__,__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    return true;
}

ffmpeg_out* createFF_out_rtpM(){
    return new ffmpeg_out_rtpM;
}

ffmpeg_out* createFF_out_rtpTCP(){
    return new ffmpeg_out_rtpS;
}

//bool open(const char* output,ffmpeg_M2out *encode_out,int flag);
bool ffmpeg_out_ipl2::open(const char* output,ffmpeg_M2out *encode_out,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not create output context",__LINE__,__FUNCTION__,__FILE__);
#endif
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;


#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = 0;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, (AVCodec*)encode_out->getEncodec());
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed allocating output stream!",__FILE__,__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    AVCodecContext *encoder_ctx = (AVCodecContext *)encode_out->getCodecContext();
    in_tBase = vector<AVRational>(1,encoder_ctx->time_base);
//            printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_from_context(out_stream->codecpar, encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        encoder_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Could not open output file {}:{}",__FILE__,__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Could not open output file {}",__FILE__,__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }

    diff_ts = 0;
    AVRational r_frame_rate = encoder_ctx->framerate;
#ifdef _dbg_cap_au_pf_
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
#endif
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        diff_ts = double(in_tBase[0].den)/double(in_tBase[0].num * r_fps);
#ifdef _dbg_cap_au_pf_
        printf("Output difference ts %g\n",diff_ts);
#endif
    }
#ifdef _dbg_using_spdlog_
    dbg_spdlogger.sysLogger->info("{}:{}:{}:Open output file with estimate fps {}:{} --- "
                                  "time base {}:{} --- Step ts {}",__FILE__,__FUNCTION__,__LINE__
                                  ,r_frame_rate.num,r_frame_rate.den,in_tBase[0].num,in_tBase[0].den,diff_ts);
#endif

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif

    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when opening output file!",__FILE__,__FUNCTION__,__LINE__);
#endif
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }
    return true;
}

ffmpeg_out* createFF_out_pp(){
    return new ffmpeg_out_ipl2;
}

bool ffmpeg_out_rtpT::open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag){
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtp", output);
    if (!ofmt_ctx) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Could not create output context!",__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    if(initBSFC(tsc->encoder_ctx) == false){
        return false;
    };
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, tsc->enc_codec);
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed allocating output stream!",__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase = vector<AVRational>(1,tsc->encoder_ctx->time_base);
    //		printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_from_context(out_stream->codecpar, tsc->encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (!(ofmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}! {}",__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}!",__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'", output);
            return false;
        }
    }

    av_dump_format(ofmt_ctx, 0, output, 1);
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file!",__FUNCTION__,__LINE__);
        }
#endif
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    return true;
}

bool ffmpeg_out_rtpT::write_data_old(int ret,AVPacket &in_pkt,bool f_continue){

    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }
    if(f_continue == false){
        av_write_trailer(ofmt_ctx);
        return true;
    }
    AVStream *out_stream;//*in_stream,
    AVPacket *pkt = av_packet_clone(&in_pkt);

    ret = av_bsf_send_packet(bsfc, pkt);
    if(ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        av_packet_unref(pkt);
        return false;
    }
    ret = av_bsf_receive_packet(bsfc, pkt);
    while(ret == 0){
        pkt->stream_index = 0;
        out_stream = ofmt_ctx->streams[0];
        if(pkt->pts != AV_NOPTS_VALUE){
            if(pPkt.pts == AV_NOPTS_VALUE){
                pPkt.pts = pkt->pts;
                pPkt.dts = pkt->dts;
                pkt->pts = 0;
                pkt->dts = 0;
            }
            else{
                pkt->pts -= pPkt.pts;
                pkt->dts -= pPkt.dts;
            }
        }
        av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
        printf("pkt pts " "%" PRId64 ,pkt->pts);
        printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
        printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
        pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
        log_packet(ofmt_ctx, pkt, "out");
#endif
        ret = av_interleaved_write_frame(ofmt_ctx, pkt);
#ifdef _dbg_ffmpeg_t_
        if (ret < 0) {

            showFError(ret);
            fprintf(stderr, "Error muxing packet\n");


        }
#endif
        av_packet_unref(pkt);
        ret = av_bsf_receive_packet(bsfc, pkt);
    }
    if(ret != AVERROR(EAGAIN)){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        return false;
    }
    return true;
}

bool ffmpeg_out_rtpT::write_data(int ret,AVPacket &in_pkt,bool f_continue){

    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            av_write_trailer(ofmt_ctx);
        }
        return false;
    }
    if(f_continue == false){
        av_write_trailer(ofmt_ctx);
        return true;
    }
    AVStream *out_stream;//*in_stream,
    AVPacket *pkt = av_packet_clone(&in_pkt);

    ret = av_bsf_send_packet(bsfc, pkt);
    if(ret < 0){
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred av_bsf_send_packet! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred av_bsf_send_packet!",__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        av_packet_unref(pkt);
        return false;
    }
    ret = av_bsf_receive_packet(bsfc, pkt);
    while(ret == 0){
        pkt->stream_index = 0;
        out_stream = ofmt_ctx->streams[0];
        if(pkt->pts != AV_NOPTS_VALUE){
            if(pPkt.pts == AV_NOPTS_VALUE){
                pPkt.pts = pkt->pts;
                pPkt.dts = pkt->dts;
                pkt->pts = 0;
                pkt->dts = 0;
            }
            else{
                pkt->pts -= pPkt.pts;
                pkt->dts -= pPkt.dts;
            }
        }
        av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
        printf("pkt pts " "%" PRId64 ,pkt->pts);
        printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
        printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
        pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
        log_packet(ofmt_ctx, pkt, "out");
#endif
        ret = av_interleaved_write_frame(ofmt_ctx, pkt);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Error occurred av_interleaved_write_frame! {}",__FUNCTION__,__LINE__,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:Error occurred av_interleaved_write_frame!",__FUNCTION__,__LINE__);
            }
#endif
#ifdef _dbg_ffmpeg_t_


            showFError(ret);
            fprintf(stderr, "Error muxing packet\n");
#endif
        }

        av_packet_unref(pkt);
        ret = av_bsf_receive_packet(bsfc, pkt);
    }
    if(ret != AVERROR(EAGAIN)){
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when writing output file! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when writing output file!",__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        return false;
    }
    return true;
}

ffmpeg_out* createFF_out_rtpT(){
    return new ffmpeg_out_rtpT;
}

bool ffmpeg_out_ipl3::write_data(int ret,AVPacket &in_pkt,bool f_continue){
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            handleLastWrite();
        }
        return false;
    }else{
        if(!f_continue){
            handleLastWrite();
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails!",__FUNCTION__,__LINE__);
                }
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = 0;
                    pkt->dts = 0;
                }
                else{
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
                    pkt->pts = pkt->pts - pPkt.pts;
                    pkt->dts = pkt->dts - pPkt.dts;
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
//                    if(dbg_c % 100 == 0){
//                        printf("td_pts " "%" PRId64 ,td_pts);
//                        printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                    }
                }

            }
            else{
                printf("Video packet AV_NOPTS_VALUE %d size\n",pkt->size);
            }
//            if(dPkt.dts != AV_NOPTS_VALUE){
//                if(dbg_c % 100 == 0){
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
//                    printf("td_pts " "%" PRId64 ,td_pts);
//                    printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                }
//            }
            dPkt.dts = pkt->dts;
            dPkt.pts = pkt->pts;
            if(pkt->flags == AV_PKT_FLAG_KEY){
                if(data_last_end != NULL){
                    av_freep(&data_last_end);
                }
                dPkt.size = pkt->size;
                data_last_end = (uint8_t*)av_malloc(pkt->size);
                memcpy(data_last_end,pkt->data,dPkt.size);
            }
            av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            log_packet(ofmt_ctx, pkt, "out");
#endif
//            ret = av_write_frame(ofmt_ctx,pkt);
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
            if (ret < 0) {
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails!",__FUNCTION__,__LINE__);
                }
#endif
#ifdef _dbg_ffmpeg_t_

                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");



#endif
            }else{
                frame_counter += 1;
            }
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }
    }
}

void ffmpeg_out_ipl3::handleLastWrite(){
    if(dPkt.dts != AV_NOPTS_VALUE && dPkt.size > 0 && diff_ts > 0){
        int ret;
        double stop_ts_thres = 0;
        if(in_tBase[0].num > 0){
            stop_ts_thres = double(in_tBase[0].den * time_to_end * 60)/in_tBase[0].num;
        }
        int maxNumberFrame = time_to_end * 60 * _BC_MAX_FPS_;
        AVStream *out_stream = ofmt_ctx->streams[0];
        double t_pts = dPkt.pts;
        double t_dts = dPkt.dts;
        if(t_pts < stop_ts_thres){
#ifdef _dbg_using_spdlog_
            double dbg_diff_v = diff_ts;
#endif
            diff_ts = stop_ts_thres/frame_counter;
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->info("{}:{}:{}: Update pts/dts: Record {} --- Calculator {}"
                                          " --- dts_old {} --- dts_new {}",__FUNCTION__,__FILE__,
                                          __LINE__,pPkt.dts,stop_ts_thres,dbg_diff_v,diff_ts);
#endif
        }
#ifdef _dbg_cap_au_pf_
        int dbg_last_pad_frame = 0;
        printf("Last dts " "%" PRId64 ,dPkt.dts);
        printf(" --- Stop ts thresh " "%" PRId64 "\n",stop_ts_thres);
#endif
//        getchar();
        while(t_pts < stop_ts_thres){
            AVPacket pkt;
            memset(&pkt,0,sizeof(pkt));
            av_init_packet(&pkt);
            uint8_t *data_temp = (uint8_t*)av_malloc(dPkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                break;
            }
            //                memcpy(data_temp,dPkt.data,dPkt.size);
            memcpy(data_temp,data_last_end,dPkt.size);
            ret = av_packet_from_data(&pkt,data_temp,dPkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails!",__FUNCTION__,__LINE__);
                }
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                break;
            }
            t_dts += diff_ts;
            t_pts += diff_ts;
            pkt.pts = t_pts;
            pkt.dts = t_dts;
            pkt.stream_index = 0;
            av_packet_rescale_ts(&pkt,in_tBase[0],out_stream->time_base);
            ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
            if(ret < 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails!",__FUNCTION__,__LINE__);
                }
#endif
#ifdef _dbg_cap_au_pf_

                printf("Write interleaved error!\n");

#endif
            }else{
                frame_counter += 1;
                if(frame_counter >= maxNumberFrame){
                    break;
                }
            }
            av_packet_unref(&pkt);
#ifdef _dbg_cap_au_pf_
            dbg_last_pad_frame += 1;
#endif
        }
#ifdef _dbg_cap_au_pf_
        cout << "Number last pad frame " << dbg_last_pad_frame << endl;
#endif
        av_packet_unref(&dPkt);
        dPkt.pts = AV_NOPTS_VALUE;
        dPkt.dts = AV_NOPTS_VALUE;
        //            av_interleaved_write_frame(ofmt_ctx, NULL);
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
    }else{
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
    }
    refresh();
}

bool ffmpeg_out_ipl4::write_data(int ret,AVPacket &in_pkt,bool f_continue){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            handleLastWrite();
        }
        return false;
    }else{
        if(!f_continue){
            handleLastWrite();
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            if(pPkt.pts == AV_NOPTS_VALUE){
                if(in_pkt.flags != AV_PKT_FLAG_KEY){
                    printf("Warning start frame of file should be key frame!\n");
                    return true;
                }
            }
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails!",__FUNCTION__,__LINE__);
                }
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            if(pPkt.pts == AV_NOPTS_VALUE){
                r_ts = diff_ts;
                pPkt.pts = diff_ts;
                pPkt.dts = diff_ts;
            }
            else{
                r_ts += diff_ts;
                pPkt.pts = r_ts;
                pPkt.dts = r_ts;
            }
            frame_counter += 1;
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
            buffAV.push_back(pkt);
            return true;
        }
    }
}

void ffmpeg_out_ipl4::handleLastWrite(){
    if(pPkt.dts != AV_NOPTS_VALUE){
        int ret;
        double stop_ts_thres = 0;
        if(in_tBase[0].num > 0){
            stop_ts_thres = double(in_tBase[0].den * time_to_end * 60)/in_tBase[0].num;
        }
        AVStream *out_stream = ofmt_ctx->streams[0];
#ifdef _dbg_ffmpeg_t_
        printf("Last dts " "%" PRId64 ,pPkt.dts);
        printf(" --- Stop ts thresh " "%" PRId64 "\n",stop_ts_thres);
#endif
        if(r_ts != stop_ts_thres){
#ifdef _dbg_using_spdlog_
            double dbg_diff_v = diff_ts;
#endif
            diff_ts = stop_ts_thres/frame_counter;
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->info("{}:{} Update pts/dts: Record {} --- Calculator {}"
                                          " --- dts_old {} --- dts_new {}",__FUNCTION__,__FILE__
                                          ,r_ts,stop_ts_thres,dbg_diff_v,diff_ts);
#endif
        }
#ifdef _dbg_ffmpeg_t_
        printf("Diff_ts %g\n",diff_ts);
//        getchar();
#endif

        double temp_ts = -diff_ts;

        for(auto it : buffAV){
            temp_ts += diff_ts;
            it->pts = temp_ts;
            it->dts = temp_ts;
            av_packet_rescale_ts(it,in_tBase[0],out_stream->time_base);
            ret = av_interleaved_write_frame(ofmt_ctx, it);
            if(ret < 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails!",__FUNCTION__,__LINE__);
                }
#endif
#ifdef _dbg_ffmpeg_t_
                printf("Write interleaved error!\n");
#endif
            }
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
        pPkt.pts = AV_NOPTS_VALUE;
        pPkt.dts = AV_NOPTS_VALUE;
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
        refresh();
    }
}

bool ffmpeg_out_ipl5::write_data_good(int ret,AVPacket &in_pkt,my_output_signal out_signal){
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(out_signal == MOUTFILE_END){
            handleLastWrite();
        }
        return false;
    }else{
        if(out_signal == MOUTFILE_END){
            handleLastWrite();
            return true;
        }else{
            if(out_signal == MOUTFILE_INTERUPT){
                internal_signal = MOUTFILE_WAIT_KEY_FRAME;
            }
            if(in_pkt.stream_index != vid){
                return true;
            }
            if(internal_signal == MOUTFILE_WAIT_KEY_FRAME){
                if(in_pkt.flags & AV_PKT_FLAG_KEY){
                    internal_signal = MOUTFILE_NORMAL;
                }else{
                    return true;
                }
            }
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:Try to alloc temp data fails!",__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails! {}",__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:av_packet_from_data fails!",__FUNCTION__,__LINE__);
                }
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            frame_counter += 1;
            pkt->dts = frame_counter;
            pkt->pts = frame_counter;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            pkt->pos = -1;
            buffAV.push_back(pkt);
            return true;
        }
    }
}

void ffmpeg_out_ipl5::handleLastWrite(){
    if(frame_counter > 0){
        int ret;
        AVRational correctTBase;
        correctTBase.den = frame_counter;
        correctTBase.num = time_to_end * 60;
        AVStream *out_stream = ofmt_ctx->streams[0];
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->info("{}:{}:Correct time base {}:{}",__FUNCTION__,__LINE__
                                      ,correctTBase.num,correctTBase.den);
#endif
        for(auto it : buffAV){
            av_packet_rescale_ts(it,correctTBase,out_stream->time_base);
            ret = av_interleaved_write_frame(ofmt_ctx, it);
            if(ret < 0){
#ifdef _dbg_using_spdlog_
                std::string outErrStr;
                if(getFError(ret,outErrStr)){
                    dbg_spdlogger.sysLogger->error("{}:{}:{}: av_interleaved_write_frame fails! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
                }else{
                    dbg_spdlogger.sysLogger->error("{}:{}:{}: av_interleaved_write_frame fails!",__FILE__,__FUNCTION__,__LINE__);
                }
#endif
#ifdef _dbg_ffmpeg_t_
                printf("Write interleaved error!\n");
#endif
            }
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
        refresh();
        if(!fileWorking.empty() && !fileTempName.empty()){
#ifdef _dbg_using_spdlog_
            if(!boardCastDataProvider::mvFile(fileTempName.c_str(),fileWorking.c_str())){
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Copy file {} to {} fails!",__FILE__,__FUNCTION__,__LINE__,fileTempName,fileWorking);
            }
#else
            boardCastDataProvider::mvFile(fileTempName.c_str(),fileWorking.c_str());
#endif
        }
    }
}

bool ffmpeg_out_ipl5::open(const char* output,const char* realFinalFile,ffmpeg_cap &cap,ffmpeg_transcode *tsc)
{
    if(realFinalFile != NULL && open(output,cap,tsc) == true)
    {
        fileTempName = std::string(output);
        fileWorking = std::string(realFinalFile);
        return true;
    }
    return false;
}

bool ffmpeg_out_ipl5::open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not create output context!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    vid = 0;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, tsc->enc_codec);
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed allocating output stream!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    timeBase = tsc->encoder_ctx->time_base;
    ret = avcodec_parameters_from_context(out_stream->codecpar, tsc->encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}! {}",__FILE__,__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}!",__FILE__,__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }

#ifdef _dbg_using_spdlog_
    AVRational r_frame_rate = tsc->encoder_ctx->framerate;
    dbg_spdlogger.sysLogger->info("{}:{}:{}: Open output file {} with estimate fps {}:{} --- "
                                  "time base {}:{}",__FILE__,__FUNCTION__,__LINE__,output
                                  ,r_frame_rate.num,r_frame_rate.den,timeBase.num,timeBase.den);
#endif
    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif


    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->warn("{}:{}: Set key {} : value {} fails!",__FUNCTION__,__LINE__,t->key,t->value);
#endif
        }
        av_dict_free(&d);
    }
    return true;
}

bool ffmpeg_out_ipl6::open(const char* output,ffmpeg_M2out *encode_out,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not create output context!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    vid = 0;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, (AVCodec*)encode_out->getEncodec());
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed allocating output stream!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    AVCodecContext *encoder_ctx = (AVCodecContext *)encode_out->getCodecContext();
    timeBase = encoder_ctx->time_base;
    ret = avcodec_parameters_from_context(out_stream->codecpar, encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}! {}",__FILE__,__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}!",__FILE__,__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }

#ifdef _dbg_using_spdlog_
    AVRational r_frame_rate = encoder_ctx->framerate;
    dbg_spdlogger.sysLogger->info("{}:{}:{}: Open output file {} with estimate fps {}:{} --- "
                                  "time base {}:{}",__FILE__,__FUNCTION__,__LINE__,output
                                  ,r_frame_rate.num,r_frame_rate.den,timeBase.num,timeBase.den);
#endif
    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif


    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->warn("{}:{}: Set key {} : value {} fails!",__FUNCTION__,__LINE__,t->key,t->value);
#endif
        }
        av_dict_free(&d);
    }
    return true;
}

void ffmpeg_out_ipl6::refresh(){
    frame_counter = 0;
    vid = 0;
    internal_signal = MOUTFILE_WAIT_KEY_FRAME;
    if(ofmt_ctx != NULL){
        if(ofmt != NULL){
            if(!(ofmt->flags & AVFMT_NOFILE)){
                avio_closep(&ofmt_ctx->pb);
            }
        }
        avformat_free_context(ofmt_ctx);
    }
    ofmt = NULL;
    ofmt_ctx = NULL;
}

void ffmpeg_out_ipl6::close(){
    av_write_frame(ofmt_ctx, NULL);
    av_write_trailer(ofmt_ctx);
    if(ofmt_ctx != NULL){
        if(ofmt != NULL){
            if(!(ofmt->flags & AVFMT_NOFILE)){
                avio_closep(&ofmt_ctx->pb);
            }
        }
        avformat_free_context(ofmt_ctx);
    }
    ofmt = NULL;
    ofmt_ctx = NULL;
}

bool ffmpeg_out_ipl6::writeToRealFile(){
    bool f_rs = false;
    if(frame_counter <= 0){
        return f_rs;
    }
    AVRational correctTBase;
    correctTBase.den = frame_counter;
    correctTBase.num = time_to_end * 60;
#ifdef _dbg_using_spdlog_
    dbg_spdlogger.sysLogger->info("{}:{}:Correct time base {}:{}",__FUNCTION__,__LINE__
                                  ,correctTBase.num,correctTBase.den);
#endif
    close();
    ffmpeg_cap temp_n;
    if(temp_n.open(fileTempName.c_str())){
        if(open(fileWorking.c_str(),temp_n)){
            f_rs = true;
            AVStream *out_stream = ofmt_ctx->streams[0];
            AVPacket basePkt;
            memset(&basePkt,0,sizeof(basePkt));
            av_init_packet(&basePkt);
            int ret = temp_n.get_new_packet(basePkt);
            frame_counter = 1;
            while(ret >= 0){
                basePkt.pts = frame_counter;
                basePkt.dts = frame_counter;
                basePkt.duration = 0;
                basePkt.stream_index = 0;
                basePkt.pos = -1;
                av_packet_rescale_ts(&basePkt,correctTBase,out_stream->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &basePkt);
                if(ret < 0){
#ifdef _dbg_using_spdlog_
                    std::string outErrStr;
                    if(getFError(ret,outErrStr)){
                        dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails! {}",__FUNCTION__,__LINE__,outErrStr);
                    }else{
                        dbg_spdlogger.sysLogger->error("{}:{}:av_interleaved_write_frame fails!",__FUNCTION__,__LINE__);
                    }
#endif
#ifdef _dbg_ffmpeg_t_
                    printf("Write interleaved error!\n");
#endif
                }
                av_packet_unref(&basePkt);
                ret = temp_n.get_new_packet(basePkt);
                frame_counter += 1;
            }
            av_packet_unref(&basePkt);
            av_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);

        }
#ifdef _dbg_using_spdlog_
        else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Writter open the {} fails!",__FILE__,__FUNCTION__,__LINE__,fileWorking);
        }
#endif
    }
#ifdef _dbg_using_spdlog_
    else{
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Capture open the {} fails!",__FILE__,__FUNCTION__,__LINE__,fileTempName);
    }
#endif
    return f_rs;
}

bool ffmpeg_out_ipl6::write_data_good(int ret,AVPacket &in_pkt,my_output_signal out_signal){
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(out_signal == MOUTFILE_END){
            writeToRealFile();
        }
        return false;
    }else{
        if(out_signal == MOUTFILE_END){
            return writeToRealFile();
        }else{
            if(out_signal == MOUTFILE_INTERUPT){
                internal_signal = MOUTFILE_WAIT_KEY_FRAME;
            }
            if(in_pkt.stream_index != vid){
                return true;
            }
            if(internal_signal == MOUTFILE_WAIT_KEY_FRAME){
                if(in_pkt.flags & AV_PKT_FLAG_KEY){
                    internal_signal = MOUTFILE_NORMAL;
                }else{
                    return true;
                }
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Try to alloc temp data fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Try make packet from data fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = frame_counter;
            pkt->pts = frame_counter;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            av_packet_rescale_ts(pkt, timeBase,out_stream->time_base);
            pkt->pos = -1;
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
            if (ret < 0) {
#ifdef _dbg_using_spdlog_
                dbg_spdlogger.sysLogger->error("{}:{}:{}:Write frame fails!",__FILE__,__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_


                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


#endif
            }
            frame_counter += 1;
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }

    }
}

bool ffmpeg_out_ipl6::open(const char* output,ffmpeg_cap &cap,int flag)
{
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not create output context!",__FILE__,__FUNCTION__,__LINE__);
#endif
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (vid < 0) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}: Cannot find a video stream in the input file!",__FUNCTION__,__LINE__);
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Cannot find a video stream in the input file. ");
        showFError(ret);
#else
        fprintf(stderr, "Cannot find a video stream in the input file.\n");
#endif
        return false;
    }
    AVStream *in_stream = ifmt_ctx->streams[vid];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed allocating output stream!",__FILE__,__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    timeBase = in_stream->time_base;
#ifdef _dbg_ffmpeg_t_
    printf("In Time Base %d - %d\n",timeBase.den,timeBase.num);
#endif
    ret = avcodec_parameters_copy(out_stream->codecpar,in_codecpar);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}: Failed to copy the stream parameters!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}:{}",__FILE__,__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:{}: Could not open output file {}",__FILE__,__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }

#ifdef _dbg_using_spdlog_
    AVRational r_frame_rate = in_stream->r_frame_rate;
    dbg_spdlogger.sysLogger->info("{}:{}:{}: Open output file {} with estimate fps {}:{} --- "
                                  "time base {}:{}",__FILE__,__FUNCTION__,__LINE__,output
                                  ,r_frame_rate.num,r_frame_rate.den,timeBase.num,timeBase.den);
#endif

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
#ifdef _dbg_ffmpeg_t_
    av_dump_format(ofmt_ctx, 0, output, 1);
#endif

    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header! {}",__FILE__,__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:{}:Error occurred when avformat_write_header!",__FILE__,__FUNCTION__,__LINE__);
        }
#endif
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
#ifdef _dbg_using_spdlog_
            dbg_spdlogger.sysLogger->warn("{}:{}: Set key {} : value {} fails!",__FUNCTION__,__LINE__,t->key,t->value);
#endif
        }
        av_dict_free(&d);
    }
    return true;
}

bool ffmpeg_out_ipl6::open(const char* output,const char* realFinalFile,ffmpeg_cap &cap)
{
    if(realFinalFile != NULL && open(output,cap) == true)
    {
        fileTempName = std::string(output);
        fileWorking = std::string(realFinalFile);
        return true;
    }
    return false;
}

bool ffmpeg_out_ipl6::open(const char* output,const char* realFinalFile,ffmpeg_M2out *encode_out)
{
    if(realFinalFile != NULL && open(output,encode_out) == true)
    {
        fileTempName = std::string(output);
        fileWorking = std::string(realFinalFile);
        return true;
    }
    return false;
}

ffmpeg_out* createFF_out_auFps(uint timeStop){
    if(timeStop > 0){
        return new ffmpeg_out_ipl4(timeStop);
    }
    printf("Time record must be positive!\n");
    return NULL;
}

ffmpeg_out* createFF_out_auFps_tc(uint timeToSave){
    if(timeToSave > 0){
        return new ffmpeg_out_ipl5(timeToSave);
    }
    printf("Time record must be positive!\n");
    return NULL;
}

ffmpeg_out* createFF_out_auFps_dr(uint timeToSave){
    if(timeToSave > 0){
        return new ffmpeg_out_ipl6(timeToSave);
    }
    printf("Time record must be positive!\n");
    return NULL;
}

bool ffmpeg_out_debug::open(const char* output,ffmpeg_cap &cap,int flag)
{
    int ret;
    refresh();
    dbg_c = 0;
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (vid < 0) {
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Cannot find a video stream in the input file. ");
        showFError(ret);
#else
        fprintf(stderr, "Cannot find a video stream in the input file.\n");
#endif
        return false;
    }
    AVStream *in_stream = ifmt_ctx->streams[vid];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase.push_back(in_stream->time_base);

    printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_copy(out_stream->codecpar,in_codecpar);
    if (ret < 0) {
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'\n", output);
            return false;
        }

    }

    AVRational r_frame_rate = in_stream->r_frame_rate;
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        diff_ts = float(in_tBase[0].den)/float(in_tBase[0].num * r_fps);
#ifdef _dbg_ffmpeg_t_        
        printf("Output difference ts " "%" PRId64 "\n",diff_ts);
#endif        
    }

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
//    av_opt_set_dict(ofmt_ctx->pb, &d);
//    if(d != NULL){
//        AVDictionaryEntry *t = NULL;
//        cout << endl;
//        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
//            cout << "Key " << t->key << ": Value " << t->value << endl;
//        }
//        av_dict_free(&d);
//    }

    av_dump_format(ofmt_ctx, 0, output, 1);


    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }



    return true;
}

void ffmpeg_out_debug::handleLastWrite(){
    if(dPkt.dts != AV_NOPTS_VALUE && dPkt.size > 0){
        int ret;
        int64_t stop_ts_thres = 0;
        if(in_tBase[0].num > 0){
            stop_ts_thres = double(in_tBase[0].den * time_to_end * 60)/in_tBase[0].num;
        }
        AVStream *out_stream = ofmt_ctx->streams[0];
        //            dPkt.pts = (dPkt.pts - pPkt.pts) + diff_ts;
        //            dPkt.dts = (dPkt.dts - pPkt.dts) + diff_ts;
        dPkt.pts += diff_ts;
        dPkt.dts += diff_ts;
        int dbg_last_pad_frame = 0;
#ifdef _dbg_ffmpeg_t_        
        printf("Last dts " "%" PRId64 ,dPkt.dts);
        printf(" --- Stop ts thresh " "%" PRId64 "\n",stop_ts_thres);
#endif        
        while(dPkt.dts < stop_ts_thres){
            AVPacket pkt;
            memset(&pkt,0,sizeof(pkt));
            av_init_packet(&pkt);
            uint8_t *data_temp = (uint8_t*)av_malloc(dPkt.size);
            if(data_temp == NULL){
                printf("Try to alloc temp data fails!\n");
                break;
            }
            //                memcpy(data_temp,dPkt.data,dPkt.size);
            memcpy(data_temp,data_last_end,dPkt.size);
            ret = av_packet_from_data(&pkt,data_temp,dPkt.size);
            if(ret != 0){
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                break;
            }
            pkt.pts = dPkt.pts = dPkt.pts + diff_ts;
            pkt.dts = dPkt.dts = dPkt.dts + diff_ts;
            pkt.stream_index = 0;
            av_packet_rescale_ts(&pkt,in_tBase[0],out_stream->time_base);
            ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
            if(ret < 0){
                printf("Write interleaved error!\n");
            }
            av_packet_unref(&pkt);
            dbg_last_pad_frame += 1;
        }
        cout << "Number last pad frame " << dbg_last_pad_frame << endl;
        av_packet_unref(&dPkt);
        dPkt.pts = AV_NOPTS_VALUE;
        dPkt.dts = AV_NOPTS_VALUE;
        //            av_interleaved_write_frame(ofmt_ctx, NULL);
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
    }else{
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
    }
}

void ffmpeg_out_debug::handleLastWrite_New(){
    if(dPkt.dts != AV_NOPTS_VALUE){
        int ret;
        int64_t stop_ts_thres = 0;
        if(in_tBase[0].num > 0){
            stop_ts_thres = double(in_tBase[0].den * time_to_end * 60)/in_tBase[0].num;
        }
        AVStream *out_stream = ofmt_ctx->streams[0];
#ifdef _dbg_ffmpeg_t_        
        printf("Last dts " "%" PRId64 ,dPkt.dts);
        printf(" --- Stop ts thresh " "%" PRId64 "\n",stop_ts_thres);
#endif        
        diff_ts = 0;
        if(dPkt.dts < stop_ts_thres){
            diff_ts = double(stop_ts_thres)/dbg_c;
        }
        dPkt.pts = -diff_ts;
        dPkt.dts = -diff_ts;

        for(auto it : buffAV){
            dPkt.pts += diff_ts;
            dPkt.dts += diff_ts;
            it->pts = dPkt.pts;
            it->dts = dPkt.dts;
            av_packet_rescale_ts(it,in_tBase[0],out_stream->time_base);
            ret = av_interleaved_write_frame(ofmt_ctx, it);
            if(ret < 0){
                printf("Write interleaved error!\n");
            }
            av_packet_unref(it);
            av_packet_free(&it);
        }
        buffAV.clear();
        av_packet_unref(&dPkt);
        dPkt.pts = AV_NOPTS_VALUE;
        dPkt.dts = AV_NOPTS_VALUE;
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
    }else{
        av_write_frame(ofmt_ctx, NULL);
        av_write_trailer(ofmt_ctx);
    }
}

bool ffmpeg_out_debug::write_data_new(int ret,AVPacket &in_pkt,bool f_continue){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            handleLastWrite_New();
        }
        return false;
    }else{
        if(!f_continue){
            handleLastWrite_New();
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = diff_ts;
                    pkt->dts = diff_ts;
                }
                else{
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
                    pkt->pts = ( pkt->pts - pPkt.pts ) + diff_ts;
                    pkt->dts = ( pkt->dts - pPkt.dts) + diff_ts;
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
//                    if(dbg_c % 100 == 0){
//                        printf("td_pts " "%" PRId64 ,td_pts);
//                        printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                    }
                }

            }
            dbg_c += 1;
//            if(dPkt.dts != AV_NOPTS_VALUE){
//                if(dbg_c % 100 == 0){
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
//                    printf("td_pts " "%" PRId64 ,td_pts);
//                    printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                }
//            }
            dPkt.dts = pkt->dts;
            dPkt.pts = pkt->pts;
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
            buffAV.push_back(pkt);
            out_stream = NULL;
            return true;
        }
    }
}

bool ffmpeg_out_debug::write_data(int ret,AVPacket &in_pkt,bool f_continue){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            handleLastWrite();
        }
        return false;
    }else{
        if(!f_continue){
            handleLastWrite();
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = diff_ts;
                    pkt->dts = diff_ts;
                }
                else{
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
                    pkt->pts = ( pkt->pts - pPkt.pts ) + diff_ts;
                    pkt->dts = ( pkt->dts - pPkt.dts) + diff_ts;
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
//                    if(dbg_c % 100 == 0){
//                        printf("td_pts " "%" PRId64 ,td_pts);
//                        printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                    }
                }

            }
            dbg_c += 1;
//            if(dPkt.dts != AV_NOPTS_VALUE){
//                if(dbg_c % 100 == 0){
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
//                    printf("td_pts " "%" PRId64 ,td_pts);
//                    printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                }
//            }
            dPkt.dts = pkt->dts;
            dPkt.pts = pkt->pts;
            if(pkt->flags == AV_PKT_FLAG_KEY){
                if(data_last_end != NULL){
                    av_freep(&data_last_end);
                }
                dPkt.size = pkt->size;
                data_last_end = (uint8_t*)av_malloc(pkt->size);
                memcpy(data_last_end,pkt->data,dPkt.size);
            }
            av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            log_packet(ofmt_ctx, pkt, "out");
#endif
//            ret = av_write_frame(ofmt_ctx,pkt);
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
#ifdef _dbg_ffmpeg_t_
            if (ret < 0) {

                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


            }
#endif
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }
    }
}

bool ffmpeg_out_debug::write_data_old(int ret,AVPacket &in_pkt,bool f_continue){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            handleLastWrite();
        }
        return false;
    }else{
        if(!f_continue){
            handleLastWrite();
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_clone(&in_pkt);

            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = diff_ts;
                    pkt->dts = diff_ts;
                }
                else{
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
                    pkt->pts = ( pkt->pts - pPkt.pts ) + diff_ts;
                    pkt->dts = ( pkt->dts - pPkt.dts) + diff_ts;
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
//                    if(dbg_c % 100 == 0){
//                        printf("td_pts " "%" PRId64 ,td_pts);
//                        printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                    }
                }

            }
            dbg_c += 1;
            av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
            if(dPkt.dts != AV_NOPTS_VALUE){
                if(dbg_c % 100 == 0){
                    int64_t td_pts = ( pkt->pts - dPkt.pts );
                    int64_t td_dts = ( pkt->dts - dPkt.dts);
#ifdef _dbg_ffmpeg_t_                    
                    printf("td_pts " "%" PRId64 ,td_pts);
                    printf(" --- td_dts " "%" PRId64 "\n",td_dts);
#endif
                }
                dPkt.dts = pkt->dts;
                dPkt.pts = pkt->pts;
            }else{
                dPkt.dts = pkt->dts;
                dPkt.pts = pkt->pts;
            }
            if(pkt->flags == AV_PKT_FLAG_KEY){
                if(data_last_end != NULL){
                    av_freep(&data_last_end);
                }
                dPkt.size = pkt->size;
                data_last_end = (uint8_t*)av_malloc(pkt->size);
                memcpy(data_last_end,pkt->data,dPkt.size);
            }
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            log_packet(ofmt_ctx, pkt, "out");
#endif
//            ret = av_write_frame(ofmt_ctx,pkt);
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
#ifdef _dbg_ffmpeg_t_
            if (ret < 0) {

                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


            }
#endif
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }

    }
}

ffmpeg_out* createFF_padFrame(uint timeStop){
    if(timeStop > 0){
        return new ffmpeg_out_ipl3(timeStop);
    }else{
        return new ffmpeg_out_ipl3;
    }
}

void ffmpeg_out_debug1::handleLastWrite(){
    av_write_frame(ofmt_ctx, NULL);
    av_write_trailer(ofmt_ctx);
    if(ofmt_ctx != NULL && ofmt != NULL){
        if(!(ofmt->flags & AVFMT_NOFILE)){
            avio_closep(&ofmt_ctx->pb);
        }
    }
    avformat_free_context(ofmt_ctx);
    ofmt_ctx = NULL;
    if(dPkt.dts != AV_NOPTS_VALUE){
        int64_t stop_ts_thres = 0;
        if(in_tBase[0].num > 0){
            stop_ts_thres = double(in_tBase[0].den * time_to_end * 60)/in_tBase[0].num;
        }
#ifdef _dbg_ffmpeg_t_        
        printf("Last dts " "%" PRId64 ,dPkt.dts);
        printf(" --- Stop ts thresh " "%" PRId64 "\n",stop_ts_thres);
#endif        
        if(dPkt.dts < stop_ts_thres){
            av_packet_unref(&dPkt);
            dPkt.pts = AV_NOPTS_VALUE;
            dPkt.dts = AV_NOPTS_VALUE;
            //Read packet out and fix the timestamp here
            ffmpeg_cap cap;
            if(!cap.open(fileTempName.c_str())){
                cout << "Could not open input temp file!" << endl;
                return;
            }
            if(!openRefine(cap)){
                cout << "Could not open refile output!" << endl;
                return;
            }
            stop_ts_thres = double(in_tBase[0].den * time_to_end * 60)/in_tBase[0].num;
            diff_ts = double(stop_ts_thres)/dbg_c;
#ifdef _dbg_ffmpeg_t_            
            printf("Modify difference ts " "%" PRId64 "\n",diff_ts);
#endif            
            int ret;
            AVPacket t_pkt;
            memset(&t_pkt,0,sizeof(t_pkt));
            av_init_packet(&t_pkt);
            ret = cap.get_new_packet(t_pkt);
            AVStream *out_stream = ofmt_ctx->streams[0];
            dPkt.pts = -diff_ts;
            dPkt.dts = -diff_ts;
            while(ret >= 0){
                dPkt.pts += diff_ts;
                dPkt.dts += diff_ts;
                t_pkt.pts = dPkt.pts;
                t_pkt.dts = dPkt.dts;
                av_packet_rescale_ts(&t_pkt,in_tBase[0],out_stream->time_base);
                ret = av_interleaved_write_frame(ofmt_ctx, &t_pkt);
                if(ret < 0){
                    printf("Write interleaved error!\n");
                }
                av_packet_unref(&t_pkt);
                ret = cap.get_new_packet(t_pkt);
            }
            av_packet_unref(&t_pkt);
            av_write_frame(ofmt_ctx, NULL);
            av_write_trailer(ofmt_ctx);
            dPkt.pts = AV_NOPTS_VALUE;
            dPkt.dts = AV_NOPTS_VALUE;
        }else{
            av_packet_unref(&dPkt);
            dPkt.pts = AV_NOPTS_VALUE;
            dPkt.dts = AV_NOPTS_VALUE;
            FILE *fInput = fopen(fileTempName.c_str(),"rb");
            if(fInput == NULL){
                cout << "Try to open " << fileTempName << " fails!" << endl;
                return;
            }
            FILE *fOuput = fopen(fileWorking.c_str(),"wb");
            if(fOuput == NULL){
                cout << "Try to open " << fileWorking << " fails!" << endl;
                fclose(fInput);
                return;
            }

            char tempD[1025];
            int length = fread(tempD,1,1024,fInput);
            int length1;
            while(length > 0)
            {
                length1 = fwrite(tempD,1,length,fOuput);
                if(length1 != length)
                {
                    cout << "Write byte error!" << endl;
                    break;
                }
                length = fread(tempD,1,1024,fInput);
            }
            fclose(fInput);
            fclose(fOuput);
        }

        //            av_interleaved_write_frame(ofmt_ctx, NULL);
    }
}

bool ffmpeg_out_debug1::openRefine(ffmpeg_cap &cap)
{
    int ret;
    dbg_c = 0;
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, fileWorking.c_str());
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (vid < 0) {
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Cannot find a video stream in the input file. ");
        showFError(ret);
#else
        fprintf(stderr, "Cannot find a video stream in the input file.\n");
#endif
        return false;
    }
    AVStream *in_stream = ifmt_ctx->streams[vid];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase.push_back(in_stream->time_base);

    printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_copy(out_stream->codecpar,in_codecpar);
    if (ret < 0) {
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, fileWorking.c_str(), AVIO_FLAG_READ_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'\n", fileWorking.c_str());
            return false;
        }

    }

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }

//    av_dump_format(ofmt_ctx, 0, fileWorking.c_str(), 1);


    ret = avformat_write_header(ofmt_ctx, &d);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }

    return true;
}

bool ffmpeg_out_debug1::open(const char* output,ffmpeg_cap &cap,int flag)
{
    int ret;
    refresh();
    dbg_c = 0;
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, fileTempName.c_str());
    if (!ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    fileWorking = std::string(output);
    ofmt = ofmt_ctx->oformat;
    AVFormatContext *ifmt_ctx = cap.ifmt_ctx;

#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    vid = av_find_best_stream(ifmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    if (vid < 0) {
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Cannot find a video stream in the input file. ");
        showFError(ret);
#else
        fprintf(stderr, "Cannot find a video stream in the input file.\n");
#endif
        return false;
    }
    AVStream *in_stream = ifmt_ctx->streams[vid];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase.push_back(in_stream->time_base);

    printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_copy(out_stream->codecpar,in_codecpar);
    if (ret < 0) {
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }

    if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
        out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (!(ofmt->flags & AVFMT_NOFILE)) {

        ret = avio_open(&ofmt_ctx->pb, fileTempName.c_str(), AVIO_FLAG_READ_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'\n", fileTempName.c_str());
            return false;
        }

    }

    AVRational r_frame_rate = in_stream->r_frame_rate;
    printf("Output frame rate %d - %d\n",r_frame_rate.den,r_frame_rate.num);
    if(r_frame_rate.den > 0 && in_tBase[0].num > 0){
        float r_fps = float(r_frame_rate.num)/r_frame_rate.den;
        diff_ts = float(in_tBase[0].den)/float(in_tBase[0].num * r_fps);
#ifdef _dbg_ffmpeg_t_        
        printf("Output difference ts " "%" PRId64 "\n",diff_ts);
#endif        
    }

    AVDictionary *d = NULL;
    ret = av_dict_set(&d, "movflags", "+skip_trailer", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using skip_trailer fails!\n");
        return false;
    }
    ret = av_dict_set(&d, "movflags", "+faststart", 0);
    if(ret < 0){
        fprintf(stderr,"Try to using faststart fails!\n");
        return false;
    }
//    av_opt_set_dict(ofmt_ctx->pb, &d);
//    if(d != NULL){
//        AVDictionaryEntry *t = NULL;
//        cout << endl;
//        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
//            cout << "Key " << t->key << ": Value " << t->value << endl;
//        }
//        av_dict_free(&d);
//    }

    av_dump_format(ofmt_ctx, 0, fileTempName.c_str(), 1);


    ret = avformat_write_header(ofmt_ctx, &d);
    //ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
        return false;
    }
    if(d != NULL){
        AVDictionaryEntry *t = NULL;
        cout << endl;
        while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
            cout << "Key " << t->key << ": Value " << t->value << endl;
        }
        av_dict_free(&d);
    }

    return true;
}

bool ffmpeg_out_debug1::write_data(int ret,AVPacket &in_pkt,bool f_continue){
//    return true;
    if (ret < 0){
#ifdef _dbg_ffmpeg_t_
        showFError(ret);
#endif
        if(f_continue == false){
            handleLastWrite();
        }
        return false;
    }else{
        if(!f_continue){
            handleLastWrite();
            return true;
        }else{
            if(in_pkt.stream_index != vid){
                return true;
            }
            AVStream *out_stream;//*in_stream,
            AVPacket *pkt = av_packet_alloc();
            uint8_t *data_temp = (uint8_t*)av_malloc(in_pkt.size);
            if(data_temp == NULL){
                printf("Try to alloc temp data fails!\n");
                av_packet_free(&pkt);
                return false;
            }
            memcpy(data_temp,in_pkt.data,in_pkt.size);
            ret = av_packet_from_data(pkt,data_temp,in_pkt.size);
            if(ret != 0){
                printf("Try make packet from data fails!\n");
                av_freep(&data_temp);
                av_packet_free(&pkt);
                return false;
            }
            pkt->dts = in_pkt.dts;
            pkt->pts = in_pkt.pts;
            pkt->duration = in_pkt.duration;
            pkt->flags = in_pkt.flags;
            pkt->stream_index = 0;
            out_stream = ofmt_ctx->streams[0];
            if(pkt->pts != AV_NOPTS_VALUE){
                if(pPkt.pts == AV_NOPTS_VALUE){
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
                    pPkt.pts = pkt->pts;
                    pPkt.dts = pkt->dts;
                    pkt->pts = diff_ts;
                    pkt->dts = diff_ts;
                }
                else{
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
                    pkt->pts = ( pkt->pts - pPkt.pts ) + diff_ts;
                    pkt->dts = ( pkt->dts - pPkt.dts) + diff_ts;
//                    av_packet_unref(&dPkt);
//                    av_packet_ref(&dPkt,&in_pkt);
//                    if(dbg_c % 100 == 0){
//                        printf("td_pts " "%" PRId64 ,td_pts);
//                        printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                    }
                }

            }
            dbg_c += 1;
//            if(dPkt.dts != AV_NOPTS_VALUE){
//                if(dbg_c % 100 == 0){
//                    int64_t td_pts = ( pkt->pts - dPkt.pts );
//                    int64_t td_dts = ( pkt->dts - dPkt.dts);
//                    printf("td_pts " "%" PRId64 ,td_pts);
//                    printf(" --- td_dts " "%" PRId64 "\n",td_dts);
//                }
//            }
            dPkt.dts = pkt->dts;
            dPkt.pts = pkt->pts;
            av_packet_rescale_ts(pkt, in_tBase[0],out_stream->time_base);
#ifdef _dbg_ffmpeg_t_
            printf("pkt pts " "%" PRId64 ,pkt->pts);
            printf(" --- pkt dts " "%" PRId64 ,pkt->dts);
            printf(" --- pkt duration " "%" PRId64 "\n",pkt->duration);
#endif
            pkt->pos = -1;
#ifdef _dbg_ffmpeg_t_
            log_packet(ofmt_ctx, pkt, "out");
#endif
//            ret = av_write_frame(ofmt_ctx,pkt);
            ret = av_interleaved_write_frame(ofmt_ctx, pkt);
#ifdef _dbg_ffmpeg_t_
            if (ret < 0) {

                showFError(ret);
                fprintf(stderr, "Error muxing packet\n");


            }
#endif
            out_stream = NULL;
            av_packet_unref(pkt);
            av_packet_free(&pkt);
            return true;
        }
    }
}

bool ffmpeg_out_srtpT::open(const char* output,ffmpeg_cap &cap,ffmpeg_transcode *tsc,int flag){
    int ret;
    refresh();
    avformat_alloc_output_context2(&ofmt_ctx, NULL, "rtp_mpegts", output);
    if (!ofmt_ctx) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Could not create output context!",__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    ofmt = ofmt_ctx->oformat;
#ifdef _dbg_ffmpeg_t_
    if(ofmt != NULL){
        printf("Output name %s ",ofmt->name);
        printf("| extend %s ",ofmt->extensions);
        printf("| long name %s ",ofmt->long_name);
        printf("| mine type %s\n",ofmt->mime_type);
    }
#endif
    if(initBSFC(tsc->encoder_ctx) == false){
        return false;
    }
    AVStream *out_stream;
    out_stream = avformat_new_stream(ofmt_ctx, tsc->enc_codec);
    if (!out_stream) {
#ifdef _dbg_using_spdlog_
        dbg_spdlogger.sysLogger->error("{}:{}:Failed allocating output stream!",__FUNCTION__,__LINE__);
#endif
        fprintf(stderr, "Failed allocating output stream\n");
        ret = AVERROR_UNKNOWN;
        return false;
    }
    in_tBase = vector<AVRational>(1,tsc->encoder_ctx->time_base);
    //		printf("In Time Base %d - %d\n",in_tBase[0].den,in_tBase[0].num);
    ret = avcodec_parameters_from_context(out_stream->codecpar, tsc->encoder_ctx);
    if (ret < 0) {
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Failed to copy the stream parameters!",__FUNCTION__,__LINE__);
        }
#endif
#ifdef _dbg_ffmpeg_t_
        fprintf(stderr, "Failed to copy the stream parameters.");
        showFError(ret);
#else
        fprintf(stderr, "Failed to copy the stream parameters.\n");
#endif
        return false;
    }
    if (!(ofmt->flags & AVFMT_NOFILE)) {
        AVDictionary *d = NULL;
        //Using tcp to connect rtsp
        if(flag == 0){
            ret = av_dict_set(&d, "srtp_out_suite", "AES_CM_128_HMAC_SHA1_80", 0);
        }else if(flag == 1){
            ret = av_dict_set(&d, "srtp_out_suite", "AES_CM_128_HMAC_SHA1_32", 0);
        }
        if(ret < 0){
            fprintf(stderr,"Try to using crypto rtp fails!\n");
            return false;
        }
    //    cout << "Test key SHA1 " << key_base64 << endl;
        ret = av_dict_set(&d, "srtp_out_params", key_base64.c_str(), 0);
        if(ret < 0){
            fprintf(stderr,"Set key crypto rtp fails!\n");
            return false;
        }

        ret = avio_open2(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE,NULL,&d);
        //ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
        if (ret < 0) {
#ifdef _dbg_using_spdlog_
            std::string outErrStr;
            if(getFError(ret,outErrStr)){
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}:{}",__FUNCTION__,__LINE__,output,outErrStr);
            }else{
                dbg_spdlogger.sysLogger->error("{}:{}:Could not open output file {}",__FUNCTION__,__LINE__,output);
            }
#endif
            fprintf(stderr, "Could not open output file '%s'", output);
            return false;
        }


        if(d != NULL){
            AVDictionaryEntry *t = NULL;
            cout << endl;
            while (t = av_dict_get(d, "", t, AV_DICT_IGNORE_SUFFIX)) {
                cout << "Key " << t->key << ": Value " << t->value << endl;
            }
            av_dict_free(&d);
        }
    }




//    av_opt_set(ofmt_ctx->pb, "srtp_out_suite", "AES_CM_128_HMAC_SHA1_80",  AV_OPT_SEARCH_CHILDREN);
//    av_opt_set(ofmt_ctx->pb, "srtp_out_params", key_base64.c_str(),  AV_OPT_SEARCH_CHILDREN);
    ret = avformat_write_header(ofmt_ctx,NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
#ifdef _dbg_using_spdlog_
        std::string outErrStr;
        if(getFError(ret,outErrStr)){
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file! {}",__FUNCTION__,__LINE__,outErrStr);
        }else{
            dbg_spdlogger.sysLogger->error("{}:{}:Error occurred when opening output file!",__FUNCTION__,__LINE__);
        }
#endif
        return false;
    }
    av_dump_format(ofmt_ctx, 0, output, 1);
    return true;
}

ffmpeg_out* createFF_out_srtpT(const char *key_s){
    return new ffmpeg_out_srtpT(key_s);
}

}
