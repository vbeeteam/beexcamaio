/*
 * boardCastReader_FFmpeg_Capture.cpp
 *
 *  Created on: Mar 12, 2020
 *      Author: kien
 */

#include <iostream>

using namespace std;

//#define _Dbg_show_infor_

extern "C" {
#include <libavformat/avformat.h>
#ifdef using_hardware_qsv
#include <libavdevice/avdevice.h>
#endif
#include <libavutil/timestamp.h>
#include <libavcodec/avcodec.h>
//#include <libswresample/swresample.h>
#ifdef _Dbg_show_infor_
#include <libavutil/pixdesc.h>
#endif
}

#include "boardCastImplement.hpp"

//#define _dbg_ffmpeg_t_

#ifdef _dbg_ffmpeg_t_
#include "Ffmpeg_support.h"
#endif

namespace boardCastDataProvider{

static const char* getCodecName_omx(enum AVCodecID id_codec){
    switch(id_codec){
    case AV_CODEC_ID_H264:
        return "h264_mmal";
    case AV_CODEC_ID_MPEG4:
        return "mpeg4_mmal";
    case AV_CODEC_ID_MPEG2VIDEO:
        return "mpeg2_mmal";
    case AV_CODEC_ID_VC1:
        return "vc1_mmal";
    default:
        return NULL;
    };
}

bool ffmpeg_remux::remuxNonStop(){
	bool f_rs = false;
	int ret;
	AVPacket pkt;
	old_dts = -1;
	while (1) {
		AVStream *in_stream, *out_stream;

		ret = av_read_frame(ifmt_ctx, &pkt);
		if (ret < 0){
			if(ret == AVERROR_EOF){
				f_rs = true;
			}
			break;
		}

		in_stream  = ifmt_ctx->streams[pkt.stream_index];
		if (pkt.stream_index >= stream_mapping_size ||
				stream_mapping[pkt.stream_index] < 0) {
			av_packet_unref(&pkt);
			continue;
		}

		pkt.stream_index = stream_mapping[pkt.stream_index];
		out_stream = ofmt_ctx->streams[pkt.stream_index];
#ifdef _dbg_ffmpeg_t_
		log_packet(ifmt_ctx, &pkt, "in");
#endif
		/* copy packet */
		pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
		pkt.pos = -1;
		//if(pkt.dts > old_dts)
		{
#ifdef _dbg_ffmpeg_t_
			log_packet(ofmt_ctx, &pkt, "out");
#endif
			ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
			if (ret < 0) {
				fprintf(stderr, "Error muxing packet\n");
				//break;
			}
			old_dts = pkt.dts;
		}
		av_packet_unref(&pkt);
	}

	av_write_trailer(ofmt_ctx);
#ifdef _dbg_ffmpeg_t_
	showFError(ret);
#endif
	return f_rs;
}


bool ffmpeg_remux::remuxUntil(int maxFrame){
	if(maxFrame < 1){
		return remuxNonStop();
	}
	int count_f = 0;
	bool f_rs = false;
	int ret;
	AVPacket pkt;
	old_dts = -1;
	while (1) {
		AVStream *in_stream, *out_stream;

		ret = av_read_frame(ifmt_ctx, &pkt);
		if (ret < 0)
			break;

		in_stream  = ifmt_ctx->streams[pkt.stream_index];
		if (pkt.stream_index >= stream_mapping_size ||
				stream_mapping[pkt.stream_index] < 0) {
			av_packet_unref(&pkt);
			continue;
		}

		pkt.stream_index = stream_mapping[pkt.stream_index];
		out_stream = ofmt_ctx->streams[pkt.stream_index];
#ifdef _dbg_ffmpeg_t_
		log_packet(ifmt_ctx, &pkt, "in");
#endif

		/* copy packet */
		pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
		pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
		pkt.pos = -1;
		//if(pkt.dts > old_dts)
		{
#ifdef _dbg_ffmpeg_t_
			log_packet(ofmt_ctx, &pkt, "out");
#endif
			ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
			if (ret < 0) {
				fprintf(stderr, "Error muxing packet\n");
				//break;
			}
			count_f += 1;
			if(count_f == maxFrame){
				f_rs = true;
				break;
			}
			old_dts = pkt.dts;
		}

		av_packet_unref(&pkt);

	}

	av_write_trailer(ofmt_ctx);
#ifdef _dbg_ffmpeg_t_
	printf("Number frame success remux %d\n",count_f);
	showFError(ret);
#endif
	return f_rs;
}

bool ffmpeg_remux::remux(bool f_continue){
	AVStream *in_stream, *out_stream;
	AVPacket pkt;
	int ret = av_read_frame(ifmt_ctx, &pkt);
	if (ret < 0){
#ifdef _dbg_ffmpeg_t_
		showFError(ret);
#endif
		if(f_continue == false){
			av_write_trailer(ofmt_ctx);
		}
		return false;
	}else{
		in_stream  = ifmt_ctx->streams[pkt.stream_index];
		if (pkt.stream_index >= stream_mapping_size ||
				stream_mapping[pkt.stream_index] < 0) {
			av_packet_unref(&pkt);
		}
		else{
			pkt.stream_index = stream_mapping[pkt.stream_index];
			out_stream = ofmt_ctx->streams[pkt.stream_index];
#ifdef _dbg_ffmpeg_t_
			log_packet(ifmt_ctx, &pkt, "in");
#endif
			/* copy packet */
			pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
			pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
			pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
			pkt.pos = -1;
			//if(pkt.dts > old_dts)
			{
#ifdef _dbg_ffmpeg_t_
				log_packet(ofmt_ctx, &pkt, "out");
#endif

				ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
				if (ret < 0) {
#ifdef _dbg_ffmpeg_t_
					showFError(ret);
					fprintf(stderr, "Error muxing packet\n");
#endif

					//return false;
				}
				old_dts = pkt.dts;
			}
			av_packet_unref(&pkt);
		}
		if(f_continue == false){
			av_write_trailer(ofmt_ctx);
			old_dts = -1;
		}
		return true;
	}
}

bool ffmpeg_remux::open(const char* input,const char* output,const char* extra_infor){
	bool init_rs = false;
	int ret;
	unsigned int i;
	int stream_index = 0;
	refresh();
	if ((ret = avformat_open_input(&ifmt_ctx, input, 0, 0)) < 0) {
		fprintf(stderr, "Could not open input file '%s'", input);
		return init_rs;
	}

	if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
		fprintf(stderr, "Failed to retrieve input stream information");
		return init_rs;
	}

	av_dump_format(ifmt_ctx, 0, input, 0);

	avformat_alloc_output_context2(&ofmt_ctx, NULL, extra_infor, output);
	if (!ofmt_ctx) {
		fprintf(stderr, "Could not create output context\n");
		ret = AVERROR_UNKNOWN;
		return init_rs;
	}


	stream_mapping_size = ifmt_ctx->nb_streams;
	stream_mapping = (int*)av_mallocz_array(stream_mapping_size, sizeof(*stream_mapping));
	if (!stream_mapping) {
		fprintf(stderr, "Out of memory\n");
		ret = AVERROR(ENOMEM);
		return init_rs;
	}

	ofmt = ofmt_ctx->oformat;
#ifdef _dbg_ffmpeg_t_
	if(ofmt != NULL){
		printf("Output name %s ",ofmt->name);
		printf("| extend %s ",ofmt->extensions);
		printf("| long name %s ",ofmt->long_name);
		printf("| mine type %s\n",ofmt->mime_type);
	}
#endif
	bool f_noAudio = false;
	if(strncmp(ofmt->name,"mp4",3) == 0){
		f_noAudio = true;
	}else if(strncmp(ofmt->name,"rtp",3) == 0){
		f_noAudio = true;
	}

	for (i = 0; i < ifmt_ctx->nb_streams; i++) {
		AVStream *out_stream;
		AVStream *in_stream = ifmt_ctx->streams[i];
		AVCodecParameters *in_codecpar = in_stream->codecpar;

		if ( (in_codecpar->codec_type != AVMEDIA_TYPE_AUDIO || f_noAudio == true) &&
				in_codecpar->codec_type != AVMEDIA_TYPE_VIDEO &&
				in_codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE) {
			stream_mapping[i] = -1;
			continue;
		}

		stream_mapping[i] = stream_index++;

		out_stream = avformat_new_stream(ofmt_ctx, NULL);
		if (!out_stream) {
			fprintf(stderr, "Failed allocating output stream\n");
			ret = AVERROR_UNKNOWN;
			return init_rs;
		}

		ret = avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
		if (ret < 0) {
			fprintf(stderr, "Failed to copy codec parameters\n");
			return init_rs;
		}
		out_stream->codecpar->codec_tag = 0;
	}
	av_dump_format(ofmt_ctx, 0, output, 1);

	if (!(ofmt->flags & AVFMT_NOFILE)) {
		ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
		if (ret < 0) {
			fprintf(stderr, "Could not open output file '%s'", output);
			return init_rs;
		}
	}

	ret = avformat_write_header(ofmt_ctx, NULL);
	if (ret < 0) {
		fprintf(stderr, "Error occurred when opening output file\n");
		return init_rs;
	}
	return true;
}

ffmpeg_remux::ffmpeg_remux(){
	ofmt = NULL;
	ifmt_ctx = NULL;
	ofmt_ctx = NULL;
	stream_mapping_size = 0;
	stream_mapping = NULL;
	old_dts = -1;
}

void ffmpeg_remux::refresh(){
	old_dts = -1;
	if(ifmt_ctx != NULL){
		avformat_close_input(&ifmt_ctx);
	}
	if(ofmt_ctx != NULL && ofmt != NULL){
		if(!(ofmt->flags & AVFMT_NOFILE)){
			avio_closep(&ofmt_ctx->pb);
		}
	}
	ofmt = NULL;
	avformat_free_context(ofmt_ctx);
	ofmt_ctx = NULL;
	av_freep(&stream_mapping);
}

ffmpeg_remux_v1::ffmpeg_remux_v1(){
	bsfc = NULL;
#ifdef using_hardware_qsv
	avdevice_register_all();
#endif
	avformat_network_init();
}

ffmpeg_remux_v1::~ffmpeg_remux_v1(){
	avformat_network_deinit();
}

bool ffmpeg_remux_v1::initBSFC(const char* bsf_name,int iVideoStream){
	const AVBitStreamFilter *bsf = av_bsf_get_by_name(bsf_name);
	if (!bsf) {
		cout << "FFmpeg error: " << __FILE__ << " " << __LINE__ << " " << "av_bsf_get_by_name() failed";
		return false;
	}
	int ret = av_bsf_alloc(bsf, &bsfc);
	if(ret < 0){
#ifdef _dbg_ffmpeg_t_
		showFError(ret);
#endif
		return false;
	}
    //TODO: Try this int avcodec_parameters_copy(AVCodecParameters *dst, const AVCodecParameters *src) then re-test av_bsf_free
	bsfc->par_in = ifmt_ctx->streams[iVideoStream]->codecpar;
	ret = av_bsf_init(bsfc);

	if(ret < 0){
#ifdef _dbg_ffmpeg_t_
		showFError(ret);
#endif
		return false;
	}
	return true;
}

bool ffmpeg_remux_v1::open(const char* input,const char* output,const char* extra_infor){
	bool init_rs = false;
	int ret;
	unsigned int i;
	int stream_index = 0;
	refresh();
	AVDictionary *d = NULL;           // "create" an empty dictionary
	ret = av_dict_set(&d, "rtsp_transport", "tcp", 0);
	//av_dict_set(&d, "rtsp_flags", "listen", 0);
	if(ret < 0){
		return false;
	}
	if ((ret = avformat_open_input(&ifmt_ctx, input, 0, &d)) < 0)
	//if ((ret = avformat_open_input(&ifmt_ctx, input, 0, 0)) < 0)
	{
		fprintf(stderr, "Could not open input file '%s'\n", input);
		return init_rs;
	}

	if ((ret = avformat_find_stream_info(ifmt_ctx, 0)) < 0) {
		fprintf(stderr, "Failed to retrieve input stream information");
		return init_rs;
	}




	av_dump_format(ifmt_ctx, 0, input, 0);



	avformat_alloc_output_context2(&ofmt_ctx, NULL, extra_infor, output);
	if (!ofmt_ctx) {
		fprintf(stderr, "Could not create output context\n");
		ret = AVERROR_UNKNOWN;
		return init_rs;
	}



	stream_mapping_size = ifmt_ctx->nb_streams;
	stream_mapping = (int*)av_mallocz_array(stream_mapping_size, sizeof(*stream_mapping));
	if (!stream_mapping) {
		fprintf(stderr, "Out of memory\n");
		ret = AVERROR(ENOMEM);
		return init_rs;
	}

	ofmt = ofmt_ctx->oformat;


#ifdef _dbg_ffmpeg_t_
	if(ofmt != NULL){
		printf("Output name %s ",ofmt->name);
		printf("| extend %s ",ofmt->extensions);
		printf("| long name %s ",ofmt->long_name);
		printf("| mine type %s\n",ofmt->mime_type);
	}
#endif
	int f_noAudio = 0;
	if(strncmp(ofmt->name,"mp4",3) == 0){
		f_noAudio = 1;
	}else if(strncmp(ofmt->name,"rtp",3) == 0){
		f_noAudio = 2;
	}

	for (i = 0; i < ifmt_ctx->nb_streams; i++) {
		AVStream *out_stream;
		AVStream *in_stream = ifmt_ctx->streams[i];
		AVCodecParameters *in_codecpar = in_stream->codecpar;

		if ( (in_codecpar->codec_type != AVMEDIA_TYPE_AUDIO || f_noAudio != 0) &&
				in_codecpar->codec_type != AVMEDIA_TYPE_VIDEO &&
				in_codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE) {
			stream_mapping[i] = -1;
			continue;
		}else if(in_codecpar->codec_type == AVMEDIA_TYPE_VIDEO){
			if(initBSFC("dump_extra",i) == false){
				return init_rs;
			};
		}



		stream_mapping[i] = stream_index++;

		out_stream = avformat_new_stream(ofmt_ctx, NULL);
		if (!out_stream) {
			fprintf(stderr, "Failed allocating output stream\n");
			ret = AVERROR_UNKNOWN;
			return init_rs;
		}

		ret = avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
		if (ret < 0) {
			fprintf(stderr, "Failed to copy codec parameters\n");
			return init_rs;
		}

		out_stream->codecpar->codec_tag = 0;
		if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
			out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

	}
	av_dump_format(ofmt_ctx, 0, output, 1);

	if (!(ofmt->flags & AVFMT_NOFILE)) {
		ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
		if (ret < 0) {
			fprintf(stderr, "Could not open output file '%s'", output);
			return init_rs;
		}
	}

	ret = avformat_write_header(ofmt_ctx, NULL);
	if (ret < 0) {
		fprintf(stderr, "Error occurred when opening output file\n");
		return init_rs;
	}
	return true;
}

bool ffmpeg_remux_v1::remux(bool f_continue){
	AVStream *in_stream, *out_stream;
	AVPacket pkt;
	av_init_packet(&pkt);
	int ret = av_read_frame(ifmt_ctx, &pkt);
	if (ret < 0){
#ifdef _dbg_ffmpeg_t_
		showFError(ret);
#endif
		if(f_continue == false){
			av_write_trailer(ofmt_ctx);
		}
		return false;
	}else{

		in_stream  = ifmt_ctx->streams[pkt.stream_index];
		if (pkt.stream_index >= stream_mapping_size ||
				stream_mapping[pkt.stream_index] < 0) {
			av_packet_unref(&pkt);
		}
		else{

			ret = av_bsf_send_packet(bsfc, &pkt);
			if(ret < 0){
#ifdef _dbg_ffmpeg_t_
				showFError(ret);
#endif
				av_packet_unref(&pkt);
				return false;
			}
			ret = av_bsf_receive_packet(bsfc, &pkt);
			while(ret == 0){
				pkt.stream_index = stream_mapping[pkt.stream_index];
				out_stream = ofmt_ctx->streams[pkt.stream_index];
#ifdef _dbg_ffmpeg_t_
				log_packet(ifmt_ctx, &pkt, "in");
#endif
				/* copy packet */
				if (pkt.pts != (int64_t)AV_NOPTS_VALUE)
					pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
				if (pkt.dts != (int64_t)AV_NOPTS_VALUE)
					pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base,AVRounding( AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX));
				if (pkt.duration)
					pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
				pkt.pos = -1;
				if(pkt.dts > old_dts)
				{
#ifdef _dbg_ffmpeg_t_
					//log_packet(ofmt_ctx, &pkt, "out");
					if(old_dts != -1){
						log_packetX(ofmt_ctx, &pkt, "out",old_dts);
					}else{
						log_packet(ofmt_ctx, &pkt, "out");
					}
#endif
					old_dts = pkt.dts;
					ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
					if (ret < 0) {
#ifdef _dbg_ffmpeg_t_
						showFError(ret);
						fprintf(stderr, "Error muxing packet\n");
#endif
						av_packet_unref(&pkt);
						return false;
					}

				}

				av_packet_unref(&pkt);
				ret = av_bsf_receive_packet(bsfc, &pkt);
			};
			if(ret != AVERROR(EAGAIN)){
#ifdef _dbg_ffmpeg_t_
				showFError(ret);
#endif
				return false;
			}

		}
		if(f_continue == false){
			av_write_trailer(ofmt_ctx);
			old_dts = -1;
		}
		return true;
	}
}

};
