/**
******************************************************************************
* @file           : report_api.cpp
* @brief          : source file of report API
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/

#include "event_report/report_api.h"


using namespace BeexEventReport;

bool ReportApi::requestByHttps(std::string &api, std::string &data){
    try{
#ifdef INTEL_NUC_BOX        
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
    
        Poco::SharedPtr<Poco::Net::InvalidCertificateHandler> ptrCert = new Poco::Net::AcceptCertificateHandler(false);
        Poco::Net::Context::Ptr ptrContext = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", this->cacert_path + std::string("cacert.pem"),  Poco::Net::Context::VERIFY_NONE);
        Poco::Net::SSLManager::instance().initializeClient(0, ptrCert, ptrContext);

        Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());

        std::ostream &os = session.sendRequest(request);
        os << data;
        std::istream &rsp = session.receiveResponse(response);
        std::stringstream messgae;
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            std::cout << "api response : " << message.str() << std::endl;
            return true;
        }
#endif        

    }
    catch(const std::exception& e){
        //std::cerr << e.what() << '\n';
    }
    return false;          
}


bool ReportApi::requestByHttp(std::string &api, std::string &data){
    try{
#ifdef INTEL_NUC_BOX        
        Poco::URI uri(api);
        std::string path(uri.getPathAndQuery());
        if(path.empty()){
            path = "/";
        }
        Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());
        Poco::Net::HTTPRequest request(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
        Poco::Net::HTTPResponse response;
        std::ostringstream message;

        request.setContentType("application/json");
        request.setContentLength(data.length());
        std::ostream &os = session.sendRequest(request);
        os << data;        

        std::istream &rsp = session.receiveResponse(response);
        std::stringstream messgae;
        Poco::StreamCopier::copyStream(rsp, message);

        if(response.getStatus() == Poco::Net::HTTPResponse::HTTP_OK){
            std::cout << "api response : " << message.str() << std::endl;
            return true;
        }
#endif        

    }
    catch(const std::exception& e){
        //std::cerr << e.what() << '\n';
    }
    return false;
}



bool ReportApi::reportEvent(std::string &type, std::string &content){
#ifdef INTEL_NUC_BOX    
    Poco::JSON::Object object;
    std::ostringstream out;
    bool result = false;

    object.set("deviceSerial", this->dev_serial);
    object.set("gatewaySerial", this->gw_serial);
    object.set("type", type);
    object.set("content", content);

    object.stringify(out);

    std::string data = out.str();

    if(this->url.find("https") != std::string::npos){
        result = this->requestByHttps(this->url, data);
    }
    else if(this->url.find("http") != std::string::npos){
        result = this->requestByHttp(this->url, data);
    }
    return result;
#endif    
}