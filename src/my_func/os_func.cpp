/**
******************************************************************************
* @file           : os_func.h
* @brief          : Header file of OS Function
* @author         : Tuan_NT
******************************************************************************
******************************************************************************
*/
/*Include -------------------------------------------------------------------*/
#include "my_func/os_func.h"
#include <sys/time.h>
#include<time.h>
#include <logger/webe_logger.h>
#include <sys/stat.h>
#include <iostream>
#include <cstdarg>
#include <string>
#include <fstream>
#include <memory>
#include <cstdio>

uint32_t OSFunc::getTimestamp(void){
	struct timeval tv;
	uint32_t get_time = 0;
	gettimeofday(&tv, NULL);
	
	get_time = tv.tv_sec + 25200;
	return get_time;
}

std::string OSFunc::getDate(void){
    std::string get_date = "";

    time_t now = time(0);
    tm *ltm = localtime(&now);
    get_date = std::to_string(ltm->tm_mday) + "_" + std::to_string(1+ltm->tm_mon) + "_" + std::to_string(1900+ltm->tm_year);
    return get_date;
}


std::string OSFunc::getTime(void){
    std::string get_time = "";

    time_t now = time(0);
    tm *ltm = localtime(&now);
    get_time = std::to_string(1+ltm->tm_hour) + "_" + std::to_string(1+ltm->tm_min) + "_" + std::to_string(1+ltm->tm_sec);
    return get_time;    
}

uint8_t OSFunc::splitString(std::string data, std::string key_split, std::string *data_split){
    uint16_t pos = 0;
    uint16_t i = 0;

    
    while ((pos = data.find(key_split)) != std::string::npos) {
        data_split[i] = data.substr(0, pos);
        data.erase(0, pos + key_split.length());
        i++;
        if (i > 9) {
            return 9;
        }
    }
    //myLogger.sysLogger->info("debug: {0}", std::to_string(i));

    return i;
}

bool OSFunc::checkFile(std::string path){
    struct stat buffer;   
    return (stat (path.c_str(), &buffer) == 0); 
}

size_t OSFunc::getSizeFile(std::string path){
    struct stat st;
    if(stat(path.c_str(), &st) != 0) {
        return 0;
    }
    return st.st_size;   
}

std::string OSFunc::exeCommand(const char* cmd){
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}